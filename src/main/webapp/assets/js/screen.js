var screen = {
  belts: [],
  clear: function () {
    video.init = 0;
    chart.clear();
    screen.chartdevs = [];
  },
  init: function () {
    $("#userName").text(sessionStorage.getItem("userName"));
    if (sessionStorage.getItem("role") == 1) {
      $("#tocfgspan").show();
    } else {
      $("#tocfgspan").hide();
    }
    screen.clear();
    screen.initAlarm();
    screen.timeUpdate();
    $.get("/screen/chartdev.do", function (data, status) {
      screen.chartdevs = data.objs;
      screen.initChart();
      /*
      let x1 = {
        xseries: ["2023-03-10 10:23:53", "2023-03-10 10:33:53"],
        yseries: ["12", "24"],
      };

      let x2 = {
        xseries: ["2023-03-10 10:23:53", "2023-03-10 10:33:53"],
        yseries: ["12", "24"],
      };

      let res = {
        "0-1": x1,
        "0-2": x2,
      };

      $.each(res, function (devId, value) {
        chart.setChartData(devId, value);
      });

      res = {
        "0-1": {
          ySeries: "75",
          xSeries: "2023-03-16 16:58:23",
          hour: 2.3,
        },
      };

      $.each(res, function (devId, value) {
        chart.addChartData(devId, value);
        $("#h" + devId).text(value.hour + "hours");
      });
      */

      //取电流的初始值
      $.get(
        "/registerValueRecord/queryInfo.do?type=0&devType=0",
        function (res, status) {
          if (res.code == 0) {
            //let data = res.data;
            $.each(res.data, function (devId, value) {
              //折线图
              chart.setChartData(devId, value);
              $("#h" + devId).text(value.hour + "hours");
            });
          }
        }
      );

      let timeout = setTimeout(function () {
        if (index.nowPage == 1) {
          screen.playVideo();
        } else {
          clearTimeout(timeout);
        }
      }, 2000);
    });
  },
  addAlarm: function (obj) {
    var imgTd = "-";
    // if(obj.alarmType>=5&&obj.alarmType<=9){
    // 	imgTd="<img src='assets/img/pic.png' onclick=screen.showPic('backup/block/"+obj.img+"') />";
    // }

    if (obj.img) {
      imgTd =
        "<img src='assets/img/pic.png' onclick=screen.showPic('backup/block/" +
        obj.img +
        "') />";
    }
    var html =
      "<tr id='" +
      obj.id +
      "'><td>" +
      obj.devName +
      "</td><td>" +
      obj.alarmTypeZH +
      "</td>" +
      "<td>" +
      obj.severityZH +
      "</td>" +
      "<td>" +
      obj.alarmTime +
      "</td>" +
      "<td>" +
      imgTd +
      "</td></tr>";
    if ($("#activeT").children().length >= 5) {
      var tr = $("#activeT").children(":first");
      tr.remove();
    }
    $("#activeT").append(html);
  },
  addHistoryAlarm: function (obj) {
    let activeAlarmTr = $("#" + obj.activeAlarmId);
    if (activeAlarmTr) {
      activeAlarmTr.remove();
    }
    var imgTd = "-";
    if (obj.alarmType >= 5 && obj.alarmType <= 9) {
      imgTd =
        "<img src='assets/img/pic.png' onclick=screen.showPic('backup/block/" +
        obj.img +
        "') />";
    }
    var html =
      "<tr><td>" +
      obj.devName +
      "</td><td>" +
      obj.alarmTypeZH +
      "</td>" +
      "<td>" +
      obj.severityZH +
      "</td>" +
      "<td>" +
      obj.alarmTime +
      "</td>" +
      "<td>" +
      obj.restoreTime +
      "</td>" +
      "<td>" +
      imgTd +
      "</td></tr>";
    if ($("#historyT").children().length >= 5) {
      var tr = $("#historyT").children(":first");
      tr.remove();
    }
    $("#historyT").append(html);
  },
  addEleDegree: function (objs) {
    $.each(objs, function (devId, value) {
      //折线图
      chart.addChartData(devId, value);
      $("#h" + devId).text(value.hour + "hours");
    });
  },

  /*
  addEleDegree: function (objs, time) {
    $.each(objs, function (devId, typeValues) {
      //折线图
      chart.setChartData(devId, typeValues, time);
    });
  },
  */
  timeUpdate: function () {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = "00" + date.getHours();
    hour = hour.substr(hour.length - 2);
    var minute = "00" + date.getMinutes();
    minute = minute.substr(minute.length - 2);
    var second = "00" + date.getSeconds();
    second = second.substr(second.length - 2);

    $("#timeSpan").text(
      year +
        "-" +
        month +
        "-" +
        day +
        " " +
        " " +
        hour +
        ":" +
        minute +
        ":" +
        second
    );
    setTimeout("screen.timeUpdate()", 1000); //设置过1000毫秒就是1秒，调用show方法
  },
  initAlarm: function () {
    $.get(
      "/common/findPaging.do?tbl=activealarm&page=1&limit=10",
      function (data, status) {
        var objs = data.objs;
        for (var i = 0; i < objs.length; i++) {
          var obj = objs[i];
          screen.addAlarm(objs[i]);
        }
      }
    );
    $.get(
      "/common/findPaging.do?tbl=historyalarm&page=1&limit=10",
      function (data, status) {
        var objs = data.objs;
        for (var i = 0; i < objs.length; i++) {
          var obj = objs[i];
          screen.addHistoryAlarm(objs[i]);
        }
      }
    );
  },
  initChart: function () {
    for (var i = 0; i < screen.chartdevs.length; i++) {
      var dev = screen.chartdevs[i];
      var html =
        "<div class='layui-row' id=" +
        "title" +
        dev.id +
        "' style='margin-top:18px;height:25px;'>" +
        "</div>";
      html =
        html +
        "<div class='layui-col-md4 dev'>" +
        "<span " +
        "' style='margin-left:20px;'>" +
        dev.devName +
        "</span>" +
        "</div>";
      html =
        html +
        "<div class='layui-col-md8 hours'>" +
        "<span id='h" +
        dev.id +
        "'>" +
        "&nbsp;" +
        "</span>" +
        "</div>";
      /*
      html =
        html +
        "<div class='layui-col-md6'>" +
        "<div id=" +
        dev.id +
        " class='clearTime'>" +
        "</div>" +
        "</div>";
      */
      html =
        html +
        "<div id='chart" +
        dev.id +
        "' style='margin-top:24px;height:180px;'></div>";
      $("#chartDivs").append(html);
      chart.setchart(dev);
    }
  },
  logout: function () {
    //退出
    var r = confirm("您确定要退出?");
    if (r == true) {
      $.get("/user/logout.do", function (data, status) {
        if (data.success) {
          // 转到登录页
          sessionStorage.removeItem("userName");
          sessionStorage.removeItem("role");
          window.location.href = "/";
        }
      });
    }
  },
  playVideo: function () {
    $.get(
      "/camera/findPaging.do?page=1&limit=9&showIndex=>,0",
      function (data, status) {
        if (data.success) {
          screen.clearPlay();
          for (var i = 0; i < data.objs.length; i++) {
            var camera = data.objs[i];
            let nvr = {
              ip: camera.devIp,
              port: camera.devPort,
              userName: camera.userName,
              password: camera.password,
            };
            nvr.channel = camera.channel;
            nvr.name = camera.name;
            document
              .getElementById("vframe" + camera.showIndex)
              .contentWindow.play(nvr);
            $("#v" + camera.showIndex + "v").text(nvr.name);
          }
        }
      }
    );
  },
  clearPlay: function () {
    for (var i = 1; i <= 9; i++) {
      if ($("#v" + i + "v")) {
        document.getElementById("vframe" + i).contentWindow.stopPlay();
        $("#v" + i + "v").text("--");
      }
    }
  },
  showPic: function (img) {
    $("#imgBig").attr("src", img);
    $("#alarmPic").fadeOut();
    $("#alarmPic").fadeIn();
  },
  toCfg: function () {
    window.location.href = "/cfg/home.html";
  },
  toDetail: function () {
    $("#alarmPic").fadeOut();
    let img = $("#imgBig")[0].src;
    img = img.split("block/")[1];
    $.get("/alarm/findBelt.do?img=" + img, function (data, status) {
      if (data.success) {
        sessionStorage.setItem("beltId", data.beltId);
        index.nowPage = 2;
        screen.clear();
        index.toPath("selfDetail");
      }
    });
  },
  toInspect: function () {
    $("#mainDiv").load("page1.html", function (responseTxt, statusTxt, xhr) {
      if (statusTxt == "success") {
        index.nowPage = 2;
        screen.clear();
        //page1.init();
      }
    });
    //删除定时任务
    page6.removeInterval();
  },
  toPage5: function () {
    $("#mainDiv").load("page5.html", function (responseTxt, statusTxt, xhr) {
      if (statusTxt == "success") {
        index.nowPage = 5;
        screen.clear();
        page5.init();
      }
    });
  },
};
