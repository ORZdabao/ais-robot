/**
 * 折线图.
 * @type
 */
var chart = {
  myCharts: {},
  clear: function () {
    chart.myCharts = {};
  },
  setchart: function (dev) {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById("chart" + dev.id));
    myChart.clear();
    var dateData = [
      "0:00",
      "0:00",
      "0:00",
      "0:00",
      "0:00",
      "0:00",
      "0:00",
      "0:00",
    ];
    var colors = [
      "#FF4949",
      "#FFA74D",
      "#DC143C",
      "#008B8B",
      "#DAA520",
      "#32CD32",
      "#1E90FF",
    ];
    let types = dev.types;
    var chartCfg = { chart: myChart, dateData: dateData };
    let legendData = [];
    let color = [];
    let series = [];
    for (let i = 0; i < types.length; i++) {
      chartCfg[types[i] + "data"] = [0, 0, 0, 0, 0, 0, 0, 0];
      legendData.push(chart.getTypeZH(types[i]));
      color.push(colors[i]);
      series.push({
        name: chart.getTypeZH(types[i]),
        type: "line",
        data: chartCfg[types[i] + "data"],
      });
    }
    chart.myCharts[dev.id] = chartCfg;
    var option = {
      title: {
        text: "",
      },
      tooltip: {
        trigger: "axis",
      },
      //   legend: {
      //     data: legendData,
      //     textStyle: {
      //       color: "#fff",
      //     },
      //     top: "5%",
      //   },
      grid: {
        top: "30%",
        left: "3%",
        right: "3%",
        bottom: "3%",
        containLabel: true,
      },
      color: color,
      xAxis: {
        type: "category",
        boundaryGap: false,
        data: dateData,
        splitLine: {
          show: false,
        },
        axisLine: {
          lineStyle: {
            color: "#fff",
          },
        },
      },
      yAxis: {
        //name: dev.devName,
        type: "value",
        splitLine: {
          show: false,
        },
        axisLine: {
          lineStyle: {
            color: "#fff",
          },
        },
      },
      series: series,
    };
    myChart.setOption(option);
  },
  setChartData: function (devId, data) {
    var myChart = chart.myCharts[devId];
    var xData = data.xseries.map(function (item) {
      return item.slice(11);
    });
    myChart.dateData = xData;
    let series = [];
    series.push({
      name: "电流",
      type: "line",
      data: data.yseries,
    });

    /*
    $.each(data.yseries, function (index, value) {
      series.push({
        name: "电流",
        type: "line",
        data: "23",
      });
    });
    */
    myChart.chart.setOption({
      xAxis: {
        data: myChart.dateData,
      },
      series: series,
    });
    myChart["0data"] = data.yseries;
  },
  addChartData: function (devId, data) {
    var myChart = chart.myCharts[devId];
    myChart.dateData.shift();
    myChart.dateData.push(data.xSeries.slice(11));
    //折线图
    myChart["0data"].shift();
    myChart["0data"].push(data.ySeries);
    let series = [];
    series.push({
      name: "电流",
      type: "line",
      data: myChart["0data"],
    });
    myChart.chart.setOption({
      xAxis: {
        data: myChart.dateData,
      },
      series: series,
    });
  },

  /*
  setChartData: function (devId, typeValues, time) {
    var myChart = chart.myCharts[devId];
    myChart.dateData.shift();
    myChart.dateData.push(time);

    let series = [];
    $.each(typeValues, function (type, v) {
      //折线图
      myChart[type + "data"].shift();
      myChart[type + "data"].push(v);
      series.push({
        name: chart.getTypeZH(type),
        type: "line",
        data: myChart[type + "data"],
      });
    });
    myChart.chart.setOption({
      xAxis: {
        data: myChart.dateData,
      },
      series: series,
    });
  },
  */
  getTypeZH: function (type) {
    if (type == 0) {
      return "电流";
    }
    if (type == 1) {
      return "取料电流";
    }
    if (type == 2) {
      return "堆料电流";
    }
    if (type == 3) {
      return "电度";
    }
    if (type == 4) {
      return "温度1";
    }
    if (type == 5) {
      return "温度2";
    }
    if (type == 6) {
      return "秤瞬时流量";
    }
    if (type == 7) {
      return "电机非驱动端温度";
    }
    if (type == 8) {
      return "电机驱动端温度";
    }
    if (type == 9) {
      return "非驱动端温度";
    }
    if (type == 10) {
      return "驱动端温度";
    }
    return null;
  },
};
