var page6={
	intervalId:"",
	init:function(){
		$.get("/coalFeeder/findAll.do",function(data,status){
			var myChartArr = [];
			var optionArr = [];
			if(data.success){
				var coalFeeders=data.objs;
				for(var coalFeeder in coalFeeders){
					if(coalFeeder>3)
						break;
					var myChart = echarts.init(document.getElementById("coalFeeder"+coalFeeder));
					var option = page6.initCharts(coalFeeders[coalFeeder]);
					myChart.setOption(option);
					//初始化字段
					$("#auto"+coalFeeder).html("1"==coalFeeders[coalFeeder].auto?"自动模式":"手动模式");
					$("#coalfeederid"+coalFeeder).val(coalFeeders[coalFeeder].id);
					myChartArr[coalFeeder] = myChart;
					optionArr[coalFeeder] = option;
				}
				page6.getModBusData(myChartArr,optionArr);
				page6.intervalId = setInterval(function(){
					$.get("/coalFeeder/getModBusDatas.do",function(datas){
						if(datas!=null&&datas!=undefined){
							for(var data in datas){
								$('input[id^="coalfeederid"][value='+datas[data].id+']').nextAll().filter("span[id^=beltA]").html(datas[data].beltA);
								$('input[id^="coalfeederid"][value='+datas[data].id+']').nextAll().filter("span[id^=HZ]").html(datas[data].coalfeederHZ);
								optionArr[data].series[0].data[0].value = datas[data].coalfeederHZ+"";
								myChartArr[data].setOption(optionArr[data]);
							}

						}else{
							//toastr.warning("modbus取值失败");
						}

					});
				},5000);
			}
		});

	},
	getModBusData:function(myChartArr,optionArr){
		$.get("/coalFeeder/getModBusDatas.do",function(datas){
			if(datas!=null&&datas!=undefined){
				for(var data in datas){
					$('input[id^="coalfeederid"][value='+datas[data].id+']').nextAll().filter("span[id^=beltA]").html(datas[data].beltA);
					$('input[id^="coalfeederid"][value='+datas[data].id+']').nextAll().filter("span[id^=HZ]").html(datas[data].coalfeederHZ);
					optionArr[data].series[0].data[0].value = datas[data].coalfeederHZ+"";
					myChartArr[data].setOption(optionArr[data]);
				}

			}else{
				//toastr.warning("modbus取值失败");
			}

		});
	},
	initCharts:function(item){
		return option = {
			title: [{
				x:'17',
				top: '21',
				text: item.coalfeedername,
				textStyle: {
					fontWeight: 'normal',
					fontSize: '16',
					color: "#fff"
				},
			}],
			series : [{
				name:'HZ',
				type:'gauge',
				min:0,
				max:80,
				center: ['50%', '60%'], // 默认全局居中
				radius: '85%',
				axisLine: {            // 坐标轴线
					lineStyle: {       // 属性lineStyle控制线条样式
						color: [[0.2, '#77D97F'],[0.8, '#4285F4'],[1, '#FF7E7F']],
						width: 15,
						shadowColor : '#fff', //默认透明
					}
				},
				axisLabel: {            // 坐标轴小标记
					textStyle: {       // 属性lineStyle控制线条样式
						fontWeight: 'bolder',
						fontSize:20,
					},

				},
				axisTick: {            // 仪表盘的线条
					length :20,        // 属性length控制线长
					lineStyle: {       // 属性lineStyle控制线条样式
						color: 'auto',
						width:2,
					},
					splitNumber: 10,
				},
				splitLine:{
					//最长的刻度线
					length:25,
					lineStyle:{
						color: 'auto',
					}
				},
				itemStyle:{//指针颜色
					color:'#77D97F',
				},
				pointer:{//指针长短
					length:130
				},
				detail: {
					formatter:'{value}',
				},
				data:[{value: "0", name: 'HZ'}]
			}
			]
		};
	},
	divBtnEXE:function(no,type){
		if(type=="2"){
			$.get("/coalFeeder/manualAdjustCoalFeeder.do?coalFeederIds="+$("#coalfeederid"+no).val(),function(data,status){
				if(data=="手动模式设置成功"){
					$("#auto"+no).html("手动模式");
					toastr.success(data);
				}else{
					toastr.warning(data);
				}
			});
		}else{
			$.get("/coalFeeder/autoAdjustCoalFeeder.do?coalFeederIds="+$("#coalfeederid"+no).val(),function(data,status){
				if(data=="自动模式设置成功"){
					$("#auto"+no).html("自动模式");
					toastr.success(data);
				}else{
					toastr.warning(data);
				}
			});
		}
	},
	removeInterval:function(){
		window.clearInterval(page6.intervalId);
	}
};