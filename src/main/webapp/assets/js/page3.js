var page3={
	init:function(){
		let timeout=setTimeout(function(){
			for(var i=1;i<=2;i++){
				document.getElementById("vframe"+i).contentWindow.realPlayDiv.style.height="525px";
			}
			$.get("/page3/belt.do",function(data,status){
				if(data.success){
					let belts=data.belts;
					let html="";
					for(let i=0;i<belts.length;i++){
						html+="<span style='cursor:pointer' id=belt"+belts[i].id+" onclick=page3.playVideo('"+belts[i].id+"')>"+belts[i].name+"</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}
					$("#beltDiv").html(html);

					if(belts.length>0){
						page3.playVideo(belts[0].id);
					}
				}
			});
		},2000);
		for(let i=0;i<8;i++){
			let html="<tr><td>&nbsp;</td></tr>";
			$("#page3Table").append(html);
		}
	},
	playVideo:function(beltId){
		$("#beltDiv span").css("color","#FFF");
		$("#belt"+beltId).css("color","#FFFF00");

		$.get("/page3/camera.do?beltId="+beltId,function(data,status){
			if(data.success){
				page3.clearPlay();
				for(var i=0;i<data.objs.length&&i<2;i++){
					var camera=data.objs[i];
					let nvr={ip:camera.devIp,port:camera.devPort,userName:camera.userName,password:camera.password};
					nvr.channel=camera.channel;
					nvr.name=camera.name;
					document.getElementById("vframe"+(i+1)).contentWindow.play(nvr);
					$("#v"+(i+1)+"v").text(nvr.name);
				}
			}
		});
	},
	clearPlay:function(){
		for(var i=1;i<=2;i++){
			if($("#v"+i+"v")){
				document.getElementById("vframe"+i).contentWindow.stopPlay();
				$("#v"+i+"v").text("--");
			}
		}
	},
	addPoll:function(info){
		let trs=$("#page3Table").find("tr");
		let html="<td style='color:#FFF'>"+info.startTime+"</td><td style='color:#FFF'>"+info.endTime+"</td>";
		if(info.isOK==0){
			html+="<td style='color:#FFF'>无异常</td>";
		}else{
			html+="<td style='color:#FF0000'>有异常</td>";
		}
		for(let i=2;i<trs.length;i++){
			if(trs[i].innerText.trim()==""){
				trs[i].innerHTML=html;
				return;
			}
		}
		trs[2].remove();
		$("#page3Table").append("<tr>"+html+"</tr>");
	},
	divBtnEXE:function(flag){
		if(flag==5){
			index.indexvue.autoOnCoalDialogVisible=true;
		}else{
			$.get("/selfInspect/divbtn.do?flag="+flag,function(data,status){
				toastr.warning(data.error);
				page3.init();
			});
		}
	},
};