var index = {
  indexvue: null,
  nowPage: 0,
  websocket: null,
  init: function () {
    if (sessionStorage.getItem("back") == 1) {
      //从后台管理系统进入
      sessionStorage.removeItem("back");
      index.toPath("screen");
    } else {
      $("#mainDiv").load("page1.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
          index.nowPage = 2;
          //setTimeout('page1.init()', 100); //延迟1秒
        }
      });
    }

    index.initWebSocket();
    $("#alarmPicBtn").click(function () {
      $("#alarmPic").fadeOut();
      let img = $("#imgBig")[0].src;
      img = img.split("block/")[1];
      $.get("/alarm/restoreByImg.do?img=" + img, function (data, status) {
        if (data.success) {
        }
      });
    });
    $("#pendPicBtn").click(function () {
      $("#alarmPic").fadeOut();
    });
  },
  initWebSocket: function () {
    // 判断当前浏览器是否支持WebSocket
    if ("WebSocket" in window) {
      index.websocket = new ReconnectingWebSocket(
        "ws://" + window.location.host + "/ws"
      );
    } else {
      alert("当前浏览器不支持WebSocket");
      return;
    }
    index.websocket.onopen = function (event) {
      index.websocket.send("userName=zs");
    };
    index.websocket.onclose = function () {};
    index.websocket.onmessage = function (event) {
      var data = JSON.parse(event.data);

      if (data.type == 1) {
        // 服务器时间信息
        if (index.nowPage == 1) {
          // $("#timeSpan").text(data.time);
        }
      } else if (data.type == 2) {
        //声音告警
        let alarmType = data.alarm.alarmType;
        if (data.alarm.audio) {
          document.getElementById("audio").src =
            "assets/wav/" + alarmType + "/" + data.alarm.audio;
          document.getElementById("audio").play();
        }
        if (index.nowPage != 2) {
          index.indexvue.$notify({
            title: data.alarm.alarmTypeZH,
            duration: 10000,
            dangerouslyUseHTMLString: true,
            message: data.info,
            type: "warning",
            position: "bottom-right",
          });
        }
        //图片告警
        if (index.nowPage == 1) {
          //screen.showPic("backup/block/"+data.alarm.img);
          if (!data.alarm.repeat) {
            screen.addAlarm(data.alarm);
          }
        } else if (index.nowPage == 4) {
          page4.chart1();
          page4.chart3();
        } else if (index.nowPage == 5) {
          page5.chart2();
        } else if (index.nowPage == 11) {
          page11.addAlarm(data.alarm);
        }
      } else if (data.type == 5) {
        //告警恢复
        if (index.nowPage == 1) {
          screen.addHistoryAlarm(data.alarm);
        } else if (index.nowPage == 4) {
          page4.chart1();
          page4.chart3();
        } else if (index.nowPage == 5) {
          page5.chart2();
        }
      } else if (data.type == 3) {
        console.log(data.objs);
        if (index.nowPage == 1) {
          screen.addEleDegree(data.objs);
          //screen.addEleDegree(data.objs, data.time);
        }
      } else if (data.type == 4) {
        if (index.nowPage == 1) {
          //料位都高于10米
          document.getElementById("audio").src = "assets/wav/10/仓位过高.wav";
          document.getElementById("audio").play();
          index.indexvue.$notify({
            title: "煤仓料位",
            duration: 10000,
            dangerouslyUseHTMLString: true,
            message: data.info,
            type: "warning",
            position: "bottom-right",
          });
        }
      } else if (data.type == 6) {
        //自动巡检结果
        if ($("#page3Table")[0]) {
          page3.addPoll(data.info);
        }
      } else if (data.type == 7) {
        //机器人预置位改变
        if (index.nowPage == 4) {
          page4.chart4();
        }
      } else if (data.type == 8) {
        //煤仓料位
        if (index.nowPage == 5) {
          page5.addBunkerML(data.bunkerMLs, data.time);
        }
      }
    };
  },
  cameraSelection: function () {
    index.indexvue.cameraDialogVisible = true;
  },
  modifyPwd: function () {
    index.indexvue.passDialogVisible = true;
  },
  toPath: function (path) {
    if (path == "screen") {
      $("#mainDiv").load("screen.html", function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
          index.nowPage = 1;
          screen.init();
        }
      });
    } else {
      $("#mainDiv").load(
        path + ".html",
        function (responseTxt, statusTxt, xhr) {
          console.log(path);
          console.log(statusTxt);
          if (statusTxt == "success") {
            //index.nowPage=2;
            if (path == "page1") {
              index.nowPage = 2;
              //page1.init();
            } else if (path == "page2") {
              index.nowPage = 12;
              //page2.init();
            } else if (path == "page3") {
              page3.init();
            } else if (path == "page4") {
              page4.init();
              index.nowPage = 4;
            } else if (path == "page5") {
              page5.init();
              index.nowPage = 5;
            } else if (path == "page6") {
              page6.init();
              index.nowPage = 6;
            } else if (path == "page7") {
              page7.init();
              index.nowPage = 7;
            } else if (path == "page99") {
              //page99.init();
              index.nowPage = 99;
            } else if (path == "selfInspect") {
              index.nowPage = 11;
            }
          }
        }
      );
    }
  },
};
