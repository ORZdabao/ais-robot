var page2={
	init:function(){
		$.get("/page2/chart1.do",function(data,status){
			if(data.success){
				page2.chart1(data.nums);
			}
		});
		
		$.get("/page2/chart2.do",function(data,status){
			if(data.success){
				page2.chart2(data.obj);
			}
		});
	},
	chart1:function(nums){
		$.each(nums,function(k,v){
			if($("#alarm"+k)[0]){
				$("#alarm"+k).text(v);
			}
		});
	},
	chart2:function(obj){
		let alarmTypes=["电流过高","撕裂左","撕裂右","堵煤","异物","跑偏"];
		let colors=["#16A1B4","#26A645","#FCC00A","#DD3648","#5E72E8","#FF2FA0"];
		let series=[];
		for(let i=0;i<alarmTypes.length;i++){
			let serie={name:alarmTypes[i],type:"bar",stack:"实时告警",color:colors[i],
				label:{show:true,position:"insideRight"},
				data:obj.alarmNums[i]};
			series.push(serie);
		}
		let option={
			tooltip:{
				trigger:'axis',
				axisPointer:{type:'shadow'}
			},
			legend:{data:alarmTypes,textStyle:{color:"#fff",fontSize:18}},
			grid:{left:'3%',right:'4%',bottom:'3%',containLabel:true},
			xAxis:{type:'value',axisLabel:{textStyle:{color:"#fff",fontSize:15}},axisLine:{
				lineStyle:{type:'solid',color:'#fff',width:'1'}}},
			yAxis:{type:'category',data:obj.names,axisLabel:{margin:20,textStyle:{color:"#fff",fontSize:15}},
				axisLine:{lineStyle:{type:'solid',color:'#fff',width:'1'}}},
			series:series
		};
		let pchart=echarts.init(document.getElementById("pchart"),"shine");
		pchart.setOption(option);
		pchart.on("click",function(params){
			//page2.chart3(params.name);
		});
	},
	chart3:function(name){
		$("#aName").text(name+"统计");
		$.ajax({
			url:"page2/chart3.do",
			data:{beltName:name},
			success:function(data){
				if(data.success){
					$.each(data.obj,function(k,v){
						for(let i=0;i<v.length;i++){
							$("#a"+k+i).text(v[i]);
						}
					});
				}
			}
		});
	}
};