var page1={
	init:function(){
		$.get("/page1/chart1.do",function(data,status){
			if(data.success){
				page1.chart1(data.nums);
			}
		});
		$.get("/page1/chart2.do",function(data,status){
			if(data.success){
				page1.chart2(data.nums);
			}
		});
		$.get("/page1/chart3.do",function(data,status){
			if(data.success){
				page1.chart3(data.nums);
			}
		});
		$.get("/page1/chart4.do",function(data,status){
			if(data.success){
				page1.chart4(data.nums);
			}
		});
		$.get("/common/findPaging.do?tbl=activealarm&page=1&limit=8",function(data,status){
			var objs=data.objs;
			for(let i=0;i<objs.length;i++){
				let obj=objs[i];
				page1.chart5(objs[i]);
			}
			let n=8-objs.length;
			for(let i=0;i<n;i++){
				$("#pchart5").append("<tr><td colspan=5>&nbsp;</td></tr>");
			}
		});


		$.get("/page1/chart6.do",function(data,status){
			if(data.success){
				page1.chart6(data.nums);
			}
		});
	},
	chart1:function(obj){
		let colors=["#3399FF","#FFCC00","#66CDBB","#0f7cc0","#9cceee"];
		let kinds={1:"皮带",2:"摄像头",3:"机器人",4:"给煤机",5:"空气炮"};
		let data=[];
		$.each(obj,function(k,v){
			data.push({value:v,name:kinds[k]});
		});
		let option={
			tooltip:{trigger:"item",formatter:"{b} : {c} ({d}%)"},
			color:colors,
			series:[{
				type:"pie",
				radius:"80%",
				center:["50%","50%"],
				data:data,
				itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0},
					normal:{label:{show:true,formatter:"{b} : {c} ({d}%)"}}
				}}]
		};
		let devTypePie=echarts.init(document.getElementById("pchart1"),"shine");
		devTypePie.setOption(option);
	},
	chart2:function(obj){
		let option={
			title:{text:'实时告警类型',itemGap:20,textStyle:{color:"#fff"},subtext:""},
			tooltip:{trigger:"axis",axisPointer:{type:"shadow"}},
			grid:{top:"4%",left:"2%",right:"2%",bottom:"0%",containLabel:true},
			xAxis:[{min:0,splitNumber:10,type:"value",axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
					}},axisLabel:{textStyle:{color:"#fff"}}}],
			yAxis:[{type:"category",data:["电流过高","撕裂左","撕裂右","堵煤","异物","跑偏"],
				axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
					}},axisTick:{alignWithLabel:true},axisLabel:{textStyle:{color:"#fff"}}}],
			series:[{
				type:"bar",
				barWidth:"30%",
				itemStyle:{
					normal:{
						color:function(params){
							var colorList=["#CCFFCC","#CC33CC","#FFCC00","#33FFCC","#00FF00","#FFFF99"];
							return colorList[params.dataIndex];
						}
					}
				},
				label:{normal:{show:true,position:"top"}},
				data:obj
			}]
		};
		let pchart2=echarts.init(document.getElementById("pchart2"),"shine");
		pchart2.setOption(option);
	},
	chart3:function(obj){
		let colors=["#FF3333","#FFCC00","#33FF00"];
		let kinds={1:"一级",2:"二级",3:"三级"};
		let data=[];
		$.each(obj,function(k,v){
			data.push({value:v,name:kinds[k]});
		});
		let option={
			tooltip:{
				trigger:'item',
				formatter:"{b} : {c} ({d}%)"
			},
			color:colors,
			legend:{
				orient:'vertical',
				top:10,
				left:10,
				bottom:10,
				data:['一级','二级','三级'],
				textStyle:{color:"#fff"}
			},
			series:[{
				type:'pie',
				radius:['50%','70%'],
				center:['50%','50%'],
//				center:["70%","80%"],
				avoidLabelOverlap:false,
				label:{show:false,position:'center'},
				emphasis:{
					label:{show:true,fontSize:'30',fontWeight:'bold'}
				},
				labelLine:{show:false},
				data:data
			}]
		};
		let pchart3=echarts.init(document.getElementById("pchart3"));
		pchart3.setOption(option);
	},
	chart4:function(obj){
		let option={
			tooltip:{trigger:"axis",axisPointer:{type:"shadow"}},
			grid:{top:"7%",left:"2%",right:"2%",bottom:"0%",containLabel:true},
			xAxis:[{type:"category",data:["电流过高","撕裂左","撕裂右","堵煤","异物","跑偏"],
				axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
					}},axisTick:{alignWithLabel:true},axisLabel:{textStyle:{color:"#fff"}}}],
			yAxis:[{min:0,splitNumber:10,type:"value",axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
					}},axisLabel:{textStyle:{color:"#fff"}}}],
			series:[{
				type:"bar",
				barWidth:"30%",
				itemStyle:{
					normal:{
						color:function(params){
							var colorList=["#CCFFCC","#CC33CC","#FFCC00","#33FFCC","#00FF00","#FFFF99"];
							return colorList[params.dataIndex];
						}
					}
				},
				label:{normal:{show:true,position:"top"}},
				data:obj
			}]
		};
		let pchart4=echarts.init(document.getElementById("pchart4"),"shine");
		pchart4.setOption(option);
	},
	chart5:function(obj){
		var imgTd="-";
		if(obj.alarmType>=5&&obj.alarmType<=9){
			imgTd="<img src='assets/img/pic.png' onclick=screen.showPic('backup/block/"+obj.img+"') style='cursor:pointer;'/>";
		}
		var html="<tr id='"+obj.id+"'><td align=center>"+obj.devName+"</td><td align=center>"+obj.alarmTypeZH+"</td>" +
			"<td align=center>"+obj.severityZH+"</td>"+
			"<td align=center>"+obj.alarmTime+"</td>" +
			"<td align=center>"+imgTd+"</td></tr>";
		$("#pchart5").append(html);
	},
	chart6:function(obj){
		let colors=["#FF3333","#FFCC00","#33FF00"];
		let kinds={1:"一级",2:"二级",3:"三级"};
		let data=[];
		$.each(obj,function(k,v){
			data.push({value:v,name:kinds[k]});
		});
		let option={
			tooltip:{
				trigger:'item',
				formatter:"{b} : {c} ({d}%)"
			},
			color:colors,
			legend:{
				orient:'vertical',
				top:10,
				left:10,
				bottom:10,
				data:['一级','二级','三级'],
				textStyle:{color:"#fff"}
			},
			series:[{
				type:'pie',
				radius:['50%','70%'],
				center:['50%','50%'],
//				center:["70%","80%"],
				avoidLabelOverlap:false,
				label:{show:false,position:'center'},
				emphasis:{
					label:{show:true,fontSize:'30',fontWeight:'bold'}
				},
				labelLine:{show:false},
				data:data
			}]
		};
		let pchart6=echarts.init(document.getElementById("pchart6"));
		pchart6.setOption(option);
	}
};