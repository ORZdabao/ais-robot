var service=axios.create({
  //baseURL: "http://localhost:8080/",
  timeout: 60000
});

// request interceptor
service.interceptors.request.use(
  function (config) {
    //开启等待
   // $("#wait").attr("class", "wait fadeIn");
    return config;
  },
  function(error) {
    //结束等待
   // wait.className="wait";
    //return Promise.reject(error);
    return null;
  }
);

service.interceptors.response.use(
  function (response) {
    //结束等待
   // $("#wait").attr("class","wait");
  	if(!response){
  		return null;
  	}
    if(response.data.success){
      if(response.data.error){
    	//Vue.prototype.$message.success(response.data.error);
      }
      return response.data;
    }else{
    	Vue.prototype.$message.error(response.data.error);
    }
    return response.data;
  },
  function (error) {
    //结束等待
   // $("#wait").attr("class","wait");
    Vue.prototype.$message.error(error.message);
    if (error.response){
      if(error.response.status===401){
        window.location.href = "/";
      }
    }
    return Promise.reject(error);
  }
);
