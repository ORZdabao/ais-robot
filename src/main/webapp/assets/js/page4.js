var page4={
	init:function(){
		page4.chart1();
		
		$.get("/page4/chart2.do",function(data,status){
			if(data.success){
				page4.chart2({1:data.avg1,2:data.avg0,3:data.avg2});
			}
		});
		
		
		let html="";
		for(let i=0;i<8;i++){
			html+="<tr><td>&nbsp;</td><td></td><td></td></tr>";
		}
		$("#pchart3").append(html);
		page4.chart3();
		
		html="";
		for(let i=0;i<8;i++){
			html+="<tr><td>&nbsp;</td><td></td><td></td></tr>";
		}
		$("#pchart4").append(html);
		page4.chart4();
		
		$.get("/page4/chart5.do",function(data,status){
			if(data.success){
				page4.chart5(data.times,data.vals);
			}
		});
		
		$.get("/page4/chart6.do",function(data,status){
			if(data.success){
				page4.chart6(data.times,data.vals);
			}
		});
	},
	chart1:function(){
		$.get("/page4/chart1.do",function(data,status){
			if(data.success){
				$("#1n1").text(data["1n1"]);
				$("#1n2").text(data["1n2"]);
				$("#1n3").text(data["1n1"]+data["1n2"]);
				
				$("#2n1").text(data["2n1"]);
				$("#2n2").text(data["2n2"]);
				$("#2n3").text(data["2n1"]+data["2n2"]);
				
				$("#3n1").text(data["3n1"]);
				$("#3n2").text(data["3n2"]);
				$("#3n3").text(data["3n1"]+data["3n2"]);
				
				$("#4n1").text(data["4n1"]);
				$("#4n2").text(data["4n2"]);
				$("#4n3").text(data["4n1"]+data["4n2"]);
			}
		});
	},
	chart2:function(obj){
		let colors=["#FF3333","#FFCC00","#33FF00"];
		let kinds={1:"平均在线时间(h)",2:"平均离线时间(h)",3:"平均在线率(%)"};
		let data=[];
		$.each(obj,function(k,v){
			data.push({value:v,name:kinds[k]});
		});
		let option={
			tooltip:{
				trigger:'item',
				formatter:"{b} : {c}"
			},
			color:colors,
			legend:{
				orient:'vertical',
				top:10,
				left:10,
				bottom:10,
				data:['平均在线时间(h)','平均离线时间(h)','平均在线率(%)'],
				textStyle:{color:"#fff"}
			},
			series:[{
				type:'pie',
				radius:['50%','70%'],
				center:['50%','50%'],
//				center:["70%","80%"],
				avoidLabelOverlap:false,
				label:{show:false,position:'center'},
				emphasis:{
					label:{show:true,fontSize:'30',fontWeight:'bold'}
				},
				labelLine:{show:false},
				data:data
			}]
		};
		let pchart2=echarts.init(document.getElementById("pchart2"));
		pchart2.setOption(option);
	},
	chart3:function(){
		$.get("/page4/chart3.do",function(data,status){
			if(data.success){
				let html="<tr><td align='center'><b>皮带编号</b></td><td align='center'><b>状态</b></td><td align='center'><b>时间</b></td></tr>";
				for(let i=0;i<data.objs.length;i++){
					let obj=data.objs[i];
					html+="<tr><td align=center>"+obj.devName+"</td><td align=center>"+obj.note+"</td>" +
					"<td align=center>"+obj.time+"</td></tr>";
				}
				$("#pchart3").html(html);
			}
		});
	},
	chart4:function(){
		$.get("/page4/chart4.do",function(data,status){
			if(data.success){
				let html="<tr><td align='center'><b>机器人</b></td><td align='center'><b>位置</b></td><td align='center'><b>时间</b></td></tr>";
				for(let i=0;i<data.objs.length;i++){
					let obj=data.objs[i];
					html+="<tr><td align=center>"+obj.devName+"</td><td align=center>"+obj.note+"</td>" +
					"<td align=center>"+obj.time+"</td></tr>";
				}
				let j=10-data.objs.length;
				if(j>0){
					for(let m=0;m<j;m++){
						html+="<tr><td>&nbsp;</td><td></td><td></td></tr>";
					}
				}
				$("#pchart4").html(html);
			}
		});
	},
	chart5:function(times,vals){
		let option={
			tooltip:{trigger:"axis",axisPointer:{type:"shadow"}},
			grid:{top:"7%",left:"2%",right:"2%",bottom:"0%",containLabel:true},
			xAxis:[{type:"category",data:times,
				axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
            }},axisTick:{alignWithLabel:true},axisLabel:{textStyle:{color:"#fff"}}}],
			yAxis:[{min:0,splitNumber:10,type:"value",axisLine:{
				lineStyle:{type:'solid',color:'#fff',width:'1'
	            }},axisLabel:{textStyle:{color:"#fff"}}}],
			series:[{
				type:"bar",
				barWidth:"30%",
				itemStyle:{
					normal:{
						color:function(params){
							var colorList=["#CCFFCC","#CC33CC","#FFCC00","#33FFCC","#00FF00","#FFFF99","#0099CC","#6666CC","#999966","#333366","#FFCCCC","#006633"];	
							return colorList[params.dataIndex];
						}
					}
				},
				label:{normal:{show:true,position:"top"}},
				data:vals
			 }]
		  };
		  let pchart5=echarts.init(document.getElementById("pchart5"),"shine");
		  pchart5.setOption(option);
	},
	chart6:function(times,vals){
		let option={
			tooltip:{trigger:"axis",axisPointer:{type:"shadow"}},
			grid:{top:"7%",left:"2%",right:"2%",bottom:"0%",containLabel:true},
			xAxis:[{type:"category",data:times,
				axisLine:{
					lineStyle:{type:'solid',color:'#fff',width:'1'
            }},axisTick:{alignWithLabel:true},axisLabel:{textStyle:{color:"#fff"}}}],
			yAxis:[{min:0,splitNumber:10,type:"value",axisLine:{
				lineStyle:{type:'solid',color:'#fff',width:'1'
	            }},axisLabel:{textStyle:{color:"#fff"}}}],
			series:[{
				type:"bar",
				barWidth:"30%",
				itemStyle:{
					normal:{
						color:function(params){
							var colorList=["#CCFFCC","#CC33CC","#FFCC00","#33FFCC","#00FF00","#FFFF99","#0099CC","#6666CC","#999966","#333366","#FFCCCC","#006633"];	
							return colorList[params.dataIndex];
						}
					}
				},
				label:{normal:{show:true,position:"top"}},
				data:vals
			 }]
		  };
		  let pchart6=echarts.init(document.getElementById("pchart6"),"shine");
		  pchart6.setOption(option);
	}
};