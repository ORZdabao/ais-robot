var page5={
	init:function(){
		page5.chart1();
		$.get("/selfInspect/autooncoalbelt.do",function(data,status){
			if(data.success){
				index.indexvue.autoOnCoalBelts=data.objs;
			}
		});

		let html="";
		for(let i=0;i<8;i++){
			html+="<tr><td>&nbsp;</td><td></td><td></td></tr>";
		}
		$("#pchart1").append(html);
		page5.chart2();

		html="";
		for(let i=0;i<10;i++){
			html+="<tr><td>&nbsp;</td><td></td><td></td></tr>";
		}
		$("#pchart2").append(html);
		page5.chart3();

		$.get("/imachine/imachineStatus.do",function(data,status){
			if(data=="开启"){
				$("#beginIMachineDiv").css("display","none");
				$("#endIMachineDiv").css("display","");

			}else if(data=="关闭"){
				$("#beginIMachineDiv").css("display","");
				$("#endIMachineDiv").css("display","none");
			}
		});
	},
	chart1:function(){
		$.get("/common/find.do?tbl=autooncoal",function(data,status){
			if(data.success){
				let obj=data.objs[0];
				if(obj.flag==0){
					//不启用
					$("#divBtn4State").text("(未启用)");
					$("#divBtn4").css("background-color","#FF0000");
				}else{
					//启用
					$("#divBtn4State").text("(已启用)");
					$("#divBtn4").css("background-color","#00FF00");
				}
				index.indexvue.autoOnCoalForm.flag.value=obj.flag+"";
				if(obj.beltId){
					index.indexvue.autoOnCoalForm.beltId.value=obj.beltId+"";
				}
				if(obj.stove){
					index.indexvue.autoOnCoalForm.stove.value=obj.stove+"";
				}
			}
		});
	},
	divBtnEXE:function(flag){
		if(flag==5){
			index.indexvue.autoOnCoalDialogVisible=true;
		}else{
			$.get("/selfInspect/divbtn.do?flag="+flag,function(data,status){
				toastr.warning(data.error);
			});
		}
	},
	beginIMachine:function(){
		$.get("/imachine/beginIMachine.do",function(data,status){
			toastr.warning(data);
			if(data=="开始清理检修"){
				$("#beginIMachineDiv").css("display","none");
				$("#endIMachineDiv").css("display","");
			}

		});
	},
	endIMachine:function(){
		$.get("/imachine/endIMachine.do",function(data,status){
			toastr.warning(data);
			if(data=="结束清理检修"){
				$("#beginIMachineDiv").css("display","");
				$("#endIMachineDiv").css("display","none");
			}
		});
	},
	chart2:function(){
		$.get("/page4/chart3.do",function(data,status){
			if(data.success){
				let html="<tr><td align='center' width=33%><b>皮带编号</b></td><td align='center' width=33%><b>状态</b></td><td align='center' width=33%><b>时间</b></td></tr>";
				for(let i=0;i<data.objs.length;i++){
					let obj=data.objs[i];
					html+="<tr><td align=center>"+obj.devName+"</td><td align=center>"+obj.note+"</td>" +
						"<td align=center>"+obj.time+"</td></tr>";
				}
				$("#pchart1").html(html);
			}
		});
	},
	addBunkerML:function(bunkerMLs,time){
		let html="<tr><td align=center width=33%><b>料位名称</b></td><td align=center width=33%><b>料位</b></td><td align=center width=33%><b>时间</b></td></tr>";
		for(let i=0;i<bunkerMLs.length;i++){
			let obj=bunkerMLs[i];
			html+="<tr><td align=center>"+obj.name+"</td>" +
				"<td align=center>"+obj.value+"</td><td align=center>"+time+"</td></tr>";
		}
		$("#pchart2").html(html);
	},
	chart3:function(){
		$.get("/page5/chart2.do",function(data,status){
			if(data.success){
				let html="<tr><td align=center width=33%><b>料位名称</b></td><td align=center width=33%><b>料位</b></td><td align=center width=33%><b>时间</b></td></tr>";
				for(let i=0;i<data.objs.length;i++){
					let obj=data.objs[i];
					html+="<tr><td align=center>"+obj.name+"</td>" +
						"<td align=center>-</td><td align=center>-</td></tr>";
				}
				$("#pchart2").html(html);
			}
		});
	}
};