var page7={
	intervalId:"",
	init:function(){
		$.get("/aircannon/getAirCannons.do",function(data){
			var myChartArr = [];
			var optionArr = [];
			if(data.success){
				var storages=data.objs;
				var i=0;
				for(var storage in storages){
					i++;
					var j=0;
					var storageTitle="";
					if(storages[storage][0].auto==1){
						storageTitle = storage+"(<font size='4' color='yellow' >自动模式</font>)";
					}else{
						storageTitle = storage+"(<font size='4' color='yellow'>手动模式</font>)";
					}

					$("#storage"+i+"_title").html(storageTitle);
					for(var aircannon in storages[storage]){
						var btn = $("#storage"+i).children("button")[j];
						$(btn).attr("aircannonid",storages[storage][aircannon].id);
						$(btn).attr("devip",storages[storage][aircannon].devip);
						$(btn).attr("devport",storages[storage][aircannon].devport);
						$(btn).attr("mx",storages[storage][aircannon].mx);
						var divBtn = $("#storage"+i+"_control").children("div")[j];
						$(divBtn).attr("devlocation",storages[storage][aircannon].devlocation);
						if(divBtn!=undefined){
							if(storages[storage][aircannon].hasauto!=1){
								if($(divBtn).html().indexOf("自动模式")!=-1){
									$(divBtn).removeAttr("onclick");
									$(divBtn).css("color","grey");
									// $(divBtn).remove();
									// $("#storage"+i+"_control").children("div")[1].remve();
								}
							}else{

								$("#storage"+i).css("background-color","#9794a5")
								$("#storage"+i+"_control").css("background-color","#9794a5")

							}
						}
						if(storages[storage][aircannon].auto==1){
							$(btn).css("background-color","red");
						}
						j++;
					}
				}
			}
		});

	},
	onfire:function(btn){
		var param = "devip="+btn.getAttribute("devip")+"&devport="+btn.getAttribute("devport")+"&mx="+btn.getAttribute("mx");
		$.get("/aircannon/onFire.do?"+param,function(data){
			if(data.success){

				toastr.success(data.message);
			}else{
				toastr.warning(data.message);
			}

		});
	},
	divBtnEXE:function(btn,type){

		if(type=="2"){
			var url = "/aircannon/manualAdjustAirCannon.do?devlocation="+encodeURIComponent($(btn).attr("devlocation"));
			$.get(url,function(data,status){
				debugger;
				if(data=="手动模式设置成功"){
					var title = $(btn).parent("div").siblings()[0];
					$(title).html($(title).html().replace("自动","手动"));
					var btnDiv = $(btn).parent("div").siblings()[1];
					$(btnDiv).children("button").css("background-color","#00a7d0");
					toastr.success(data);
				}else{
					toastr.warning(data);
				}
			});
			debugger;
		}else{

			var url = "/aircannon/autoAdjustAirCannon.do?devlocation="+encodeURIComponent($(btn).attr("devlocation"));
			$.get(url,function(data,status){
				if(data=="自动模式设置成功"){
					var title = $(btn).parent("div").siblings()[0];
					$(title).html($(title).html().replace("手动","自动"));
					var btnDiv = $(btn).parent("div").siblings()[1];
					$(btnDiv).children("button").css("background-color","red");
					toastr.success(data);
				}else{
					toastr.warning(data);
				}
			});
		}
	}
};