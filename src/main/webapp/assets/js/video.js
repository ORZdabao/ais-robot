//视频播放
var video={
	init:0,
	play:function(nvr){
		//初始化视频预览组件
		// 检查插件是否已经安装过
		var iRet = WebVideoCtrl.I_CheckPluginInstall();
		if (-2 == iRet) {
			alert("您的Chrome浏览器版本过高，不支持NPAPI插件！");
			return false;
		} else if (-1 == iRet) {
			var title = "下载提示";
			var msg = "您还未安装过插件，请先下载。\r\n安装前请先关闭浏览器!";
			alert(msg);
			return false;
		}
		if(video.init==0){
			setTimeout(function () {
				// 初始化插件参数及插入插件
				WebVideoCtrl.I_InitPlugin("94%", "94%", {
					bWndFull: true,//是否支持单窗口双击全屏,默认支持 true:支持 false:不支持
					iWndowType:1,
					cbSelWnd:function(xmlDoc){
						if(video.init==0){
							video.init=1;
							if(nvr.channel>0){
								video.login(nvr);
							}
						}
					}
				});
				WebVideoCtrl.I_InsertOBJECTPlugin("realPlayDiv");
			}, 100);
		}else{
			if(nvr.channel>0){
				video.login(nvr);
			}
		}
	},
	login:function(nvr){
		var iRet = WebVideoCtrl.I_Login(nvr.ip, 1, nvr.port,nvr.userName, nvr.password, {
			success: function (xmlDoc) {
				setTimeout(function () {
					video.startRealPlay(nvr);
				}, 500);
			},
			error: function () {
				parent.toastr.warning(nvr.ip + " NVR登录失败！", "视频");
			}
		});
		if (-1==iRet) {
			setTimeout(function () {
				video.startRealPlay(nvr);
			}, 500);
		}
		return iRet;
	},
	// 开始预览
	startRealPlay:function(nvr) {
		if(video.init==1){
			var oWndInfo = WebVideoCtrl.I_GetWindowStatus(nvr.wndIndex);
			if (oWndInfo != null) {// 已经在播放了，先停止
				WebVideoCtrl.I_Stop();
			}
		}
		var iRet = WebVideoCtrl.I_StartRealPlay(nvr.ip, {
			iWndIndex:nvr.wndIndex,
			iStreamType: 2,
			iChannelID: nvr.channel,
			bZeroChannel: false
		});
		if (0!=iRet) {
			parent.toastr.warning(nvr.name+" NVR:"+nvr.ip+"开始预览失败", "视频");
		}
	},
	// 停止预览
	stopSingleRealPlay:function() {
		if(video.init==1){
			var oWndInfo = WebVideoCtrl.I_GetWindowStatus(0);
			if (oWndInfo != null) {
				var iRet = WebVideoCtrl.I_Stop(0);
				if (iRet!=0) {
					parent.toastr.warning(oWndInfo.szIP+" 窗口:"+i+"停止预览失败");
				}
			}
		}
	}
}