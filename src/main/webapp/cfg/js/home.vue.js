Vue.prototype.$request = service;
var childVue=null;
new Vue({
	el: '#app',
	name:"Home",
	data:function(){
		return {
			userName: sessionStorage.getItem("userName"),
	        activeName:"first",
	        label:"",
	    }
	},
	mounted: function(){		
		if(sessionStorage.getItem("role")==null){
			//返回
			window.location.href="../index.html";
		}else{
			this.toPath("user","用户");
		}
		this.$nextTick( function() {
			
		});
	},
    methods:{
    	toPath:function(v,label){
    		if(v=="index"){
    			//返回
    			sessionStorage.setItem("back",1);
    			window.location.href="../index.html";
    		}else{
    			this.label=label;
    			$("#mainDiv").load(v+".html", function(responseTxt, statusTxt, xhr) {});
    		}
    	},
    	handleOpen(key, keyPath) {
            console.log(key, keyPath);
          },
          handleClose(key, keyPath) {
            console.log(key, keyPath);
          }
    }
  });