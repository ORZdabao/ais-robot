﻿
drop database if exists `ais`;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `ais`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `ais`;

#
# Structure for the `tbl_activealarm` table : 活动告警
#

CREATE TABLE `tbl_activealarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `devType` int(11) NOT NULL COMMENT '设备类型 0:皮带 1:其他设备',
  `alarmType` int(1) NOT NULL COMMENT '告警类型 1:电度 2:电流 3:甲乙粗碎温度1 4:瞬时电流 5:撕裂左 6:撕裂右 7:堵煤 8:异物 9:跑偏 11:电机非驱动端温度 12:电机驱动端温度 13:甲乙粗碎温度2 14:非驱动端温度 15:驱动端温度',
  `severity` int(1) NOT NULL COMMENT '告警级别',
  `img` varchar(100) DEFAULT NULL COMMENT '告警图片',
  `alarmTime` datetime NOT NULL COMMENT '发生时间',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_alarmaudio` table : 告警音
#

CREATE TABLE `tbl_alarmaudio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devType` int(11) NOT NULL COMMENT '设备类型 0:皮带 1:其他设备',
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `alarmType` int(1) NOT NULL COMMENT '告警类型 1:电度 2:电流 3:甲乙粗碎温度1 4:瞬时电流 5:撕裂左 6:撕裂右 7:堵煤 8:异物 9:跑偏 11:电机非驱动端温度 12:电机驱动端温度 13:甲乙粗碎温度2 14:非驱动端温度 15:驱动端温度',
  `fileName` varchar(50) NOT NULL COMMENT '告警音',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devType` (`devType`,`devId`,`alarmType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_autoOnCoal` table : 自动上煤
#

CREATE TABLE `tbl_autooncoal` (
  `id` bigint(20) NOT NULL,
  `flag` int(1) NOT NULL DEFAULT '0' COMMENT '是否自动上煤 0:不启用 1:启用',
  `beltId` bigint(20) DEFAULT NULL COMMENT '皮带id',
  `stove` int(1) NOT NULL DEFAULT '0' COMMENT '自动上煤煤炉 0:无 1:一号炉 2:二号炉 3:一号二号炉',
  `beltploughId` bigint(20) DEFAULT NULL COMMENT '抬犁的犁煤器id',
  PRIMARY KEY (`id`),
  KEY `beltId` (`beltId`),
  CONSTRAINT `tbl_autooncoal_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_belt` table : 皮带
#

CREATE TABLE `tbl_belt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devName` varchar(20) NOT NULL COMMENT '皮带名称',
  `alarmdir` varchar(100) DEFAULT NULL COMMENT '告警图片目录路径',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  `no` int(11) DEFAULT NULL COMMENT '分组',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devName` (`devName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_otherdev` table : 其他设备
#

CREATE TABLE `tbl_otherdev` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devName` varchar(20) NOT NULL COMMENT '设备名称',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devName` (`devName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_historyalarm` table : 历史告警
#

CREATE TABLE `tbl_historyalarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `devType` int(11) NOT NULL COMMENT '设备类型 0:皮带 1:其他设备',
  `devName` varchar(20) NOT NULL COMMENT '皮带名称',
  `alarmType` int(1) NOT NULL COMMENT '告警类型 1:电度 2:电流 3:甲乙粗碎温度1 4:瞬时电流 5:撕裂左 6:撕裂右 7:堵煤 8:异物 9:跑偏 11:电机非驱动端温度 12:电机驱动端温度 13:甲乙粗碎温度2 14:非驱动端温度 15:驱动端温度',
  `severity` int(1) NOT NULL COMMENT '告警级别',
  `img` varchar(100) DEFAULT NULL COMMENT '告警图片',
  `alarmTime` datetime NOT NULL COMMENT '发生时间',
  `restoreType` int(1) NOT NULL COMMENT '恢复类型 1:自动恢复 2:手动恢复',
  `restoreUserName` varchar(20) DEFAULT NULL COMMENT '恢复人',
  `restoreTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '恢复时间',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_nvr` table : 硬盘录像机
#

CREATE TABLE `tbl_nvr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devName` varchar(20) NOT NULL COMMENT '设备名称',
  `devIp` char(15) NOT NULL COMMENT '通信IP',
  `devPort` int(5) NOT NULL COMMENT '通信端口',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devName` (`devName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_camera` table : 摄像头
#

CREATE TABLE `tbl_camera` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '名称',
  `devIp` varchar(15) NOT NULL COMMENT '摄像头IP',
  `nvrId` bigint(20) NOT NULL COMMENT 'NVR编号',
  `channel` int(11) NOT NULL COMMENT '视频通道',
  `showIndex` int(1) NOT NULL DEFAULT '0' COMMENT '大屏显示的位置 0不显示',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nvrId` (`nvrId`,`channel`),
  CONSTRAINT `tbl_camera_ibfk_1` FOREIGN KEY (`nvrId`) REFERENCES `tbl_nvr` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltcamera` table : 皮带摄像头
#

CREATE TABLE `tbl_beltcamera` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltId` bigint(20) NOT NULL COMMENT '皮带id',
  `cameraId` bigint(20) NOT NULL COMMENT '摄像头id',
  `type` int(1) NOT NULL COMMENT '摄像头类型 5:撕裂左 6:撕裂右 7:堵煤 8:异物 9:跑偏 20:头部 21:尾部',
  PRIMARY KEY (`id`),
  KEY `beltId` (`beltId`),
  KEY `cameraId` (`cameraId`),
  CONSTRAINT `tbl_beltcamera_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltcamera_ibfk_2` FOREIGN KEY (`cameraId`) REFERENCES `tbl_camera` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_plc` table : PLC
#

CREATE TABLE `tbl_plc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devIp` char(15) NOT NULL COMMENT '通信IP',
  `devPort` int(5) NOT NULL COMMENT '通信端口',
  `type` int(1) NOT NULL COMMENT '1:开关量 2:预置位',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devIp` (`devIp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltpi` table : 皮带告警输出
#

CREATE TABLE `tbl_beltpi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltId` bigint(20) NOT NULL COMMENT '皮带id',
  `alarmType` int(1) NOT NULL COMMENT '告警类型 5:撕裂左 6:撕裂右 7:堵煤 8:异物 9:跑偏',
  `dir` varchar(200) DEFAULT NULL COMMENT '告警文件夹',
  `plcId` bigint(20) DEFAULT NULL COMMENT 'PLC id',
  `pi` int(11) DEFAULT NULL COMMENT '对应输出干节点',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`pi`),
  KEY `beltId` (`beltId`),
  CONSTRAINT `tbl_beltpi_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltpi_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltplough` table : 皮带犁煤器
#

CREATE TABLE `tbl_beltplough` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '犁煤器名称',
  `beltId` bigint(20) NOT NULL COMMENT '皮带id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `beltId` (`beltId`),
  CONSTRAINT `tbl_beltplough_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltploughdown` table : 皮带犁煤器落犁
#

CREATE TABLE `tbl_beltploughdown` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltploughId` bigint(20) NOT NULL COMMENT '皮带犁煤器id',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `pi` int(11) NOT NULL COMMENT '对应输出干节点',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`pi`),
  KEY `beltploughId` (`beltploughId`),
  CONSTRAINT `tbl_beltploughdown_ibfk_1` FOREIGN KEY (`beltploughId`) REFERENCES `tbl_beltplough` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltploughdown_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltploughm` table : 皮带犁煤器上位检修
#

CREATE TABLE `tbl_beltploughm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltploughId` bigint(20) NOT NULL COMMENT '皮带犁煤器id',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `mx` int(11) NOT NULL COMMENT '对应读Modbus地址',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`mx`),
  KEY `beltploughId` (`beltploughId`),
  CONSTRAINT `tbl_beltploughm_ibfk_1` FOREIGN KEY (`beltploughId`) REFERENCES `tbl_beltplough` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltploughm_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltploughup` table : 皮带犁煤器抬犁
#

CREATE TABLE `tbl_beltploughup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltploughId` bigint(20) NOT NULL COMMENT '皮带犁煤器id',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `pi` int(11) NOT NULL COMMENT '对应输出干节点',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`pi`),
  KEY `beltploughId` (`beltploughId`),
  CONSTRAINT `tbl_beltploughup_ibfk_1` FOREIGN KEY (`beltploughId`) REFERENCES `tbl_beltplough` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltploughup_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltstart` table : 皮带启动
#

CREATE TABLE `tbl_beltstart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltId` bigint(20) NOT NULL COMMENT '皮带id',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '启动类型 0:普通 1:取料 2:堆料',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `pi` int(11) NOT NULL COMMENT '对应输出干节点',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`pi`),
  KEY `beltId` (`beltId`),
  CONSTRAINT `tbl_beltstart_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltstart_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_beltstop` table : 皮带停止
#

CREATE TABLE `tbl_beltstop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beltId` bigint(20) NOT NULL COMMENT '皮带id',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `pi` int(11) NOT NULL COMMENT '对应输出干节点',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plcId` (`plcId`,`pi`),
  KEY `beltId` (`beltId`),
  CONSTRAINT `tbl_beltstop_ibfk_1` FOREIGN KEY (`beltId`) REFERENCES `tbl_belt` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_beltstop_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_bunker` table : 煤仓
#

CREATE TABLE `tbl_bunker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '煤仓名称',
  `stove` int(1) NOT NULL DEFAULT '0' COMMENT '所属煤炉 0:无 1:一号炉 2:二号炉',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_bunkerml` table : 煤仓料位
#

CREATE TABLE `tbl_bunkerml` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '名称',
  `bunkerId` bigint(20) NOT NULL COMMENT '煤仓id',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `mx` int(11) NOT NULL COMMENT '对应读Modbus地址',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `plcId` (`plcId`,`mx`),
  KEY `bunkerId` (`bunkerId`),
  CONSTRAINT `tbl_bunkerml_ibfk_1` FOREIGN KEY (`bunkerId`) REFERENCES `tbl_bunker` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_bunkerml_ibfk_2` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_bunkermlbeltplough` table : 煤仓料位与犁煤器对应关系
#

CREATE TABLE `tbl_bunkermlbeltplough` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bunkermlId` bigint(20) NOT NULL COMMENT '煤仓料位id',
  `beltploughId` bigint(20) NOT NULL COMMENT '犁煤器id',
  PRIMARY KEY (`id`),
  KEY `bunkermlId` (`bunkermlId`),
  KEY `beltploughId` (`beltploughId`),
  CONSTRAINT `tbl_bunkermlbeltplough_ibfk_1` FOREIGN KEY (`bunkermlId`) REFERENCES `tbl_bunkerml` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tbl_bunkermlbeltplough_ibfk_2` FOREIGN KEY (`beltploughId`) REFERENCES `tbl_beltplough` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_readparam` table : 读取参数
#

CREATE TABLE `tbl_readparam` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devType` int(11) NOT NULL COMMENT '设备类型 0:皮带 1:其他设备',
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `type` int(1) NOT NULL COMMENT '数据类型 0:电流 1:取料电流 2:堆料电流 3:电度 4:温度1 5:温度2 6:秤瞬时流量 7:电机非驱动端温度 8:电机驱动端温度 9:非驱动端温度 10:驱动端温度',
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `mx` int(11) NOT NULL COMMENT '对应读Modbus地址',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devType` (`devType`,`devId`,`type`),
  UNIQUE KEY `plcId` (`plcId`,`mx`),
  CONSTRAINT `tbl_readparam_ibfk_1` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_robot` table : 机器人
#

CREATE TABLE `tbl_robot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devName` varchar(20) NOT NULL COMMENT '机器人名称',
  `devIp` varchar(15) NOT NULL COMMENT '机器人IP',
  `devPort` int(5) NOT NULL COMMENT '通信端口',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devIp` (`devIp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_devline` table : 摄像头和机器人在线
#

CREATE TABLE `tbl_devline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `devType` int(11) NOT NULL COMMENT '设备类型  0:机器人 1:摄像头',
  `onlineSec` int(11) NOT NULL DEFAULT '0' COMMENT '在线(秒)',
  `offlineSec` int(11) NOT NULL DEFAULT '0' COMMENT '离线(秒)',
  `onlineNum` int(11) NOT NULL DEFAULT '0' COMMENT '在线次数',
  `offlineNum` int(11) NOT NULL DEFAULT '0' COMMENT '离线次数',
  `month` varchar(10) NOT NULL COMMENT '所在月',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_robotpreset` table : 机器人预置位
#

CREATE TABLE `tbl_robotpreset` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plcId` bigint(20) NOT NULL COMMENT 'PLC id',
  `mx` int(11) NOT NULL COMMENT '对应读Modbus地址',
  `robotId` bigint(20) NOT NULL COMMENT '机器人id',
  `preset` int(11) NOT NULL COMMENT '机器人预置位',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0:普通 1:复位 2:巡检',
  `site` varchar(200) NOT NULL COMMENT '犁煤器位置',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `preset` (`preset`,`robotId`),
  KEY `plcId` (`plcId`),
  CONSTRAINT `tbl_robotpreset_ibfk_1` FOREIGN KEY (`plcId`) REFERENCES `tbl_plc` (`id`) ON DELETE CASCADE,
  KEY `robotId` (`robotId`),
  CONSTRAINT `tbl_robotpreset_ibfk_2` FOREIGN KEY (`robotId`) REFERENCES `tbl_robot` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_threshold` table : 阈值
#

CREATE TABLE `tbl_threshold` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `devType` int(11) NOT NULL COMMENT '设备类型 0:皮带 1:其他设备',
  `devId` bigint(20) NOT NULL COMMENT '设备id',
  `type` int(1) NOT NULL COMMENT '阈值类型 0:电流 1:电度 2:温度 3:瞬时电流 4:非驱动端温度 5:驱动端温度',
  `value` int(11) NOT NULL COMMENT '阈值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `devType` (`devType`,`devId`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_user` table : 用户
#

CREATE TABLE `tbl_user` (
  `id` bigint(20) NOT NULL auto_increment,
  `company` varchar(64) DEFAULT NULL COMMENT '所属单位',
  `department` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `password` varchar(50) NOT NULL DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT '密码',
  `phoneNo` varchar(11) NOT NULL COMMENT '联系方式',
  `role` int(1) NOT NULL DEFAULT '2' COMMENT '角色 1:管理员 2:普通用户',
  `trueName` varchar(20) NOT NULL COMMENT '用户真实姓名',
  `userName` varchar(20) NOT NULL COMMENT '用户名',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for the `tbl_autoOnCoal` table  (LIMIT 0,500)
#

INSERT INTO `tbl_autoOnCoal` (`id`) VALUES 
  (1);
COMMIT;

#
# Data for the `tbl_user` table  (LIMIT 0,500)
#

INSERT INTO `tbl_user` (`id`, `company`, `department`, `password`, `phoneNo`, `role`, `trueName`, `userName`) VALUES 
  (1,'内蒙古京能盛乐热电有限公司','研发部','e10adc3949ba59abbe56e057f20f883e','15050566789',1,'admin','admin');
COMMIT;