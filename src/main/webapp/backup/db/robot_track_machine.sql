create table robot_track_machine(
id varchar(20) not null primary key  ,
name varchar(40) not null comment '轨道机名称' ,
robot_model varchar(10) not null comment '轨道机型号' ,
ip  varchar(40)   comment '轨道机ip',
port varchar(5) comment '轨道机端口',
work_model varchar(10) comment '轨道机工作模式，1表示自动0表示手动',
position varchar(5) comment '当前位置',
comment varchar(100) comment '备注'
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into robot_track_machine(id,name,robot_model,ip,port,work_model,position)
values('1','综合动力站室','K02','192.168.166.21','4196','0','1');


insert into robot_track_machine(id,name,robot_model,ip,port,work_model,position)
values('2','补偿电容室','K02','192.168.166.22','4196','0','1');
