/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : localhost:3306
 Source Schema         : ais

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 24/11/2023 21:46:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for robot_track_instruct
-- ----------------------------
DROP TABLE IF EXISTS `robot_track_instruct`;
CREATE TABLE `robot_track_instruct`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域id',
  `track_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '轨道机id',
  `text_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文字编号',
  `number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数字编号',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否使用',
  `comment` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `start_instruct` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始指令',
  `horizontal_instruct` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束指令',
  `vertical_instruct` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行动指令',
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预置位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of robot_track_instruct
-- ----------------------------
INSERT INTO `robot_track_instruct` VALUES (1, '1', '1', '车身T4，总装T1（上）', '914，,915（上）', '1', NULL, 'FF 01 00 07 00 01 09', 'FF 1A 00 07 00 01 09', 'FF 1B 00 07 00 01 09', '1');
INSERT INTO `robot_track_instruct` VALUES (2, '1', '2', '车身T4，总装T1（下）', '914，,915（下）', '1', NULL, 'FF 01 00 07 00 02 0A', 'FF 1A 00 07 00 02 0A', 'FF 1B 00 07 00 02 0A', '2');
INSERT INTO `robot_track_instruct` VALUES (3, '1', '3', '车桥T1，车身T2（下）', '916,917（下）', '1', NULL, 'FF 01 00 07 00 03 0B', 'FF 1A 00 07 00 03 0B', 'FF 1B 00 07 00 03 0B', '3');
INSERT INTO `robot_track_instruct` VALUES (4, '1', '4', '车桥T1，车身T2（上）', '916,917（上）', '1', NULL, 'FF 01 00 07 00 04 0C', 'FF 1A 00 07 00 04 0C', 'FF 1B 00 07 00 04 0C', '4');
INSERT INTO `robot_track_instruct` VALUES (5, '1', '5', '所用变，备用（上）', '961,918（上）', '1', NULL, 'FF 01 00 07 00 05 0D', 'FF 1A 00 07 00 05 0D', 'FF 1B 00 07 00 05 0D', '5');
INSERT INTO `robot_track_instruct` VALUES (6, '1', '6', '所用变，备用（下）', '961,918（下）', '1', NULL, 'FF 01 00 07 00 06 0E', 'FF 1A 00 07 00 06 0E', 'FF 1B 00 07 00 06 0E', '6');
INSERT INTO `robot_track_instruct` VALUES (7, '1', '7', '1段电容补偿，2段电容补偿（下）', '919,929（下）', '1', NULL, 'FF 01 00 07 00 07 0F', 'FF 1A 00 07 00 07 0F', 'FF 1B 00 07 00 07 0F', '7');
INSERT INTO `robot_track_instruct` VALUES (8, '1', '8', '1段电容补偿，2段电容补偿（上）', '919,929（上）', '1', NULL, 'FF 01 00 07 00 08 10', 'FF 1A 00 07 00 08 10', 'FF 1B 00 07 00 08 10', '8');
INSERT INTO `robot_track_instruct` VALUES (9, '1', '9', '综合T9，研发T2（上）', '928,927（上）', '1', NULL, 'FF 01 00 07 00 09 11', 'FF 1A 00 07 00 09 11', 'FF 1B 00 07 00 09 11', '9');
INSERT INTO `robot_track_instruct` VALUES (10, '1', '10', '综合T9，研发T2（下）', '928,927（下）', '1', NULL, 'FF 01 00 07 00 0A 12', 'FF 1A 00 07 00 0A 12', 'FF 1B 00 07 00 0A 12', '10');
INSERT INTO `robot_track_instruct` VALUES (11, '1', '11', '所用变，车桥T3（下）', '962,926（下）', '1', NULL, 'FF 01 00 07 00 0B 13', 'FF 1A 00 07 00 0B 13', 'FF 1B 00 07 00 0B 13', '11');
INSERT INTO `robot_track_instruct` VALUES (12, '1', '12', '所用变，车桥T3（上）', '962,926（上）', '1', NULL, 'FF 01 00 07 00 0C 14', 'FF 1A 00 07 00 0C 14', 'FF 1B 00 07 00 0C 14', '12');
INSERT INTO `robot_track_instruct` VALUES (13, '1', '13', '车桥T4，备用（上）', '925,924（上）', '1', NULL, 'FF 01 00 07 00 0D 15', 'FF 1A 00 07 00 0D 15', 'FF 1B 00 07 00 0D 15', '13');
INSERT INTO `robot_track_instruct` VALUES (14, '1', '14', '车桥T4，备用（下）', '925,924（下）', '1', NULL, 'FF 01 00 07 00 0E 16', 'FF 1A 00 07 00 0E 16', 'FF 1B 00 07 00 0E 16', '14');
INSERT INTO `robot_track_instruct` VALUES (15, '1', '15', '2段电源进线，2#计量（下）', '110站-911（下）', '1', NULL, 'FF 01 00 07 00 0F 17', 'FF 1A 00 07 00 0F 17', 'FF 1B 0 007 00 0F 17', '15');
INSERT INTO `robot_track_instruct` VALUES (16, '1', '16', '2段电源进线，2#计量（上）', '110站-911（上）', '1', NULL, 'FF 01 00 07 00 10 18', 'FF 1A 00 07 00 10 18', 'FF 1B 00 07 00 10 18', '16');
INSERT INTO `robot_track_instruct` VALUES (17, '1', '17', '电压互感器，车身T5（上）', '982，921（上）', '1', NULL, 'FF 01 00 07 00 11 19', 'FF 1A 00 07 00 11 19', 'FF 1B 00 07 00 11 19', '17');
INSERT INTO `robot_track_instruct` VALUES (18, '1', '18', '电压互感器，车身T5（下）', '982，921（下）', '1', NULL, 'FF 01 00 07 00 12 1A', 'FF 1A 00 07 00 12 1A', 'FF 1B 00 07 00 12 1A', '18');
INSERT INTO `robot_track_instruct` VALUES (19, '1', '19', '车身T6，车身T8（下）', '922，923（下）', '1', NULL, 'FF 01 00 07 00 13 1B', 'FF 1A 00 07 00 13 1B', 'FF 1B 00 07 00 13 1B', '19');
INSERT INTO `robot_track_instruct` VALUES (20, '1', '20', '车身T6，车身T8（上）', '922，923（上）', '1', NULL, 'FF 01 00 07 00 14 1C', 'FF 1A 00 07 00 14 1C', 'FF 1B 00 07 00 14 1C', '20');
INSERT INTO `robot_track_instruct` VALUES (21, '1', '21', '隔离柜，联络柜（上）', '910（上）', '1', NULL, 'FF 01 00 07 00 15 1D', 'FF 1A 00 07 00 15 1D', 'FF 1B 00 07 00 15 1D', '21');
INSERT INTO `robot_track_instruct` VALUES (22, '1', '22', '隔离柜，联络柜（下）', '910（下）', '1', NULL, 'FF 01 00 07 00 16 1E', 'FF 1A 00 07 00 16 1E', 'FF 1B 00 07 00 16 1E', '22');
INSERT INTO `robot_track_instruct` VALUES (23, '1', '23', '车身T7，车身T3（下）', '913，909（下）', '1', NULL, 'FF 01 00 07 00 17 1F', 'FF 1A 00 07 00 17 1F', 'FF 1B 00 07 00 17 1F', '23');
INSERT INTO `robot_track_instruct` VALUES (24, '1', '24', '车身T7，车身T3（上）', '913，909（上）', '1', NULL, 'FF 01 00 07 00 18 20', 'FF 1A 00 07 00 18 20', 'FF 1B 00 07 00 18 20', '24');
INSERT INTO `robot_track_instruct` VALUES (25, '1', '25', '车身T1，电压互感器（上）', '908，981（上）', '1', NULL, 'FF 01 00 07 00 19 21', 'FF 1A 00 07 00 19 21', 'FF 1B 00 07 00 19 21', '25');
INSERT INTO `robot_track_instruct` VALUES (26, '1', '26', '车身T1，电压互感器（下）', '908，981（下）', '1', NULL, 'FF 01 00 07 00 1A 22', 'FF 1A 00 07 00 1A 22', 'FF 1B 00 07 00 1A 22', '26');
INSERT INTO `robot_track_instruct` VALUES (27, '1', '27', '1#计量，Ⅰ段电源进线（下）', '110站-912（下）', '1', NULL, 'FF 01 00 07 00 1B 23', 'FF 1A 00 07 00 1B 23', 'FF 1B 00 07 00 1B 23', '27');
INSERT INTO `robot_track_instruct` VALUES (28, '1', '28', '1#计量，Ⅰ段电源进线（上）', '110站-912（上）', '1', NULL, 'FF 01 00 07 00 1C 24', 'FF 1A 00 07 00 1C 24', 'FF 1B 00 07 00 1C 24', '28');
INSERT INTO `robot_track_instruct` VALUES (29, '1', '29', '直流电源柜', NULL, '1', NULL, 'FF 01 00 07 00 1D 25', 'FF 1A 00 07 00 1D 25', 'FF 1B 00 07 00 1D 25', '29');
INSERT INTO `robot_track_instruct` VALUES (30, '1', '30', '双电源，直流电源柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 1E 26', 'FF 1A 00 07 00 1E 26', 'FF 1B 00 07 00 1E 26', '30');
INSERT INTO `robot_track_instruct` VALUES (31, '1', '31', '双电源，直流电源柜（下）', NULL, '1', NULL, 'FF 01 00 07 00 1F 27', 'FF 1A 00 07 00 1F 27', 'FF 1B 00 07 00 1F 27', '31');
INSERT INTO `robot_track_instruct` VALUES (32, '2', '1', 'Ⅰ段-TSC1阀组柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 01 09', 'FF 1A 00 07 00 01 09', 'FF 1B 00 07 00 01 09', '1');
INSERT INTO `robot_track_instruct` VALUES (33, '2', '2', 'Ⅰ段-TSC1电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 04 0C', 'FF 1A 00 07 00 04 0C', 'FF 1B 00 07 00 04 0C', '1');
INSERT INTO `robot_track_instruct` VALUES (34, '2', '3', 'Ⅰ段-HVC2电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 05 0D', 'FF 1A 00 07 00 05 0D', 'FF 1B 00 07 00 05 0D', '1');
INSERT INTO `robot_track_instruct` VALUES (35, '2', '4', 'Ⅰ段-TSC3电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 08 10', 'FF 1A 00 07 00 08 10', 'FF 1B 00 07 00 08 10', '1');
INSERT INTO `robot_track_instruct` VALUES (36, '2', '5', 'Ⅰ段-TSC3阀组柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 09 11', 'FF 1A 00 07 00 09 11', 'FF 1B 00 07 00 09 11', '1');
INSERT INTO `robot_track_instruct` VALUES (37, '2', '6', 'Ⅱ段-HVC3电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 0C 14', 'FF 1A 00 07 00 0C 14', 'FF 1B 00 07 00 0C 14', '12');
INSERT INTO `robot_track_instruct` VALUES (38, '2', '7', 'Ⅱ段-HVC2电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 0D 15', 'FF 1A 00 07 00 0D 15', 'FF 1B 00 07 00 0D 15', '12');
INSERT INTO `robot_track_instruct` VALUES (39, '2', '8', 'Ⅱ段-TSC1电容柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 10 18', 'FF 1A 00 07 00 10 18', 'FF 1B 00 07 00 10 18', '12');
INSERT INTO `robot_track_instruct` VALUES (40, '2', '9', 'Ⅱ段-TSC1阀组柜（上）', NULL, '1', NULL, 'FF 01 00 07 00 11 19', 'FF 1A 00 07 00 11 19', 'FF 1B 00 07 00 11 19', '12');

SET FOREIGN_KEY_CHECKS = 1;
