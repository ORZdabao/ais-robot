/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : localhost:3306
 Source Schema         : ais

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 09/11/2023 21:18:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for robot_camera
-- ----------------------------
DROP TABLE IF EXISTS `robot_camera`;
CREATE TABLE `robot_camera`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `devIp` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '摄像头IP',
  `devPort` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '摄像头IP端口',
  `nvrId` bigint(20) NOT NULL COMMENT 'NVR编号',
  `channel` int(11) NOT NULL COMMENT '视频通道',
  `userName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `showIndex` int(1) NOT NULL DEFAULT 0 COMMENT '大屏显示的位置 0不显示',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `nvrId`(`nvrId`, `channel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of robot_camera
-- ----------------------------
INSERT INTO `robot_camera` VALUES (1, '机器人视频', '192.168.166.31', '8000', 1, 1, 'admin', '12345abc', 0);
INSERT INTO `robot_camera` VALUES (2, '机器人视频', '192.168.166.32', '8000', 1, 2, 'admin', '12345abc', 0);

SET FOREIGN_KEY_CHECKS = 1;
