package com.nmgj.ais.vo;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
@Data
//@ApiModel(value = "巡检记录", description = "巡检记录")
@TableName(" robot_inspection_record")
public class InspectionVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private String id;
    private String region;

    @TableField("region_id")
    private String regionId;

    @TableField("region_name")
    private String regionName;

    private Date startTime;

    private Date endTime;

    private Date taskTime;

    private String tem;

    private String hum;

    private String picUrl;


}
