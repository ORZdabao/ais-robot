package com.nmgj.ais.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName AlarmVo
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/22 23:39
 * @return
 **/
@Data
public class AlarmVo {
    private String ID ;

    private String DeviceKey;

    private String DeviceId;

    private String DeviceName;

    private String NodeId;

    private String RecordTime;
    // 温度
    private String Tem;
    //湿度
    private String Hum;

    private String Lng;

    private String Lat;

    private String CoordinateType;

}
