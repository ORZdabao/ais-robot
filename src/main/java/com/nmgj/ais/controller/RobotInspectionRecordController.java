package com.nmgj.ais.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.service.IRobotInspectionRecordService;
import com.nmgj.ais.vo.InspectionVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
@RestController
@RequestMapping("/inspection")
@Slf4j
// @Api(tags = "巡检记录")
public class RobotInspectionRecordController {

    @Resource
    private IRobotInspectionRecordService recordService;

    @Resource
    private RedisTemplate redisTemplate;

 //   @ApiOperation(value = "查询巡检记录",notes="查询巡检记录")
    @PostMapping(value = "/getListInfo/{index}/{page}")
    public Map<String,Object> queryInspection(@RequestBody InspectionVo vo , @PathVariable("index") int index, @PathVariable("page") int page){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            IPage<InspectionVo> result = recordService.queryList(vo,index,page);
            resultMap.put("success", true);
            resultMap.put("data",result.getRecords());
            resultMap.put("total",result.getTotal());
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("error", e.getMessage());
        }
        return resultMap;
    }
 //   @ApiOperation(value = "创建巡检记录",notes="创建巡检记录")
    @PostMapping(value = "/createInspection")
    public Map<String, Object> createInspection(@RequestBody InspectionVo vo){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try {
        //    recordService.createInspection(vo);
            resultMap.put("success", true);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("error", e.getMessage());
        }
        return resultMap;
    }
    @GetMapping("/test")
    public String test(){
        Object object = redisTemplate.opsForValue().get(Constants.Robot.POWER_REDIS_KEY);
        if(ObjectUtil.isNotEmpty(object)){
            log.info(object.toString());
        }
        return "SUCCESS";
    }
}
