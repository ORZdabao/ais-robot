package com.nmgj.ais.controller;

import com.nmgj.ais.service.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {

	/**
	 * 实例化日志类.
	 */
	protected static Logger logger = LoggerFactory.getLogger(BaseController.class);

	/**
	 * 公共业务处理
	 */
	@Autowired
	protected CommonService commonService;

	/**
	 * 保存操作日志到数据库.
	 *
	 * @param type   操作类型
	 * @param obj    操作对象
	 * @param result 操作结果
	 */
	public void addOperateLog(int type, String obj, String result) {
//		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//		HttpServletRequest request = attrs.getRequest();
//		HttpSession session = request.getSession();
//		String userName = (String) session.getAttribute(Constants.SessionKey.USERNAME);
//		addOperateLog(userName, type, obj, result);
	}

	/**
	 * 保存操作日志到数据库.
	 *
	 * @param userName 操作用户
	 * @param type     操作类型
	 * @param obj      操作对象
	 * @param result   操作结果
	 */
	public void addOperateLog(String userName, int type, String obj, String result) {
//		// 请求参数
//		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//		// 请求
//		HttpServletRequest request = attrs.getRequest();
//		// 操作日志
//		Map<String, Object> operateLog = new HashMap<String, Object>();
//		operateLog.put(Constants.SessionKey.USERNAME, userName);
//		operateLog.put("terminal", request.getRemoteAddr());
//		operateLog.put("type", type);
//		operateLog.put("object", obj);
//		operateLog.put("result", result);
//		//commonService.addOne(Table.EN.OPERATELOG, operateLog);
//		logger.info("增加操作日志,{}", operateLog);
	}
}
