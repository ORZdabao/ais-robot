package com.nmgj.ais.controller;

import com.nmgj.ais.service.IRobotCameraService;
import com.nmgj.ais.service.IRobotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class RobotCameraController {

    @Autowired
    private IRobotService robotService;

    @Autowired
    private IRobotCameraService robotCameraService;

    @PostMapping(value = "/camera/getPic")
    public Map<String,Object> getPic(){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            robotCameraService.getPic();
            resultMap.put("msg", "操作成功");
            resultMap.put("success",true);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "操作失败");
        }
        return resultMap;
    }

    @PostMapping(value = "/camera/movePosition")
    public Map<String,Object> movePosition(){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
             robotCameraService.movePosition();
            resultMap.put("success", true);
            resultMap.put("msg", "操作成功");
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "操作失败");
        }
        return resultMap;
    }



}
