package com.nmgj.ais.controller;

import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.constants.ErrorCode;
import com.nmgj.ais.common.constants.GlobalData;
import com.nmgj.ais.common.constants.Table;
import com.nmgj.ais.common.util.MD5;
import com.nmgj.ais.pojo.Condition;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 用户.
 *
 */
@Controller
public class UserController extends BaseController {

	/**
	 * 用户登录.
	 *
	 * @param userName 用户名
	 * @param password 密码
	 * @param license  许可证
	 * @param session  会话
	 * @return 给前端数据
	 */
	@ResponseBody
	@RequestMapping(value = "/user/login.do")
	public Map<String, Object> login(String userName, String password, String license, HttpSession session) {
		// 返回值
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (StringUtils.isBlank(userName) || StringUtils.isBlank(password)) {
				map.put("success", false);
				map.put("error", ErrorCode.Common.PARAMNULL);
				return map;
			}

			// 通过用户名查找用户
			Map<String, Object> user = commonService.findByKeyOne(Table.EN.USER, Constants.SessionKey.USERNAME,
					userName);
			if (user == null) {
				map.put("success", false);
				map.put("error", ErrorCode.User.USERNOTEXISTS);
				logger.error("登录失败:{}:{}", userName, ErrorCode.User.USERNOTEXISTS);
				return map;
			} else {
				if (!MD5.getMD5(password).equals(user.get("password"))) {
					map.put("success", false);
					map.put("error", ErrorCode.User.USERNAMEORPASSWORDERROR);
					logger.error("登录失败:{},{}", userName, ErrorCode.User.USERNAMEORPASSWORDERROR);
					return map;
				}
				// 判断该用户是否已经登录
				if (GlobalData.webSocketSessionMap.get(userName) != null) {
					// 返回错误信息给前台
					map.put("success", false);
					map.put("error", ErrorCode.User.LOGINED);
					logger.error("登录失败:{}:{}", userName, ErrorCode.User.LOGINED);
					return map;
				}
				int role = (int) user.get(Constants.SessionKey.ROLE);
				session.setAttribute(Constants.SessionKey.ROLE, role);
				session.setAttribute(Constants.SessionKey.USERNAME, userName);
				map.put(Constants.SessionKey.ROLE, role);
				map.put("success", true);
				map.put("error", ErrorCode.Common.SUCCESS);
				logger.info("登录成功,{}", userName);
			}
		} catch (Exception e) {
			map.put("success", false);
			map.put("error", e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			addOperateLog(userName, Constants.OperateLogType.LOGIN, "用户登录", map.get("error").toString());
		}
		return map;
	}

	/**
	 * 用户退出.
	 *
	 * @param session 会话
	 * @return 给前端数据
	 */
	@ResponseBody
	@RequestMapping(value = "/user/logout.do")
	public Map<String, Object> logout(HttpSession session) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 登录用户名
			String userName = (String) session.getAttribute(Constants.SessionKey.USERNAME);
			Enumeration<String> attributeNames = session.getAttributeNames();
			while (attributeNames.hasMoreElements()) {
				session.removeAttribute(attributeNames.nextElement());
			}
			map.put("success", true);
			map.put("error", ErrorCode.Common.SUCCESS);
			logger.info("退出成功,{}", userName);
		} catch (Exception e) {
			map.put("success", false);
			map.put("error", e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			addOperateLog(Constants.OperateLogType.LOGOUT, "用户退出", map.get("error").toString());
		}
		return map;
	}

	/**
	 * 修改密码.
	 *
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/user/modifyPass.do")
	public Map<String, Object> modifyPassword(String oldPass, String newPass, HttpSession session) {
		// 返回值
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (StringUtils.isEmpty(oldPass) || StringUtils.isEmpty(newPass)) {
				map.put("success", false);
				map.put("error", ErrorCode.Common.PARAMNULL);
				return map;
			}
			oldPass = MD5.getMD5(oldPass.trim());
			newPass = MD5.getMD5(newPass.trim());
			String userName = (String) session.getAttribute(Constants.SessionKey.USERNAME);
			List<Condition> conditions = new ArrayList<Condition>();
			Condition condition = new Condition();
			condition.setKey(Constants.SessionKey.USERNAME);
			condition.setValue(userName);
			conditions.add(condition);
			condition = new Condition();
			condition.setKey("password");
			condition.setValue(oldPass);
			conditions.add(condition);
			int ret = commonService.updateOneColumnByKeys(Table.EN.USER, conditions, "password", newPass);
			if (ret > 0) {
				map.put("success", true);
				map.put("error", ErrorCode.Common.SUCCESS);
				logger.info("修改密码成功");
			} else {
				map.put("success", false);
				map.put("error", ErrorCode.Common.OLDPASS);
				logger.error("修改密码失败");
			}
		} catch (Exception e) {
			map.put("success", false);
			map.put("error", e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			addOperateLog(Constants.OperateLogType.UPDATE, "修改密码", map.get("error").toString());
		}
		return map;
	}

	/**
	 * 重置密码.
	 *
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/user/resetPass.do")
	public Map<String, Object> resetPassword(String id, String newPass) {
		// 返回值
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (StringUtils.isEmpty(id) || StringUtils.isEmpty(newPass)) {
				map.put("success", false);
				map.put("error", ErrorCode.Common.PARAMNULL);
				return map;
			}
			int ret = commonService.updateOneColumnByPk(Table.EN.USER, Long.valueOf(id), "password",
					MD5.getMD5(newPass));
			if (ret > 0) {
				map.put("success", true);
				map.put("error", ErrorCode.Common.SUCCESS);
				logger.info("重置密码成功,{},{}", id, newPass);
			} else {
				map.put("success", false);
				map.put("error", ErrorCode.Common.UPDATEDB);
				logger.error("重置密码失败,{}", id);
			}
		} catch (Exception e) {
			map.put("success", false);
			map.put("error", e.getMessage());
			logger.error(e.getMessage(), e);
		} finally {
			addOperateLog(Constants.OperateLogType.UPDATE, "重置密码", map.get("error").toString());
		}
		return map;
	}
}
