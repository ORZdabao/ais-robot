package com.nmgj.ais.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nmgj.ais.pojo.RobotCamera;
import com.nmgj.ais.service.IHomePageService;
import com.nmgj.ais.service.IRobotCameraService;
import com.nmgj.ais.vo.AlarmVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName HomePageController
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/24 22:29
 * @return
 **/
@RestController
@Slf4j
public class HomePageController {

    @Resource
    private IHomePageService homePageService;

    @Resource
    private IRobotCameraService cameraService;



    @RequestMapping(value = "/homePage/queryPowerInfo",method = RequestMethod.GET)
    public Map<String,Object> queryPowerInfo(){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            List<AlarmVo> result = homePageService.queryPowerInfo();
            resultMap.put("success", true);
            resultMap.put("msg", "查询成功");
            resultMap.put("data",result);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "查询失败");
        }
        return resultMap;
    }

    @RequestMapping(value = "/homePage/queryCapacityInfo",method = RequestMethod.GET)
    public Map<String,Object> queryCapacityInfo(){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            List<AlarmVo> result = homePageService.queryCapacityInfo();
            resultMap.put("success", true);
            resultMap.put("msg", "查询成功");
            resultMap.put("data",result);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "查询失败");
        }
        return resultMap;
    }

    @RequestMapping(value = "/homePage/queryCameraInfo",method = RequestMethod.GET)
    public Map<String,Object> queryCameraInfo(){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            QueryWrapper queryWrapper = new QueryWrapper();
            List<RobotCamera> result = cameraService.list();
            resultMap.put("success", true);
            resultMap.put("msg", "查询成功");
            resultMap.put("data",result);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "查询失败");
        }
        return resultMap;
    }

    @GetMapping(value = "/homePage/getPicHistory")
    public Map<String,Object> getPicHistory(@RequestParam int type){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            List<String> list = cameraService.getPicHistory(type);
            resultMap.put("msg", "操作成功");
            resultMap.put("success",true);
            resultMap.put("data",list);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "操作失败");
        }
        return resultMap;
    }

}
