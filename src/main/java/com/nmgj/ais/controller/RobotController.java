package com.nmgj.ais.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nmgj.ais.dao.IRobotDao;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotInstruct;
import com.nmgj.ais.service.IRobotInstructService;
import com.nmgj.ais.service.IRobotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.WebAsyncTask;

import javax.annotation.Resource;
import javax.servlet.AsyncContext;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@RestController
@Slf4j
public class RobotController {

    @Resource
    private IRobotService robotService;

    @Autowired
    private IRobotDao robotDao;

    @Autowired
    private IRobotInstructService robotInstructService;

    @Resource
    private DataSourceTransactionManager transactionManager;

    private ExecutorService executorService = new ThreadPoolExecutor(5, 10, 300L,
            TimeUnit.SECONDS, new LinkedBlockingQueue<>(10), new ThreadPoolExecutor.DiscardPolicy());





    @PostMapping(value = "/robot/start")
    public Map<String,Object> startTask( @RequestBody RobotInstruct robotInstruct){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try{
            log.info("id的值是："+robotInstruct.getRegionId());
            executorService.submit(()->{
                try{
                    robotService.startRobot(robotInstruct);
                } catch (Exception e){
                    e.printStackTrace();
                }

            });
            resultMap.put("msg", "操作成功");
            resultMap.put("success",true);
        } catch (Exception e){
            log.error(e.getMessage(),e);
            resultMap.put("success", false);
            resultMap.put("msg", "操作失败");
        }
        return resultMap;
    }


    @GetMapping(value = "/robot/queryStatus")
    public Map<String,Object> queryStatus( @RequestParam int id){

        return  robotService.queryStatus(id);
    }


    @PostMapping(value = "/robot/updateRobotModel")
    public Map<String,Object> updateRobotModel( @RequestBody Robot robot){
        return robotService.updateRobotModel( robot);
    }

    @PostMapping(value = "/robot/autoStart")
    public Map<String,Object> autoStart( @RequestBody Robot robot){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        log.info("开始电容室任务");
        try{
            // 先判断是否开启了自动巡检任务
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("id",robot.getId());
            List<Robot> list = robotDao.selectList(queryWrapper);
            if(CollectionUtil.isNotEmpty(list)){
                Robot resultRobot = list.get(0);
                int model = resultRobot.getWorkModel();
                // 1为自动 0是手动
                if(1 == model){
                    DefaultTransactionDefinition transDefinition = new DefaultTransactionDefinition();
                    transDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW); // 事物隔离级别，开启新事务，这样会比较安全些。
                    TransactionStatus status = transactionManager.getTransaction(transDefinition); // 得到事务状态

                    try {
                        QueryWrapper robotWrapper = new QueryWrapper();
                        robotWrapper.eq("region_id",1);
                        List<RobotInstruct> instructList = robotInstructService.list(robotWrapper);
                        for (RobotInstruct instruct:instructList){

                            robotService.startRobot(instruct);
                            transactionManager.commit(status);
                        }
                    } catch (Exception e) {
                        transactionManager.rollback(status);
                    }

                }else {
                    log.error("开启手动模式");
                }
            } else {
                log.error("没有配置电容室室信息");
            }
            resultMap.put("msg", "操作成功");
            resultMap.put("success",true);
        }catch (Exception e){
            resultMap.put("success", false);
            resultMap.put("msg", "操作失败");
        }
        return resultMap;
    }
}
