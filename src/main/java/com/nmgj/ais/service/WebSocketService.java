package com.nmgj.ais.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.constants.GlobalData;
import com.nmgj.ais.common.constants.Table;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * WebSocket消息处理.
 *
 */
@Slf4j
@Service
public class WebSocketService  {

	/**
	 * 广播报文.
	 *
	 * @param msg 报文
	 */
	public void broadcast(String msg) {
		String[] keys = GlobalData.webSocketSessionMap.keySet().toArray(new String[] {});
		for (String key : keys) {
			try {
				WebSocketSession session = GlobalData.webSocketSessionMap.get(key);
				session.sendMessage(new TextMessage(msg));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}



}
