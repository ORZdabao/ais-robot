package com.nmgj.ais.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.nmgj.ais.common.constants.DataTypeConstant;
import com.nmgj.ais.common.constants.Symbol.Regex;
import com.nmgj.ais.common.util.HCNetSDK;
import com.nmgj.ais.common.util.PatternX;
import com.nmgj.ais.common.util.PropertyUtil;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * 摄像头预置点调用
 *
 */
@Service
@Slf4j
public class PtzPresetService  {

	private static final String USERNAME = "admin";
	private static final String PASSWORD = "12345abc";

	private static List<Thread> ths = new ArrayList<>();



	@Autowired
    IRobotService robotService;

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String excute() {
		try {
			if (!ths.isEmpty()) {
				for (int i = 0; i < ths.size(); i++) {
					try {
						ths.get(i).stop();
						ths.remove(i);
						i--;
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			}
			Map<Integer, String> data = new HashMap<>();
			PropertyUtil.load(this.getClass().getResourceAsStream("/ptzPreset.properties"), data);
			List<Map<String, Object>> values = new ArrayList<>();
			for (int i = 0; i < 20; i++) {
				try {
					String str = data.get(i);
					if (str == null) {
						continue;
					}
					String[] arr = str.split(Regex.SPLIT);
					if (arr.length != 3) {
						log.error("{},摄像头预置点配置错误", i);
						continue;
					}
					// 摄像头IP
					List<String> ipL = new ArrayList<>();
					String[] ips = arr[0].split(Regex.COMMA);
					for (String ip : ips) {
						if (!PatternX.IP.matcher(ip).matches()) {
							log.error("{},IP错误", ip);
							continue;
						}
						ipL.add(ip);
					}
					if (ipL.isEmpty()) {
						continue;
					}
					// 预置点
					String[] iPresets = arr[1].split(Regex.COMMA);
					int[] ps = new int[iPresets.length];
					for (int j = 0; j < iPresets.length; j++) {
						ps[j] = Integer.valueOf(iPresets[j]);
					}
					// 预置点间隔时间(秒)
					int sleepTime = Integer.valueOf(arr[2]);
					Map<String, Object> value = new HashMap<>();
					value.put("ips", ipL);
					value.put("ps", ps);
					value.put("sleepTime", sleepTime);
					values.add(value);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
			log.error("获取数据为：{}", values);
			if (values.isEmpty()) {
				log.error("没有配置正确的摄像头预置点轮巡点位");
				return "没有配置正确的摄像头预置点轮巡点位";
			}
			Thread thT = new Thread() {
				public void run() {
					for (Map<String, Object> value : values) {
						List<String> ips = (List<String>) value.get("ips");
						CountDownLatch latch = new CountDownLatch(ips.size());
						int[] iPresets = (int[]) value.get("ps");
						int sleepTime = (int) value.get("sleepTime");
						for (String ip : ips) {
							Thread th = new Thread() {
								public void run() {
									call(ip, iPresets, sleepTime, latch);
								}
							};
							ths.add(th);
							th.start();
						}
						try {
							latch.await();
						} catch (Exception e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			};
			ths.add(thT);
			thT.start();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return "摄像头巡检失败";
		}
		return "摄像头巡检中";
	}

	/**
	 * 调用预置位
	 */
	public void call(String ip, int[] iPresets, int sleepTime, CountDownLatch latch) {
		// 注册设备
		int userId = -1;
		// 预览句柄
		int lPreviewHandle = -1;
		HCNetSDK hCNetSDK = null;
		try {
			log.info("开始摄像头{}预置点{},间隔时间{}", ip, Arrays.toString(iPresets), sleepTime);
			hCNetSDK = (HCNetSDK) Native.loadLibrary("D:\\dll\\HCNetSDK.dll", HCNetSDK.class);

			if (!hCNetSDK.NET_DVR_Init()) {
				log.error("海康SDK初始化失败{}", ip);
				return;
			}
			// 设备信息
			HCNetSDK.NET_DVR_DEVICEINFO_V30 devinfo = new HCNetSDK.NET_DVR_DEVICEINFO_V30();
			// 注册设备
			userId = hCNetSDK.NET_DVR_Login_V30(ip, (short) 8000, USERNAME, PASSWORD, devinfo);
			if (userId < 0) {
				log.error("设备{}注册失败,错误码:{}", ip, hCNetSDK.NET_DVR_GetLastError());
				return;
			}
			HCNetSDK.NET_DVR_WORKSTATE_V30 devwork = new HCNetSDK.NET_DVR_WORKSTATE_V30();
			if (!hCNetSDK.NET_DVR_GetDVRWorkState_V30(userId, devwork)) {
				// 返回Boolean值，判断是否获取设备能力
				log.error("返回设备{}状态失败", ip);
				return;
			}
			// 用户参数
			HCNetSDK.NET_DVR_CLIENTINFO m_strClientInfo = new HCNetSDK.NET_DVR_CLIENTINFO();
			m_strClientInfo.lChannel = 1;
			lPreviewHandle = hCNetSDK.NET_DVR_RealPlay_V30(userId, m_strClientInfo, null, null, true);

			for (int iPreset : iPresets) {
				if (!hCNetSDK.NET_DVR_PTZPreset(lPreviewHandle, HCNetSDK.GOTO_PRESET, iPreset)) {
					log.error("{}调用预置点{}失败", ip, iPreset);
					continue;
				}
				log.info("{}调用预置点{}成功", ip, iPreset);
				Thread.sleep(sleepTime * 1000);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			try {
				if (hCNetSDK != null) {
					if (lPreviewHandle != -1) {
						hCNetSDK.NET_DVR_StopRealPlay(lPreviewHandle);
					}
					if (userId != -1) {
						// 退出登录
						hCNetSDK.NET_DVR_Logout(userId);
					}
					hCNetSDK.NET_DVR_Cleanup();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			latch.countDown();
		}
	}


	public void callTemp(Long cameraId,String ip, int sleepTime) {
		// 注册设备
		int userId = -1;
		// 预览句柄
		int lPreviewHandle = -1;
		HCNetSDK hCNetSDK = null;

		try {
			log.info("开始摄像头{}间隔时间{}", ip,sleepTime);
			hCNetSDK = (HCNetSDK) Native.loadLibrary("D:\\dll\\HCNetSDK.dll", HCNetSDK.class);

			if (!hCNetSDK.NET_DVR_Init()) {
				log.error("海康SDK初始化失败{}", ip);
				return;
			}

			userId = loginV40(ip,(short) 8000,USERNAME, PASSWORD,hCNetSDK);

			if (userId < 0) {
				log.error("设备{}注册失败,错误码:{}", ip, hCNetSDK.NET_DVR_GetLastError());
				return;
			}
			// 获取热成像参数能力
//			String strURL = "GET /ISAPI/Thermal/capabilities";
//
//			HCNetSDK.BYTE_ARRAY ptrUrl = new HCNetSDK.BYTE_ARRAY(strURL.length());
//			System.arraycopy(strURL.getBytes(), 0, ptrUrl.byValue, 0, strURL.length());
//			ptrUrl.write();
//			HCNetSDK.NET_DVR_XML_CONFIG_INPUT struXMLInput = new HCNetSDK.NET_DVR_XML_CONFIG_INPUT();
//			struXMLInput.read();
//			struXMLInput.dwSize = struXMLInput.size();
//			struXMLInput.lpRequestUrl = ptrUrl.getPointer();
//			struXMLInput.dwRequestUrlLen = ptrUrl.byValue.length;
//			struXMLInput.lpInBuffer = null;
//			struXMLInput.dwInBufferSize = 0;
//			struXMLInput.write();
//
//			HCNetSDK.BYTE_ARRAY ptrStatusByte = new HCNetSDK.BYTE_ARRAY(4*4096);
//			ptrStatusByte.read();
//
//			HCNetSDK.BYTE_ARRAY ptrOutByte = new HCNetSDK.BYTE_ARRAY(1024*1024);
//			ptrOutByte.read();
//
//			HCNetSDK.NET_DVR_XML_CONFIG_OUTPUT struXMLOutput = new HCNetSDK.NET_DVR_XML_CONFIG_OUTPUT();
//			struXMLOutput.read();
//			struXMLOutput.dwSize = struXMLOutput.size();
//			struXMLOutput.lpOutBuffer = ptrOutByte.getPointer();
//			struXMLOutput.dwOutBufferSize = ptrOutByte.size();
//			struXMLOutput.lpStatusBuffer = ptrStatusByte.getPointer();
//			struXMLOutput.dwStatusSize  = ptrStatusByte.size();
//			struXMLOutput.write();
//			if(!hCNetSDK.NET_DVR_STDXMLConfig(userId, struXMLInput, struXMLOutput)) {
//				int iErr = hCNetSDK.NET_DVR_GetLastError();
//				log.info("iErr:{}",iErr);
//				return;
//			} else {
//				struXMLOutput.read();
//				ptrOutByte.read();
//				ptrStatusByte.read();
//				String strOutXML = new String(ptrOutByte.byValue).trim();
//				File file = FileUtil.writeBytes(strOutXML.getBytes(StandardCharsets.UTF_8),"D:\\ais\\test.txt");
//				return;
//			}

			// 建立长连接
			HCNetSDK.NET_DVR_REALTIME_THERMOMETRY_COND thermometry = new HCNetSDK.NET_DVR_REALTIME_THERMOMETRY_COND();
			thermometry.dwSize = thermometry.size();// 结构体大小
			thermometry.byRuleID = 0;// 规则ID
			thermometry.dwChan = 2;// 通道号
			thermometry.byMode =1;
			thermometry.wInterval = 1;

			thermometry.write();
			// 获取实时温度指令
			lPreviewHandle = hCNetSDK.NET_DVR_StartRemoteConfig(userId, HCNetSDK.NET_DVR_GET_REALTIME_THERMOMETRY, thermometry.getPointer(), thermometry.dwSize, new HCNetSDK.FRemoteConfigCallBack() {
				@Override
				public void invoke(int dwType, Pointer lpBuffer, int dwBufLen, Pointer pUserData) {

					log.info("ip:{}------ get temp success",ip);
					if(dwType == 0){
						// 回调状态值
						HCNetSDK.REMOTECONFIGSTATUS struCfgStatus  = new HCNetSDK.REMOTECONFIGSTATUS();
						struCfgStatus.write();
						Pointer pCfgStatus = struCfgStatus.getPointer();
						pCfgStatus.write(0, lpBuffer.getByteArray(0, struCfgStatus.size()), 0,struCfgStatus.size());
						struCfgStatus.read();

						int iStatus = 0;
						for(int i=0;i<4;i++)
						{
							int ioffset = i*8;
							int iByte = struCfgStatus.byStatus[i]&0xff;
							iStatus = iStatus + (iByte << ioffset);
						}

						switch (iStatus){
							case 1000:// NET_SDK_CALLBACK_STATUS_SUCCESS
								log.info("实时测温回调成功,dwStatus:" + iStatus);
								break;
							case 1001:
								log.info("正在获取实时测温回调数据中,dwStatus:" + iStatus);
								break;
							case 1002:
								int iErrorCode = 0;
								for(int i=0;i<4;i++)
								{
									int ioffset = i*8;
									int iByte = struCfgStatus.byErrorCode[i]&0xff;
									iErrorCode = iErrorCode + (iByte << ioffset);
								}
								log.info("获取实时测温回调数据失败, dwStatus:" + iStatus + "错误号:" + iErrorCode);
								break;
						}

					}else if(dwType == 2) {
						// 温度内容
						HCNetSDK.NET_DVR_THERMOMETRY_UPLOAD thermometryInfo = new HCNetSDK.NET_DVR_THERMOMETRY_UPLOAD();
						thermometryInfo.write();
						Pointer pInfoV30 = thermometryInfo.getPointer();
						pInfoV30.write(0, lpBuffer.getByteArray(0, thermometryInfo.size()), 0, thermometryInfo.size());
						thermometryInfo.read();
//						if (thermometryInfo.byRuleCalibType == 0) {
//							log.info("点测温信息:" + thermometryInfo.struPointThermCfg.fTemperature);
//						}

						if(thermometryInfo.byRuleCalibType == 1 || thermometryInfo.byRuleCalibType == 2){
							log.info("ip:{}框/线测温信息: maxTemp:{}",ip,thermometryInfo.struLinePolygonThermCfg.fMaxTemperature);
							String vaule = new BigDecimal(thermometryInfo.struLinePolygonThermCfg.fMaxTemperature).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
						}
					}
				}
			},null);

			ThreadUtil.sleep(sleepTime);

			if(lPreviewHandle < 0){
				log.error("get real temp failed:{}",hCNetSDK.NET_DVR_GetLastError());
			}
		} catch (Exception exception) {
			log.error("init hksdk failed");
			log.error(exception.getMessage());
		} finally {
			try {
				if (hCNetSDK != null) {
					log.info("stop hcNetSDK start ip:{}",ip);
					if (lPreviewHandle != -1) {
						// 关闭长连接
						log.info("stop hcNetSDK handle:{} ip:{}",lPreviewHandle,ip);
						hCNetSDK.NET_DVR_StopRemoteConfig(lPreviewHandle);
					}
					if (userId != -1) {
						// 退出登录
						hCNetSDK.NET_DVR_Logout(userId);
					}
					hCNetSDK.NET_DVR_Cleanup();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}


	private int loginV40(String ip, short port, String userName, String password, HCNetSDK hcNetSDK){
		int lUserID = -1;
		//注册
		HCNetSDK.NET_DVR_USER_LOGIN_INFO m_strLoginInfo = new HCNetSDK.NET_DVR_USER_LOGIN_INFO();//设备登录信息
		HCNetSDK.NET_DVR_DEVICEINFO_V40 m_strDeviceInfo = new HCNetSDK.NET_DVR_DEVICEINFO_V40();//设备信息

		String m_sDeviceIP = ip;//设备ip地址
		m_strLoginInfo.sDeviceAddress = new byte[HCNetSDK.NET_DVR_DEV_ADDRESS_MAX_LEN];
		System.arraycopy(m_sDeviceIP.getBytes(), 0, m_strLoginInfo.sDeviceAddress, 0, m_sDeviceIP.length());

		String m_sUsername = userName;//设备用户名
		m_strLoginInfo.sUserName = new byte[HCNetSDK.NET_DVR_LOGIN_USERNAME_MAX_LEN];
		System.arraycopy(m_sUsername.getBytes(), 0, m_strLoginInfo.sUserName, 0, m_sUsername.length());

		String m_sPassword = password;//设备密码
		m_strLoginInfo.sPassword = new byte[HCNetSDK.NET_DVR_LOGIN_PASSWD_MAX_LEN];
		System.arraycopy(m_sPassword.getBytes(), 0, m_strLoginInfo.sPassword, 0, m_sPassword.length());

		m_strLoginInfo.wPort = port;
		m_strLoginInfo.bUseAsynLogin = false; //是否异步登录：0- 否，1- 是
		m_strLoginInfo.byLoginMode=0;  //ISAPI登录
		m_strLoginInfo.write();

		lUserID = hcNetSDK.NET_DVR_Login_V40(m_strLoginInfo, m_strDeviceInfo);

		return lUserID;
	}

	public String catchPic(String ip,String path){
		String picName = null;
		// 注册设备
		int userId = -1;
		// 预览句柄
		int lPreviewHandle = -1;
		HCNetSDK hCNetSDK = null;

		try {
			hCNetSDK = (HCNetSDK) Native.loadLibrary("D:\\dll\\HCNetSDK.dll", HCNetSDK.class);

			if (!hCNetSDK.NET_DVR_Init()) {
				log.error("海康SDK初始化失败{}", ip);
				return picName;
			}

			userId = loginV40(ip, (short) 8000, USERNAME, PASSWORD, hCNetSDK);

			if (userId < 0) {
				log.error("设备{}注册失败,错误码:{}", ip, hCNetSDK.NET_DVR_GetLastError());
				return picName;
			}
			HCNetSDK.NET_DVR_JPEGPARA jpegpara = new HCNetSDK.NET_DVR_JPEGPARA();
			jpegpara.wPicQuality = 2;
			jpegpara.wPicSize = 2;

			// 非内存模式 直接保存图片至 告警文件夹，逻辑可以复用
			int dwSize = 1024*1024;
			//设置图片大小
			ByteBuffer jpegBuffer = ByteBuffer.allocate(1024 * 1024);

			Date date = new Date();
			picName =  DateUtil.format(date,"yyyy_M_d_H_m_s")+"_abnormal.jpeg";
			path = path + File.separator +picName;
			boolean flag = hCNetSDK.NET_DVR_CaptureJPEGPicture(userId,1,jpegpara,path.getBytes(StandardCharsets.UTF_8));
			if(!flag){
				log.error("catch pic failed:{}",hCNetSDK.NET_DVR_GetLastError());
			}
		}
		catch (Exception exception) {
			log.error("init hksdk failed");
			log.error(exception.getMessage());
		} finally {
			try {
				if (hCNetSDK != null) {
					log.info("stop hcNetSDK start ip:{}",ip);
					if (userId != -1) {
						// 退出登录
						hCNetSDK.NET_DVR_Logout(userId);
					}
					hCNetSDK.NET_DVR_Cleanup();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		return picName;
	}


	/**
	 * 摄像头雨刷
	 * @param ip
	 */
	public void cameraWiper(String ip){
		log.info("start camera wiper ip:{}",ip);
		// 注册设备
		int userId = -1;
		// 预览句柄
		int lPreviewHandle = -1;
		HCNetSDK hCNetSDK = null;

		try {
			hCNetSDK = (HCNetSDK) Native.loadLibrary("D:\\dll\\HCNetSDK.dll", HCNetSDK.class);

			if (!hCNetSDK.NET_DVR_Init()) {
				log.error("海康SDK初始化失败{}", ip);
				return;
			}

			userId = loginV40(ip, (short) 8000, USERNAME, PASSWORD, hCNetSDK);

			if (userId < 0) {
				log.error("设备{}注册失败,错误码:{}", ip, hCNetSDK.NET_DVR_GetLastError());
				return;
			}

			HCNetSDK.NET_DVR_CLIENTINFO m_strClientInfo = new HCNetSDK.NET_DVR_CLIENTINFO();
			m_strClientInfo.lChannel = 1;
			lPreviewHandle = hCNetSDK.NET_DVR_RealPlay_V30(userId, m_strClientInfo, null, null, true);
			if(lPreviewHandle < 0){
				log.error("start hcsdk wiper failed:{}",hCNetSDK.NET_DVR_GetLastError());
				return;
			}

			// 开启雨刷
			hCNetSDK.NET_DVR_PTZControl(lPreviewHandle, HCNetSDK.WIPER_PWRON, 0);

			ThreadUtil.sleep(1000);

			// 雨停
			hCNetSDK.NET_DVR_PTZControl(lPreviewHandle, HCNetSDK.WIPER_PWRON, 1);
		}
		catch (Exception exception) {
			log.error("init hksdk failed");
			log.error(exception.getMessage());
		} finally {
			try {
				if (hCNetSDK != null) {
					log.info("stop hcsdk wiper start ip:{}",ip);
					if (lPreviewHandle != -1) {
						hCNetSDK.NET_DVR_StopRealPlay(lPreviewHandle);
					}
					if (userId != -1) {
						// 退出登录
						hCNetSDK.NET_DVR_Logout(userId);
					}
					hCNetSDK.NET_DVR_Cleanup();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
}
