package com.nmgj.ais.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.nmgj.ais.pojo.RobotCamera;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
public interface IRobotCameraService extends IService<RobotCamera> {

    public void getPic();

    public void movePosition();

    public List<String> getPicHistory(int type);

}
