package com.nmgj.ais.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nmgj.ais.pojo.RobotInstruct;

public interface IRobotInstructService extends IService<RobotInstruct> {

}
