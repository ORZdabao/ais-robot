package com.nmgj.ais.service;

import com.nmgj.ais.vo.AlarmVo;

import java.util.List;

public interface IHomePageService {

    public List<AlarmVo> queryPowerInfo();

    public List<AlarmVo> queryCapacityInfo();
}
