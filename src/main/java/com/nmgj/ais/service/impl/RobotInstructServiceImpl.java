package com.nmgj.ais.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nmgj.ais.dao.IRobotInstructDao;
import com.nmgj.ais.pojo.RobotInstruct;
import com.nmgj.ais.service.IRobotInstructService;
import org.springframework.stereotype.Service;

/**
 * @ClassName RobotInstructServiceImpl
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/11/14 23:39
 * @return
 **/
@Service
public class RobotInstructServiceImpl extends ServiceImpl<IRobotInstructDao, RobotInstruct> implements IRobotInstructService {

}
