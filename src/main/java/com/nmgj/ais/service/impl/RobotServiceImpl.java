package com.nmgj.ais.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.constants.GlobalData;
import com.nmgj.ais.common.util.CommonTools;
import com.nmgj.ais.config.socket.MySocket;
import com.nmgj.ais.dao.IRobotCameraDao;
import com.nmgj.ais.dao.IRobotDao;
import com.nmgj.ais.pojo.InspectionRecord;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotCamera;
import com.nmgj.ais.pojo.RobotInstruct;
import com.nmgj.ais.service.*;
import com.nmgj.ais.vo.AlarmVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
public class RobotServiceImpl extends ServiceImpl<IRobotDao, Robot> implements IRobotService {

    @Autowired
    private IRobotInstructService robotInstructService;

    @Autowired
    private IRobotInspectionRecordService recordService;

    @Autowired
    private IRobotDao robotDao;

    @Resource
    private PtzPresetService presetService;

    @Resource
    private IRobotCameraDao cameraDao;

    @Resource
    WebSocketService webSocketService;

    @Override
    public Map<String,Object> startRobot(RobotInstruct robotInstruct) {
        LocalDateTime taskTime = LocalDateTime.now();
        Map<String,Object> resultMap = new HashMap<String,Object>();
        //  根据id 查询需要用到哪些指令
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("region_id",robotInstruct.getRegionId());
        queryWrapper.eq("track_id",robotInstruct.getTrackId());
        List<RobotInstruct> list =  robotInstructService.list(queryWrapper);
        if(CollectionUtil.isEmpty(list)){
            resultMap.put("success",false);
            resultMap.put("msg","该区域的轨道机没有相关指令");
            return resultMap;
        }
        RobotInstruct instruct = list.get(0);
        String startInstruct = instruct.getStartInstruct();
        String ip = null;
        if(1== robotInstruct.getRegionId()){
             ip =  Constants.Robot.ROBOT_POWER_HOST ;
        } else {
            ip =  Constants.Robot.ROBOT_CAPACITOR_HOST;
        }

        int port = Integer.valueOf(Constants.Robot.ROBOT_PORT);
        // 任务完成状态
        int status = 1 ;
        // 截图的图片名
        String picName = null;
        String prefix = (1== robotInstruct.getRegionId())?"/images/dls":"/images/drs";
        try{
            MySocket socket = new MySocket(ip,port);
            log.info("创建socket成功");
            CommonTools.sendDataToServer(socket.getDataOutputStream(), socket.getSocket(),startInstruct);
            String firstResult = CommonTools.serverResponse(socket.getSocket());
            if(StrUtil.isEmpty(firstResult)){
                resultMap.put("success",false);
                resultMap.put("msg","返回指令为空");
            }
            log.info("第一条指令是："+firstResult);
            String secondResult = CommonTools.serverResponse(socket.getSocket());
            if(StrUtil.isEmpty(secondResult)){
                resultMap.put("success",false);
                resultMap.put("msg","返回指令为空");
            }
            log.info("第二条指令是："+secondResult);

            // 更新预置位置
            int position = instruct.getPosition();
            String path = null;
            String cameraIP = null;
            if(1== robotInstruct.getRegionId()){
                GlobalData.currentRobotPresetMap.put(Constants.Robot.POWER_REDIS_KEY,String.valueOf(robotInstruct.getTrackId()));
                path = Constants.Robot.CAMERA_PIC_URL+File.separator+"dls";
                cameraIP = Constants.Robot.CAMERA_ROBOT_POWER_HOST;
            } else {
                GlobalData.currentRobotPresetMap.put(Constants.Robot.CAPACITY_REDIS_KEY,String.valueOf(robotInstruct.getTrackId()));
                path = Constants.Robot.CAMERA_PIC_URL+File.separator+"drs";
                cameraIP = Constants.Robot.CAMERA_ROBOT_CAPACITOR_HOST;
            }
            String socketText =GlobalData.currentRobotPresetMap.get(Constants.Robot.POWER_REDIS_KEY)+","+GlobalData.currentRobotPresetMap.get(Constants.Robot.CAPACITY_REDIS_KEY);
            log.info("发送socket:"+socketText);
            webSocketService.broadcast(socketText);
            log.info("调用的预置指令是："+position);
            int[] callArr = new int[]{position};



            // 截图名字 需要存放到巡检记录
            presetService.call(cameraIP,callArr,6,new CountDownLatch(1));
            while(true){
                picName = presetService.catchPic(cameraIP,path);
                File file = new File(path+File.separator+picName);
                if(file.exists()){
                    break;
                }
            }


            // 更新机器人点位信息
            Robot robot = new Robot();
            robot.setId(robotInstruct.getRegionId());
            robot.setPosition(robotInstruct.getTrackId());
            robotDao.updateById(robot);

        } catch (Exception e){
            log.error("使用socket异常："+e.getMessage(),e);
            status = 0;
            resultMap.put("success",false);
            resultMap.put("msg","使用socket异常");
        }finally {
            insertRecord(robotInstruct,status,taskTime,prefix+"/"+picName);
        }
        resultMap.put("success",true);
        resultMap.put("msg","启动成功");
        return resultMap;
    }

    @Override
    public Map<String, Object> queryStatus(int id) {
        Map<String,Object> resultMap = new HashMap<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id",id);
        try{
            List<Robot> list = robotDao.selectList(queryWrapper);
            resultMap.put("success",true);
            if(CollectionUtil.isNotEmpty(list)){
                resultMap.put("data",list.get(0));
            } else {
                resultMap.put("data","");
            }

        } catch (Exception e){
            resultMap.put("success",false);
            log.error("查询异常，错误信息是："+e.getMessage(),e);
        }
        return resultMap;
    }

    @Override
    public Map<String, Object> updateRobotModel(Robot robot) {
        Map<String,Object> resultMap = new HashMap<>();

        try{
            robotDao.updateById(robot);
            resultMap.put("success",true);
        } catch (Exception e){
            resultMap.put("success",false);
            log.error("操作异常，错误信息是："+e.getMessage(),e);
        }
        return resultMap;
    }


    private void insertRecord(RobotInstruct robotInstruct,int status,LocalDateTime taskTime,String picName){
        try{
            // 获取温湿度功能
            String userId = "2";
            Map<String,Object> queryMap = new HashMap<String,Object>();
            LocalDateTime nowTime = LocalDateTime.now();
            LocalDateTime startTime = nowTime.minusMinutes(5);
            queryMap.put("isAlarmData",-1);
            queryMap.put("nodeId",1);
            queryMap.put("beginTime",startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            queryMap.put("endTime",nowTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            if(1== robotInstruct.getTrackId()){
                queryMap.put("deviceKey", Constants.Robot.DEVICE_POWER_KEY);
            } else {
                queryMap.put("deviceKey", Constants.Robot.DEVICE_CAPACITOR_KEY);
            }
            String alarmResult = null;
            AlarmVo vo = new AlarmVo();
            String tem = null;
            String hum = null;
            try{
                alarmResult =  HttpRequest.get(Constants.Robot.MANAGER_HOST+":"+Constants.Robot.PORT+"/app/QueryHistoryList").header("userId",userId).form(queryMap).timeout(5000).execute().body();
                if(StrUtil.isNotBlank(alarmResult)){
                    JSONObject alarmObject = JSONUtil.parseObj(alarmResult);
                    List<AlarmVo> alarmList = new ArrayList<>();
                    if(StrUtil.isNotBlank(alarmResult) && "1000".equals(alarmObject.getStr("code"))){
                        String powerObj = alarmObject.getStr("data");
                        alarmList = JSON.parseArray(powerObj, AlarmVo.class);
                    }
                    if(CollectionUtil.isNotEmpty(alarmList)){
                        vo = alarmList.get(0);
                        tem = vo.getTem();
                        hum = vo.getHum();
                    }
                }
            } catch (Exception e){
                log.error("连接工控机异常,"+e.getMessage(),e);
            }


            InspectionRecord inspectionRecord = new InspectionRecord();
            inspectionRecord.setRegion(1== robotInstruct.getRegionId()?Constants.Robot.ROBOT_POWER_NAME:Constants.Robot.ROBOT_CAPACITOR_NAME);
            inspectionRecord.setRegionId(String.valueOf(robotInstruct.getTrackId()));
            inspectionRecord.setRegionName("预置位"+robotInstruct.getTrackId());
            inspectionRecord.setStartTime(taskTime);
            inspectionRecord.setEndTime(nowTime);
            inspectionRecord.setTem(tem);
            inspectionRecord.setHum(hum);
            inspectionRecord.setStatus(status);
            inspectionRecord.setPicUrl(picName);
            recordService.save(inspectionRecord);
        } catch (Exception e){
            log.error("插入记录失败："+e.getMessage(),e);
        }

    }



}
