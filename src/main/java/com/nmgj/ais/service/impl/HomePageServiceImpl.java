package com.nmgj.ais.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.util.JsonUtil;
import com.nmgj.ais.service.IHomePageService;
import com.nmgj.ais.vo.AlarmVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName HomePageServiceImpl
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/25 23:37
 * @return
 **/
@Service
@Slf4j
public class HomePageServiceImpl implements IHomePageService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public List<AlarmVo> queryPowerInfo() {
        List<AlarmVo> list = new ArrayList<AlarmVo>();
        // 动力室
   //     Object id = redisTemplate.opsForValue().get("robot_userId");
   //     String userId = ObjectUtil.isEmpty(id)?"2": StrUtil.toString(id);
        String userId = "2";
        Map<String,Object> queryMap = new HashMap<String,Object>();
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime startTime = time.minusHours(1);
        queryMap.put("deviceKey", Constants.Robot.DEVICE_POWER_KEY);
        queryMap.put("isAlarmData",-1);
        queryMap.put("nodeId",1);
        queryMap.put("beginTime",startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        queryMap.put("endTime",time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        String powerResult = HttpRequest.get(Constants.Robot.MANAGER_HOST+":"+Constants.Robot.PORT+"/app/QueryHistoryList").header("userId",userId).form(queryMap).timeout(5000).execute().body();
        JSONObject powerObject = JSONUtil.parseObj(powerResult);
        if(StrUtil.isNotBlank(powerResult) && "1000".equals(powerObject.getStr("code"))){
            String powerObj = powerObject.getStr("data");
            list = JSON.parseArray(powerObj,AlarmVo.class);
        }
        return list;
    }

    @Override
    public List<AlarmVo> queryCapacityInfo() {
        List<AlarmVo> list = new ArrayList<AlarmVo>();
        // 动力室
   //     Object id = redisTemplate.opsForValue().get("robot_userId");
  //      String userId = ObjectUtil.isEmpty(id)?"2": StrUtil.toString(id);
        String userId = "2";
        Map<String,Object> queryMap = new HashMap<String,Object>();
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime startTime = time.minusHours(1);
        queryMap.put("isAlarmData",-1);
        queryMap.put("nodeId",1);
        queryMap.put("beginTime",startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        queryMap.put("endTime",time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        queryMap.put("deviceKey", Constants.Robot.DEVICE_CAPACITOR_KEY);
        String capacityResult = HttpRequest.get(Constants.Robot.MANAGER_HOST+":"+Constants.Robot.PORT+"/app/QueryHistoryList").header("userId",userId).form(queryMap).timeout(5000).execute().body();
        JSONObject capacityObject = JSONUtil.parseObj(capacityResult);

        //log.error("电容室的历史数据是："+capacityResult);
        if(StrUtil.isNotBlank(capacityResult) && "1000".equals(capacityObject.getStr("code"))){
            String capacityObj = JSONUtil.parseObj(capacityResult).getStr("data");
            list = JSON.parseArray(capacityObj,AlarmVo.class);
        }
        return list;

    }
}
