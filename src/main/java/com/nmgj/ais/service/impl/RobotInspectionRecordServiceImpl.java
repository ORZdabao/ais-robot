package com.nmgj.ais.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nmgj.ais.dao.InspectionRecordDao;
import com.nmgj.ais.pojo.InspectionRecord;
import com.nmgj.ais.service.IRobotInspectionRecordService;
import com.nmgj.ais.vo.InspectionVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
@Service
public class RobotInspectionRecordServiceImpl extends ServiceImpl<InspectionRecordDao, InspectionRecord> implements IRobotInspectionRecordService {

    @Resource
    private InspectionRecordDao inspectionRecorddao;

    /**
     * @Author jhyang
     * @Description 查询列表
     * @Date 21:13 2023/10/21
     * @Param [vo, index, page]
     * @return java.util.List<rbtctrl.vo.RbtInspectionVo>
     **/
    @Override
    public IPage<InspectionVo> queryList(InspectionVo vo, int index, int page) {

        return inspectionRecorddao.queryList(new Page<>(index, page),  vo);
    }

    /**
     * @Author jhyang
     * @Description 创建任务
     * @Date 21:13 2023/10/21
     * @Param [vo]
     * @return void
     **/
    @Override
    public void createInspection(InspectionRecord record) {
        inspectionRecorddao.insert(record);
    }
}
