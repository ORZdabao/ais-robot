package com.nmgj.ais.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.dao.IRobotCameraDao;
import com.nmgj.ais.pojo.RobotCamera;
import com.nmgj.ais.service.IRobotCameraService;
import com.nmgj.ais.service.IRobotInspectionRecordService;
import com.nmgj.ais.service.PtzPresetService;
import com.nmgj.ais.vo.InspectionVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName RobotCameraServiceImpl
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/11/9 20:34
 * @return
 **/
@Service
@Slf4j
public class RobotCameraServiceImpl extends ServiceImpl<IRobotCameraDao, RobotCamera> implements IRobotCameraService {

    @Resource
    private PtzPresetService presetService;

    @Resource
    private IRobotCameraDao dao;

    @Resource
    private IRobotInspectionRecordService  recordService;

    /**
     * @Author jhyang
     * @Description 测试拍照
     * @Date 16:10 2023/11/25
     * @Param []
     * @return void
     **/
    @Override
    public void getPic() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("id",1);
        List<RobotCamera> list = dao.selectList(wrapper);
        presetService.catchPic(list.get(0).getDevIp(),"D:");
    }
    /**
     * @Author jhyang
     * @Description 测试预置位用
     * @Date 16:11 2023/11/25
     * @Param []
     * @return void
     **/
    @Override
    public void movePosition() {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("id",1);
        List<RobotCamera> list = dao.selectList(wrapper);
        presetService.call(list.get(0).getDevIp(),new int[]{1} ,1000,new CountDownLatch(1));
    }

    @Override
    public List<String> getPicHistory(int type) {
        InspectionVo vo = new InspectionVo();
        vo.setRegionId(String.valueOf(type));
        IPage<InspectionVo> page= recordService.queryList(vo,1,20);

        List<String> resultList = new ArrayList<String>();

        for (InspectionVo inspectionVo :page.getRecords()){
            if(StrUtil.isNotBlank(inspectionVo.getPicUrl())){
                resultList.add(inspectionVo.getPicUrl());
            }
        }
        return resultList;
    }
}
