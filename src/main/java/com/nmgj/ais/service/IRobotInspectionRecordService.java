package com.nmgj.ais.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nmgj.ais.pojo.InspectionRecord;
import com.nmgj.ais.vo.InspectionVo;


import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
public interface IRobotInspectionRecordService extends IService<InspectionRecord> {

     IPage<InspectionVo> queryList(InspectionVo vo, int index, int page);

     void createInspection(InspectionRecord record);
}
