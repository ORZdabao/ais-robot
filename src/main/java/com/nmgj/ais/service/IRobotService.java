package com.nmgj.ais.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotInstruct;

import java.util.Map;

public interface IRobotService extends IService<Robot> {

    public Map<String,Object> startRobot(RobotInstruct robotInstruct);

    public Map<String,Object> queryStatus(int id );

    public Map<String,Object> updateRobotModel(Robot robot );
}
