package com.nmgj.ais.service;

import com.nmgj.ais.common.constants.Constants.DevType;
import com.nmgj.ais.common.constants.Symbol;
import com.nmgj.ais.common.constants.Table;
import com.nmgj.ais.dao.ICommonDao;
import com.nmgj.ais.pojo.Condition;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 公共业务处理类.
 *
 */
@Service
public class CommonService {

	/**
	 * 公共dao.
	 */
	@Resource
	private ICommonDao commonDao;

	/**
	 * 插入一条数据.
	 *
	 * @param tbl 表名
	 * @param obj 一条数据
	 * @return 成功条目数
	 */
	public int addOne(String tbl, Map<String, Object> obj) {
		return commonDao.insertOne(tbl, obj);
	}

	/**
	 * 插入一条需要返回主键值的数据.
	 *
	 * @param tbl 表名
	 * @param obj 一条数据
	 * @return 成功条目数
	 */
	public int addPKOne(String tbl, Map<String, Object> obj) {
		if (tbl.equals(Table.EN.CAMERA)) {
			if (obj.get("showIndex") != null && Integer.valueOf(obj.get("showIndex").toString()) > 0) {
				Map<String, String> params = new HashMap<>();
				params.put("showIndex", ">,0");
				if (countByKeys(Table.EN.CAMERA, params) >= 9) {
					return 0;
				}
			}
		}
		return commonDao.insertPKOne(tbl, obj);
	}

	/**
	 * 查询所有记录数
	 *
	 * @param tbl 表名
	 * @return 所有记录数
	 */
	public int count(String tbl) {
		return commonDao.count(tbl);
	}

	/**
	 * 通过多个字段值查询记录数
	 *
	 * @param tbl    表名
	 * @param params 多个字段
	 * @return 记录数
	 */
	public int countByKeys(String tbl, Map<String, String> params) {
		if (params.isEmpty()) {
			return count(tbl);
		}
		List<Condition> conditions = new ArrayList<Condition>(params.size());
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			int index = value.indexOf(Symbol.Common.COMMA);
			String operator = value.substring(0, index);
			value = value.substring(index + 1);
			List<Condition> cs = getCondition(entry.getKey(), value, operator);
			conditions.addAll(cs);
		}
		if (conditions.size() == 1) {
			return commonDao.countByKey(tbl, conditions.get(0));
		}
		return commonDao.countByKeys(tbl, conditions);
	}

	/**
	 * 分组查询单字段个数
	 *
	 * @param tbl    表名
	 * @param column 分组字段
	 * @return 分组字段条目数
	 */
	public Map<String, Integer> countGroup(String tbl, String column) {
		List<Map<String, Object>> objs = commonDao.countGroup(tbl, column);
		Map<String, Integer> ret = new HashMap<String, Integer>();
		for (Map<String, Object> obj : objs) {
			ret.put(obj.get(column).toString(), Integer.valueOf(obj.get("num").toString()));
		}
		return ret;
	}

	/**
	 * 按单个条件删除记录
	 *
	 * @param tbl   表名
	 * @param key   键
	 * @param value 值
	 * @return 成功条目数
	 */
	public int deleteByKey(String tbl, String key, Object value) {
		return commonDao.deleteByKey(tbl, key, value);
	}

	/**
	 * 按多个条件删除记录
	 *
	 * @param tbl    表名
	 * @param params 多个条件
	 * @return 成功条目数
	 */
	public int deleteByKeys(String tbl, Map<String, String> params) {
		if (params.isEmpty()) {
			return 0;
		}
		List<Condition> conditions = new ArrayList<Condition>();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			int index = value.indexOf(Symbol.Common.COMMA);
			String operator = value.substring(0, index);
			value = value.substring(index + 1);
			List<Condition> cs = getCondition(entry.getKey(), value, operator);
			conditions.addAll(cs);
		}
		return commonDao.deleteByKeys(tbl, conditions);
	}

	/**
	 * 按主键删除.
	 *
	 * @param tbl 表名
	 * @param id  主键
	 * @return 删除条目数
	 */
	public int deleteByPk(String tbl, long pk) {
		return commonDao.deleteByPk(tbl, pk);
	}

	/**
	 * 按主键批量删除.
	 *
	 * @param tbl 表名
	 * @param ids 编号集合
	 * @return 删除条目数
	 */
	public int deleteByPks(String tbl, List<Long> pks) {
		for (Long pk : pks) {
			commonDao.deleteByPk(tbl, pk);
			if (tbl.equals(Table.EN.BELT)) {
				List<Condition> conditions = new ArrayList<>();
				conditions.add(new Condition("devType", DevType.BELT));
				conditions.add(new Condition("devId", pk));
				commonDao.deleteByKeys(Table.EN.READPARAM, conditions);
				commonDao.deleteByKeys(Table.EN.THRESHOLD, conditions);
				commonDao.deleteByKeys(Table.EN.ACTIVEALARM, conditions);
			} else if (tbl.equals(Table.EN.OTHERDEV)) {
				List<Condition> conditions = new ArrayList<>();
				conditions.add(new Condition("devType", DevType.OTHER));
				conditions.add(new Condition("devId", pk));
				commonDao.deleteByKeys(Table.EN.READPARAM, conditions);
				commonDao.deleteByKeys(Table.EN.THRESHOLD, conditions);
				commonDao.deleteByKeys(Table.EN.ACTIVEALARM, conditions);
			}
		}
		return 1;
	}

	/**
	 * 获取查询条件.
	 *
	 * @param key      键
	 * @param value    值
	 * @param operator 操作符
	 * @return 查询条件
	 */
	public List<Condition> getCondition(String key, Object value, String... operator) {
		List<Condition> conditions = new ArrayList<Condition>();
		Condition condition = new Condition();
		condition.setKey(key);
		if (operator.length == 0) {
			condition.setOperator("=");
			condition.setValue(value);
			conditions.add(condition);
		} else {
			String op = operator[0];
			if (op.equals(">=<=") || op.equals(">=<")) {
				String[] v = value.toString().split(Symbol.Regex.COMMA);
				condition.setOperator(">=");
				condition.setValue(v[0]);
				conditions.add(condition);

				condition = new Condition();
				condition.setKey(key);
				if (op.equals(">=<")) {
					condition.setOperator("<");
				} else {
					condition.setOperator("<=");
				}
				condition.setValue(v[1]);
				conditions.add(condition);
			} else if (op.equals("in") || op.equals("not in")) {
				condition.setOperator(op);
				condition.setValue(Arrays.asList(value.toString().split(Symbol.Regex.COMMA)));
				conditions.add(condition);
			} else {
				condition.setOperator(op);
				condition.setValue(value);
				conditions.add(condition);
			}
		}
		return conditions;
	}

	/**
	 * 查询所有记录
	 *
	 * @param tbl   表名
	 * @param order 排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> find(String tbl, String order) {
		return commonDao.select(tbl, order);
	}

	/**
	 * 通过单个字段值查询多条记录
	 *
	 * @param tbl      表名
	 * @param key      字段
	 * @param value    字段值
	 * @param operator 查询操作符,缺省=
	 * @return 多条记录
	 */
	public List<Map<String, Object>> findByKey(String tbl, String key, Object value, String... operator) {
		List<Condition> conditions = getCondition(key, value, operator);
		if (conditions.size() == 1) {
			return commonDao.selectByKey(tbl, conditions.get(0), null);
		} else {
			return commonDao.selectByKeys(tbl, conditions, null);
		}
	}

	/**
	 * 通过单个字段值查询一条记录
	 *
	 * @param tbl      表名
	 * @param key      字段
	 * @param value    字段值
	 * @param operator 查询操作符,缺省=
	 * @return 一条记录
	 */
	public Map<String, Object> findByKeyOne(String tbl, String key, Object value, String... operator) {
		List<Condition> conditions = getCondition(key, value, operator);
		if (conditions.size() == 1) {
			return commonDao.selectByKeyOne(tbl, conditions.get(0));
		} else {
			return commonDao.selectByKeysOne(tbl, conditions);
		}
	}

	/**
	 * 通过多个字段值查询多条记录(不分页)
	 *
	 * @param tbl    表名
	 * @param params 多个字段
	 * @param order  排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> findByKeys(String tbl, Map<String, String> params, String order) {
		if (params.isEmpty()) {
			return find(tbl, order);
		}
		List<Condition> conditions = new ArrayList<Condition>();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			int index = value.indexOf(Symbol.Common.COMMA);
			String operator = value.substring(0, index);
			value = value.substring(index + 1);
			List<Condition> cs = getCondition(entry.getKey(), value, operator);
			conditions.addAll(cs);
		}
		if (conditions.size() == 1) {
			return commonDao.selectByKey(tbl, conditions.get(0), order);
		}
		return commonDao.selectByKeys(tbl, conditions, order);
	}

	/**
	 * 通过多个字段值查询多条记录(分页)
	 *
	 * @param tbl    表名
	 * @param params 多个字段
	 * @param start  起始下标
	 * @param limit  查询个数
	 * @param order  排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> findByKeysPaging(String tbl, Map<String, String> params, int start, int limit,
			String order) {
		if (params.isEmpty()) {
			return findPaging(tbl, start, limit, order);
		}
		List<Condition> conditions = new ArrayList<Condition>(params.size());
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			int index = value.indexOf(Symbol.Common.COMMA);
			String operator = value.substring(0, index);
			value = value.substring(index + 1);
			List<Condition> cs = getCondition(entry.getKey(), value, operator);
			conditions.addAll(cs);
		}
		if (conditions.size() == 1) {
			return commonDao.selectByKeyPaging(tbl, conditions.get(0), start, limit, order);
		}
		return commonDao.selectByKeysPaging(tbl, conditions, start, limit, order);
	}

	/**
	 * 查询所有记录(分页)
	 *
	 * @param tbl   表名
	 * @param start 起始下标
	 * @param limit 查询个数
	 * @param order 排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> findPaging(String tbl, int start, int limit, String order) {
		return commonDao.selectPaging(tbl, start, limit, order);
	}

	/**
	 * 通过主键查询
	 *
	 * @param tbl 表名
	 * @param id  主键
	 * @return 一条记录
	 */
	public Map<String, Object> findByPK(String tbl, long id) {
		return commonDao.selectByPK(tbl, id);
	}

	/**
	 * 按单个条件更新记录
	 *
	 * @param tbl      表名
	 * @param obj      更新域
	 * @param key      键
	 * @param value    值
	 * @param operator 操作符
	 * @return 成功条目数
	 */
	public int updateByKey(String tbl, Map<String, Object> obj, String key, Object value, String... operator) {
		List<Condition> conditions = getCondition(key, value, operator);
		if (conditions.size() == 1) {
			return commonDao.updateByKey(tbl, conditions.get(0), obj);
		} else {
			return commonDao.updateByKeys(tbl, conditions, obj);
		}
	}

	/**
	 * 按多个条件更新记录
	 *
	 * @param tbl    表名
	 * @param obj    更新域
	 * @param params 多个条件
	 * @return 成功条目数
	 */
	public int updateByKeys(String tbl, Map<String, Object> obj, Map<String, String> params) {
		if (params.isEmpty()) {
			return 0;
		}
		List<Condition> conditions = new ArrayList<Condition>();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			int index = value.indexOf(Symbol.Common.COMMA);
			String operator = value.substring(0, index);
			value = value.substring(index + 1);
			List<Condition> cs = getCondition(entry.getKey(), value, operator);
			conditions.addAll(cs);
		}
		if (conditions.size() == 1) {
			return commonDao.updateByKey(tbl, conditions.get(0), obj);
		}
		return commonDao.updateByKeys(tbl, conditions, obj);
	}

	/**
	 * 按主键更新一条记录
	 *
	 * @param tbl 表名
	 * @param pk  主键值
	 * @param obj 更新域(一条记录)
	 * @return 成功条目数
	 */
	public int updateByPk(String tbl, long pk, Map<String, Object> obj) {
		if (tbl.equals(Table.EN.CAMERA)) {
			if (obj.get("showIndex") != null && Integer.valueOf(obj.get("showIndex").toString()) > 0) {
				Map<String, String> params = new HashMap<>();
				params.put("showIndex", ">,0");
				if (countByKeys(Table.EN.CAMERA, params) >= 9) {
					return 0;
				}
			}
		}
		return commonDao.updateByPk(tbl, pk, obj);
	}

	/**
	 * 按一个条件更新一个字段
	 *
	 * @param tbl       表名
	 * @param condition 查询条件
	 * @param column    更新列
	 * @param value     更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByKey(String tbl, Condition condition, String column, Object value) {
		return commonDao.updateOneColumnByKey(tbl, condition, column, value);
	}

	/**
	 * 按多个条件更新一个字段
	 *
	 * @param tbl        表名
	 * @param conditions 查询条件
	 * @param column     更新列
	 * @param value      更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByKeys(String tbl, List<Condition> conditions, String column, Object value) {
		return commonDao.updateOneColumnByKeys(tbl, conditions, column, value);
	}

	/**
	 * 按主键更新一个字段
	 *
	 * @param tbl    表名
	 * @param pk     主键值
	 * @param column 更新列
	 * @param value  更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByPk(String tbl, long pk, String column, Object value) {
		return commonDao.updateOneColumnByPk(tbl, pk, column, value);
	}
}
