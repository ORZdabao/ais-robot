package com.nmgj.ais.test;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.util.JsonUtil;
import com.nmgj.ais.dao.IRobotCameraDao;
import com.nmgj.ais.pojo.RobotCamera;
import com.nmgj.ais.service.IRobotCameraService;
import com.nmgj.ais.service.IRobotInspectionRecordService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ServiceTest
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/22 16:30
 * @return
 **/
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:applicationContext.xml"})
public class ServiceTest {

    @Autowired
    private IRobotInspectionRecordService service;

    @Autowired
    private IRobotCameraService robotCameraService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private IRobotCameraDao cameraDao;


    @Test
    public void test(){
     /*   InspectionVo vo = new InspectionVo();
        vo.setName("高压室自动巡检");
        List<InspectionVo> list = service.queryList(vo,1,10);
        System.out.println(list.size());*/

      /*  String name = jedisUtil.setJedisPool();
        System.out.println("11111="+name);*/
   /*     JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","1000");
        jsonObject.put("message","111");

        JSONObject data = new JSONObject();
        data.put("userId","123456");
        jsonObject.put("data",data);
        String json = jsonObject.toString();
        String userId = JsonUtil.getValue(json,"data.userId",String.class);
        System.out.println(userId);*/
   /*     List<RobotCamera> list = robotCameraService.list();
        System.out.println(list);*/
/*        redisTemplate.opsForValue().set(Constants.Robot.POWER_REDIS_KEY,new String("1"));
        Object obj = redisTemplate.opsForValue().get(Constants.Robot.POWER_REDIS_KEY);
        System.out.println("obj的值是"+obj.toString());*/
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("id",1);
        List<RobotCamera> cameraList = cameraDao.selectList(wrapper);
        log.info("6666"+cameraList);
    }
}
