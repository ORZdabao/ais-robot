package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-12
 */
@Data
@TableName("rbt_user")
//@ApiModel(value = "RbtUser对象", description = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

 //   @ApiModelProperty("所属单位")
    private String company;

 //   @ApiModelProperty("所属部门")
    private String department;

 //   @ApiModelProperty("密码")
    private String password;

//    @ApiModelProperty("联系方式")
    private String phoneNo;

//    @ApiModelProperty("角色 1:管理员 2:普通用户")
    private Integer role;

 //   @ApiModelProperty("用户真实姓名")
    private String trueName;

//    @ApiModelProperty("用户名")
    private String userName;


}
