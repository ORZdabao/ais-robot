package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
@Data
@TableName(" robot_inspection_record")
public class InspectionRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String region;

    @TableField("region_id")
    private String regionId;

    @TableField("region_name")
    private String regionName;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private String tem;

    private String hum;

    private String comment;

    @TableField("pic_url")
    private String picUrl;

    private Integer status;

}
