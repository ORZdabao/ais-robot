package com.nmgj.ais.pojo;

/**
 * 查询条件.
 * 
 */
public class Condition {

	/**
	 * 条件字段
	 */
	private String key;

	/**
	 * 操作符 缺省=
	 */
	private String operator = "=";

	/**
	 * 条件的值
	 */
	private Object value;

	/**
	 * 无参的构造函数.
	 */
	public Condition() {

	}

	/**
	 * 有参的构造函数
	 * 
	 * @param key   条件字段
	 * @param value 条件的值
	 */
	public Condition(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * 有参的构造函数
	 * 
	 * @param key      条件字段
	 * @param operator 操作符 缺省=
	 * @param value    条件的值
	 */
	public Condition(String key, String operator, Object value) {
		this.key = key;
		this.operator = operator;
		this.value = value;
	}

	/**
	 * 获取条件字段.
	 * 
	 * @return 条件字段
	 */
	public String getKey() {
		return key;
	}

	/**
	 * 设置条件字段.
	 * 
	 * @param key 条件字段
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * 获取操作符 缺省=.
	 * 
	 * @return 操作符 缺省=
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * 设置操作符 缺省=
	 * 
	 * @param operator 操作符 缺省=
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * 获取条件的值.
	 * 
	 * @return 条件的值
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * 设置条件的值.
	 * 
	 * @param value 条件的值
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return key + operator + value;
	}
}
