package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-18
 */
@TableName("rbt_warn_record")
//@ApiModel(value = "RbtWarnRecord对象", description = "")
@Data
public class WarnRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private String id;

 //   @ApiModelProperty("告警id")
    private String recordId;

 //   @ApiModelProperty("告警点数")
    private String recordPlace;

 //   @ApiModelProperty("备注")
    private String comment;

//    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;
}
