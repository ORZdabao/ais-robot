package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-11
 */
@Data
//@TableName("robot_info")
//@ApiModel(value = "Robot", description = "机器人")
@TableName("robot_track_machine")
public class Robot implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    //    @ApiModelProperty("轨道机名称")
    private String name;

    //    @ApiModelProperty("轨道机型号")
    private String robotModel;

    //    @ApiModelProperty("轨道机ip")
    private String ip;

    //    @ApiModelProperty("轨道机端口")
    private String port;

    //   @ApiModelProperty("轨道机工作模式")
    private Integer workModel;

    private Integer position;

    //  @ApiModelProperty("备注")
    private String comment;

}
