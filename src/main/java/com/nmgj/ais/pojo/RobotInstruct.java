package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName RobotInstruct
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/11/14 23:30
 * @return
 **/
@Data
@TableName("robot_track_instruct")
public class RobotInstruct {


    @TableId
    private Integer id ;

    @TableField("region_id")
    Integer regionId;

    @TableField("track_id")
    Integer trackId;

    @TableField("start_instruct")
    String startInstruct;

    @TableField("horizontal_instruct")
    String horizontalInstruct;

    @TableField("vertical_instruct")
    String verticalInstruct;

    @TableField("text_number")
    String textNumber;

    @TableField("number")
    String number;

    @TableField("position")
    Integer position;

    Integer status;
}
