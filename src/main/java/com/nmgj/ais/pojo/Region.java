package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-11
 */
@Data
@TableName("rbt_region")
//@ApiModel(value = "Region", description = "区域表")
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

//    @ApiModelProperty("区域名称")
    private String name;

 //   @ApiModelProperty("面积")
    private Double area;

  //  @ApiModelProperty("机器人id")
    private String robotId;

 //   @ApiModelProperty("备注")
    private String comment;


}
