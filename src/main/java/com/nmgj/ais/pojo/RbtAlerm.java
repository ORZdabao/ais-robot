package com.nmgj.ais.pojo;

import lombok.Data;

/**
 * @ClassName RbtAlerm
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/22 10:12
 * @return
 **/
@Data
public class RbtAlerm {
    Integer month;
    Integer number;
}
