package com.nmgj.ais.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author jhyang
 * @since 2023-10-11
 */
@TableName("robot_camera")
//@ApiModel(value = "Camera对象", description = "摄像机")
@Data
public class RobotCamera implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * NVR编号
     */
    @TableField("nvrId")
    private Long nvrId;

    /**
     * 视频通道
     */
    private Integer channel;

    /**
     * 大屏显示的位置 0不显示
     */
    @TableField("showIndex")
    private Integer showIndex;

    /**
     * 摄像头IP
     */
    @TableField("devIp")
    private String devIp;


    @TableField("devPort")
    private String devPort;

    @TableField("userName")
    private String userName;

    @TableField("password")
    private String password;


}
