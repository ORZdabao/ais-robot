package com.nmgj.ais.utils;

public enum LogNameEnum {
    BUSINESS_1("business_1"),
    BUSINESS_2("business_2");

    private String logName;

    LogNameEnum(String logName) {
        this.logName = logName;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }
}