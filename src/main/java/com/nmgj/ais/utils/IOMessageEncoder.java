package com.nmgj.ais.utils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class IOMessageEncoder extends MessageToByteEncoder<Object> {
	@Override
	protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out)  {
		try {
			byte[] buffer = null;
			if(msg.toString().equals("软件停止")){ //01030200017984
				buffer = new byte[7];
				buffer[0]= 1;
				buffer[1]= 3;
				buffer[2]= 2;
				System.arraycopy( CoderUtil.intToByteArray(96644), 0, buffer, 3, 4);
			}else if(msg.toString().equals("软件启动")){//0103 020000B844
				buffer = new byte[7];
				buffer[0]= 1;
				buffer[1]= 3;
				buffer[2]= 2;
				System.arraycopy( CoderUtil.intToByteArray(47172), 0, buffer, 3, 4);

			}else if(msg.toString().equals("电机正转")){//010600000001480A
				buffer = new byte[8];
				buffer[0]= 1;
				buffer[1]= 6;
				buffer[2]= 0;
				buffer[3]= 0;
				System.arraycopy( CoderUtil.intToByteArray(83978), 0, buffer, 4, 4);
			}else if(msg.toString().equals("电机反转")){//01060001000119CA
				buffer = new byte[8];
				buffer[0]= 1;
				buffer[1]= 6;
				buffer[2]= 0;
				buffer[3]= 1;
				System.arraycopy( CoderUtil.intToByteArray(72138), 0, buffer, 4, 4);
			}else if(msg.toString().equals("软件预停止")){//010300000001840A
				buffer = new byte[8];
				buffer[0]= 1;
				buffer[1]= 3;
				buffer[2]= 0;
				buffer[3]= 0;
				System.arraycopy( CoderUtil.intToByteArray(99338), 0, buffer, 4, 4);
			}
			out.writeBytes(buffer);
			ctx.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}