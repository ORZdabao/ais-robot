package com.nmgj.ais.utils;

import cn.hutool.json.JSONUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NettyClientHandler extends ChannelInboundHandlerAdapter {
    protected static Logger logger = LoggerFactory.getLogger(NettyClientHandler.class);


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("MIT_Client【RemoteAddress: " + ctx.channel().remoteAddress().toString() + "】 is Connected.");
        ctx.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("MIT_Client【RemoteAddress: " + ctx.channel().remoteAddress().toString() + "】 is Disconnected.");
        //如果断开连接，酒吧定时任务取消
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object obj) throws Exception {
        if (obj instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) obj;
            if (event.state() == IdleState.READER_IDLE) {
                ctx.channel().close();
            }
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Channel channel = ctx.channel();
        logger.info("msg={}", JSONUtil.toJsonStr(msg));
//        if(channel.hasAttr(AttributeKey.newInstance("startFlag"))){
//            AttributeKey<Boolean> startFlag = AttributeKey.newInstance("startFlag");
//            if(channel.attr(startFlag).get()){
//                // 发送指令回调
//                AttributeKey<String> robot = AttributeKey.newInstance("robot");
//                RobotCoalAlarmDTO robotCoalAlarmDTO = new RobotCoalAlarmDTO();
//                PloughPlcDTO ploughPlcDTO = JSONUtil.toBean(channel.attr(robot).get(),PloughPlcDTO.class);
//
//                robotCoalAlarmDTO.setCameraId(ploughPlcDTO.getCameraId());
//                robotCoalAlarmDTO.setRobotId(ploughPlcDTO.getRobotId());
//                robotCoalAlarmDTO.setCameraIp(ploughPlcDTO.getCameraIp());
//                robotCoalAlarmDTO.setDistance(msg.toString());
//                ContextProvider.getBean(IPloughOperationService.class)
//                        .checkAlarm(robotCoalAlarmDTO);
//            }
//        }
        ChannelFuture cf = channel.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
