package com.nmgj.ais.utils;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class NettyStringHandler extends ChannelInboundHandlerAdapter {
    protected static Logger logger = LoggerFactory.getLogger(NettyStringHandler.class);


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("channel success");
        ctx.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        //如果断开连接，酒吧定时任务取消

    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object obj) throws Exception {
        if (obj instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) obj;
            if (event.state() == IdleState.READER_IDLE) {
//                ctx.channel().close();
            }
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Channel channel = ctx.channel();
        AttributeKey<String> flag = AttributeKey.valueOf("startFlag");
        if(channel.hasAttr(flag)){
            AttributeKey<String> robotId = AttributeKey.valueOf("robot");
            logger.info("msg={},flag={},id={}", msg,channel.attr(flag).get(),channel.attr(robotId).get());
            if("iACM".equals(channel.attr(flag).get())){
                // 读取配置文件 D=5.249M,73#
        /*        RobotCoalAlarmDTO robotCoalAlarmDTO = new RobotCoalAlarmDTO();
                PloughPlcDTO ploughPlcDTO = JSONUtil.toBean(channel.attr(robotId).get(),PloughPlcDTO.class);

                robotCoalAlarmDTO.setCameraId(ploughPlcDTO.getCameraId());
                robotCoalAlarmDTO.setRobotId(ploughPlcDTO.getRobotId());
                robotCoalAlarmDTO.setCameraIp(ploughPlcDTO.getCameraIp());
                robotCoalAlarmDTO.setDistance(getDistance(msg.toString()));
                robotCoalAlarmDTO.setPloughId(ploughPlcDTO.getPloughId());
                robotCoalAlarmDTO.setRobotPosition(ploughPlcDTO.getRobotPosition());
                ContextProvider.getBean(IPloughOperationService.class)
                        .checkAlarm(robotCoalAlarmDTO);*/
            }
        }
        channel.close();

//        if(channel.hasAttr(AttributeKey.newInstance("startFlag"))){
//            AttributeKey<Boolean> startFlag = AttributeKey.newInstance("startFlag");
//            if(channel.attr(startFlag).get()){
//                // 发送指令回调
//                AttributeKey<String> robot = AttributeKey.newInstance("robot");
//                RobotCoalAlarmDTO robotCoalAlarmDTO = new RobotCoalAlarmDTO();
//                PloughPlcDTO ploughPlcDTO = JSONUtil.toBean(channel.attr(robot).get(),PloughPlcDTO.class);
//
//                robotCoalAlarmDTO.setCameraId(ploughPlcDTO.getCameraId());
//                robotCoalAlarmDTO.setRobotId(ploughPlcDTO.getRobotId());
//                robotCoalAlarmDTO.setCameraIp(ploughPlcDTO.getCameraIp());
//                robotCoalAlarmDTO.setDistance(msg.toString());
//                ContextProvider.getBean(IPloughOperationService.class)
//                        .checkAlarm(robotCoalAlarmDTO);
//            }
//        }
//        ChannelFuture cf = channel.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("channel error={}",cause.getMessage());
        ctx.close();
    }

    private String getDistance(String distance){
        String result = "0";
        String regex = "D=(.*)m.*";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(distance);
        boolean flag = matcher.find();
        if(flag){
            result = matcher.group(1);
        }
        return  result;
    }

    public static void main(String[] args) {
        String a = "D=5.249M,73#";
        String regex = "D=(.*)M.*";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(a);
        boolean flag = matcher.find();
        if(flag){
            System.out.println(matcher.group(1));
        }
    }
}
