package com.nmgj.ais.utils;

import io.netty.buffer.ByteBuf;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.*;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class CoderUtil {

	public static byte[] readBuffer(ByteBuf buffer) {
		byte[] bs = new byte[buffer.readableBytes()];
		buffer.readBytes(bs);
		return bs;
	}

	public static byte[] object2Bytes(Object obj) {
		byte[] bs = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			bs = bos.toByteArray();
		} catch (IOException e) {
		} finally {
			try {
				oos.close();
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bs;
	}

	public static Object bytes2Object(byte[] bs) {
		Object obj = null;
		ObjectInputStream ois = null;
		ByteArrayInputStream bis = new ByteArrayInputStream(bs);
		try {
			ois = new ObjectInputStream(bis);
			obj = ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				ois.close();
				bis.close();
			} catch (IOException e) {
			}
		}
		return obj;
	}

	public final static String md5Encoder(byte[] buffer) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(buffer);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] intToByteArray(int value) {
		byte[] byte_src = new byte[4];
		byte_src[0] = (byte) ((value & 0xFF000000) >> 24);
		byte_src[1] = (byte) ((value & 0x00FF0000) >> 16);
		byte_src[2] = (byte) ((value & 0x0000FF00) >> 8);
		byte_src[3] = (byte) ((value & 0x000000FF));
		return byte_src;
	}
	public static byte[] intToByte(int i, int len) {
		byte[] abyte = null;
		if (len == 1) {
			abyte = new byte[len];
			abyte[0] = (byte) (0xff & i);
		} else {
			abyte = new byte[len];
			abyte[0] = (byte) ((i >>> 24) & 0xff);
			abyte[1] = (byte) ((i >>> 16) & 0xff);
			abyte[2] = (byte) ((i >>> 8) & 0xff);
			abyte[3] = (byte) (i & 0xff);
		}
		return abyte;
	}


	public static void byteMerger(LinkedList<byte[]> byteLst) {
		byte[] byte_union = new byte[byteLst.get(0).length + byteLst.get(1).length];
		System.arraycopy(byteLst.get(0), 0, byte_union, 0, byteLst.get(0).length);
		System.arraycopy(byteLst.get(1), 0, byte_union, byteLst.get(0).length, byteLst.get(1).length);
		byteLst.removeFirst();
		byteLst.removeFirst();
		byteLst.addFirst(byte_union);
		if (byteLst.size() > 1) {
			byteMerger(byteLst);
		}
	}
}