package com.nmgj.ais.utils;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyUtils {
    protected static Logger logger = LoggerFactory.getLogger(NettyUtils.class);
    static EventLoopGroup eventLoopGroup;
    static Bootstrap bootstrap;
    static EventLoopGroup eventLoopGroup1;
    static Bootstrap bootstrap1;

public static synchronized SocketChannel getSocketClient(String host,int port){
    SocketChannel socketChannel;
    ChannelFuture future;

    if(eventLoopGroup==null){
        eventLoopGroup = new NioEventLoopGroup();
    }
    if(bootstrap ==null){
        bootstrap = new Bootstrap();
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.group(eventLoopGroup);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline();
                socketChannel.pipeline().addLast(new IOMessageEncoder());
                socketChannel.pipeline().addLast(new NettyClientHandler());
            }
        });
    }
    try{
            bootstrap.remoteAddress(host, port);
            future = bootstrap.connect(host, port).sync();
            socketChannel = (SocketChannel) future.channel();
            if (future.isSuccess()) {
                logger.info(socketChannel.remoteAddress().toString() + "连接成功");
                return socketChannel;
            }
        } catch (Exception e) {
            try {
                logger.info(host + ":" + port + "连接失败");
                logger.error("连接失败",e);
                Thread.sleep(3000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
                return null;
            }
            return null;
        }
    return null;
}

    public static synchronized SocketChannel getStringSocketClient(String host,int port){
        SocketChannel socketChannel;
        ChannelFuture future;

        if(eventLoopGroup1==null){
            eventLoopGroup1 = new NioEventLoopGroup();
        }
        if(bootstrap1 ==null) {
            bootstrap1 = new Bootstrap();
            bootstrap1.channel(NioSocketChannel.class);
            bootstrap1.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap1.group(eventLoopGroup1);
            bootstrap1.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline();
                    socketChannel.pipeline().addLast(new StringDecoder());
                    socketChannel.pipeline().addLast(new StringEncoder());
                    socketChannel.pipeline().addLast(new NettyStringHandler());
                }
            });
        }

        try{
            bootstrap1.remoteAddress(host, port);
            future = bootstrap1.connect(host, port).sync();
            socketChannel = (SocketChannel) future.channel();
            if (future.isSuccess()) {
                logger.info(socketChannel.remoteAddress().toString() + "连接成功");
                return socketChannel;
            }
        } catch (Exception e) {
            try {
                logger.info(host + ":" + port + "连接失败");
                logger.error("连接失败",e);
                Thread.sleep(3000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
                return null;
            }
            return null;
        }
        return null;
    }
}
