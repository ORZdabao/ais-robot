package com.nmgj.ais.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.collect.Maps;
import com.nmgj.ais.domain.ResponseResult;

import java.util.Map;

/**
 * 响应对象工具类
 * @Author: admin
 * @Date: 2021/8/30 16:04
 * @Version: 1.0
 */
public class ResponseUtil {
    /**
     * 工具类构造方法私有
     */
    private ResponseUtil() {}

    /**
     * 成功响应，200
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseResult success(T t) {
        ResponseResult<T> responseResult = new ResponseResult();
        responseResult.setCode(0);
        responseResult.setData(t);
        responseResult.setMsg("请求成功");
        return responseResult;
    }

    /**
     * 成功响应，200
     * @return
     */
    public static ResponseResult success() {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setCode(0);
        responseResult.setMsg("请求成功");
        return responseResult;
    }

    /**
     * 成功响应，200
     * @param msg
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseResult success(String msg, T t) {
        ResponseResult<T> responseResult = new ResponseResult();
        responseResult.setCode(0);
        responseResult.setData(t);
        responseResult.setMsg(msg);
        return responseResult;
    }

    /**
     * 失败响应，500
     * @param msg
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseResult error(String msg, T t) {
        ResponseResult<T> responseResult = new ResponseResult();
        responseResult.setCode(1);
        responseResult.setData(t);
        responseResult.setMsg(msg);
        return responseResult;
    }

    /**
     * 失败响应，500
     * @param msg
     * @return
     */
    public static ResponseResult error(String msg) {
        return error(msg, null);
    }

    /**
     * 失败响应，500
     * @param code
     * @param msg
     * @return
     */
    public static ResponseResult error(int code, String msg) {
        return set(code, msg, null);
    }

    /**
     * 设置返回对象
     *
     * @param code
     * @param msg
     * @param t
     * @param <T>
     * @return
     */
    public static <T> ResponseResult set(int code, String msg, T t) {
        ResponseResult<T> responseResult = new ResponseResult();
        responseResult.setCode(code);
        responseResult.setData(t);
        responseResult.setMsg(msg);
        return responseResult;
    }

    public static <T> Map<String,Object> successPage(IPage<T> t) {
        Map<String,Object> responseResult = Maps.newHashMap();
        responseResult.put("code","0");
        responseResult.put("rows",t.getRecords());
        responseResult.put("msg","查询成功");
        responseResult.put("total",t.getTotal());
        return responseResult;
    }

    public static Map<String,Object> toMap(ResponseResult responseResult){
        Map<String,Object> map = Maps.newHashMap();
        map.put("success",responseResult.getCode() == 0? true:false);
        map.put("error",responseResult.getMsg());
        map.put("data",responseResult.getData());

        return  map;

    }

}
