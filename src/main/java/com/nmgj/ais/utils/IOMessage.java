package com.nmgj.ais.utils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.LinkedList;
import java.util.UUID;

public class IOMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private String messageId;

	private String command;

	private Object body;

	public IOMessage() {
		this.messageId = UUID.randomUUID().toString().replace("-", "");
	}

	public IOMessage(String messageId, String command, Object body) {
		this.messageId = messageId;
		this.command = command;
		this.body = body;
	}

	public IOMessage(String messageId, String command) {
		this.messageId = messageId;
		this.command = command;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

	
	public String getMD5() throws UnsupportedEncodingException {
		String md5 = null;
		if (null != body) {
			byte[] buffer_Body = null;
			if (body instanceof String) {
				buffer_Body = ((String) body).getBytes("UTF-8");
			} else if (body instanceof byte[]) {
				buffer_Body = (byte[]) body;
			}
			byte[] buffer_TotalLength = CoderUtil.intToByte(72 + buffer_Body.length, 4);
			byte[] buffer_MessageId = messageId.getBytes("UTF-8");
			int int_command = Integer.parseInt(command);
			byte[] buffer_Command = CoderUtil.intToByte(int_command, 4);
			LinkedList<byte[]> result = new LinkedList<byte[]>();
			result.add(buffer_TotalLength);
			result.add(buffer_MessageId);
			result.add(buffer_Command);
			result.add(buffer_Body);
			CoderUtil.byteMerger(result);
			md5 = md5Encoder(result.get(0));
		}
		return md5;
	}


	public final static String md5Encoder(byte[] buffer) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(buffer);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}