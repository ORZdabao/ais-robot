package com.nmgj.ais.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nmgj.ais.pojo.Robot;

public interface IRobotDao extends BaseMapper<Robot> {
}
