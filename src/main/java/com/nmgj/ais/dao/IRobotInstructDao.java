package com.nmgj.ais.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotInstruct;

public interface IRobotInstructDao extends BaseMapper<RobotInstruct> {
}
