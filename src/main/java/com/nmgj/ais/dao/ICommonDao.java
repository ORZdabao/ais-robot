package com.nmgj.ais.dao;

import com.nmgj.ais.pojo.Condition;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 公共dao.
 */
public interface ICommonDao {

	/**
	 * 查询所有记录数
	 *
	 * @param tbl
	 *            表名
	 * @return 所有记录数
	 */
	public int count(@Param("tbl") String tbl);

	/**
	 * 通过单个查询条件值查询记录数
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            单个查询条件
	 * @return 记录数
	 */
	public int countByKey(String tbl, Condition condition);

	/**
	 * 通过多个字段值查询记录数
	 *
	 * @param tbl
	 *            表名
	 * @param params
	 *            多个字段
	 * @return 记录数
	 */
	public int countByKeys(String tbl, List<Condition> conditions);

	/**
	 * 分组查询单字段个数
	 *
	 * @param tbl
	 *            表名
	 * @param column
	 *            分组字段
	 * @return 分组字段条目数
	 */
	public List<Map<String, Object>> countGroup(String tbl, String column);

	/**
	 * 按单个条件删除记录
	 *
	 * @param tbl
	 *            表名
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @return 成功条目数
	 */
	public int deleteByKey(String tbl, String key, Object value);

	/**
	 * 按多个条件删除记录
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            多个条件
	 * @return 成功条目数
	 */
	public int deleteByKeys(String tbl, List<Condition> conditions);

	/**
	 * 按主键删除.
	 *
	 * @param tbl
	 *            表名
	 * @param id
	 *            主键
	 * @return 删除条目数
	 */
	public int deleteByPk(String tbl, long pk);

	/**
	 * 按主键批量删除.
	 *
	 * @param tbl
	 *            表名
	 * @param ids
	 *            编号集合
	 * @return 删除条目数
	 */
	public int deleteByPks(String tbl, List<Long> pks);

	/**
	 * 插入一条数据.
	 *
	 * @param tbl
	 *            表名
	 * @param obj
	 *            一条数据
	 * @return 成功条目数
	 */
	public int insertOne(String tbl, Map<String, Object> params);

	/**
	 * 插入一条需要返回主键值的数据.
	 *
	 * @param tbl
	 *            表名
	 * @param obj
	 *            一条数据
	 * @return 成功条目数
	 */
	public int insertPKOne(String tbl, Map<String, Object> params);

	/**
	 * 查询所有记录
	 *
	 * @param tbl
	 *            表名
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> select(@Param("tbl") String tbl, @Param("order") String order);

	/**
	 * 通过单个字段值查询多条记录
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            查询条件
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> selectByKey(String tbl, Condition condition, @Param("order") String order);

	/**
	 * 通过单个字段值查询一条记录
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            查询条件
	 * @return 一条记录
	 */
	public Map<String, Object> selectByKeyOne(String tbl, Condition condition);

	/**
	 * 通过单个字段值查询多条记录(分页)
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            查询条件
	 * @param start
	 *            起始下标
	 * @param limit
	 *            查询个数
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> selectByKeyPaging(String tbl, Condition condition, int start, int limit,
                                                       @Param("order") String order);

	/**
	 * 通过多个字段值查询一条记录(不分页)
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            多个条件
	 * @return 一条记录
	 */
	public Map<String, Object> selectByKeysOne(String tbl, List<Condition> conditions);

	/**
	 * 通过多个字段值查询多条记录(不分页)
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            多个字段
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> selectByKeys(String tbl, List<Condition> conditions, @Param("order") String order);

	/**
	 * 通过多个字段值查询多条记录(分页)
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            多个查询条件
	 * @param start
	 *            起始下标
	 * @param limit
	 *            查询个数
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> selectByKeysPaging(String tbl, List<Condition> conditions, int start, int limit,
                                                        @Param("order") String order);

	/**
	 * 查询所有记录(分页)
	 *
	 * @param tbl
	 *            表名
	 * @param start
	 *            起始下标
	 * @param limit
	 *            查询个数
	 * @param order
	 *            排序
	 * @return 多条记录
	 */
	public List<Map<String, Object>> selectPaging(String tbl, int start, int limit, @Param("order") String order);

	/**
	 * 通过主键查询
	 *
	 * @param tbl
	 *            表名
	 * @param id
	 *            主键
	 * @return 一条记录
	 */
	public Map<String, Object> selectByPK(String tbl, long id);

	/**
	 * 按单个条件更新记录
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            单个查询条件
	 * @return 成功条目数
	 */
	public int updateByKey(String tbl, Condition condition, Map<String, Object> obj);

	/**
	 * 按多个条件更新记录
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            多个查询条件
	 * @return 成功条目数
	 */
	public int updateByKeys(String tbl, List<Condition> condition, Map<String, Object> obj);

	/**
	 * 按主键更新一条记录
	 *
	 * @param tbl
	 *            表名
	 * @param pk
	 *            主键值
	 * @param obj
	 *            更新域(一条记录)
	 * @return 成功条目数
	 */
	public int updateByPk(String tbl, long pk, Map<String, Object> obj);

	/**
	 * 按一个条件更新一个字段
	 *
	 * @param tbl
	 *            表名
	 * @param condition
	 *            查询条件
	 * @param column
	 *            更新列
	 * @param value
	 *            更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByKey(String tbl, Condition condition, String column, Object value);

	/**
	 * 按多个条件更新一个字段
	 *
	 * @param tbl
	 *            表名
	 * @param conditions
	 *            查询条件
	 * @param column
	 *            更新列
	 * @param value
	 *            更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByKeys(String tbl, List<Condition> conditions, String column, Object value);

	/**
	 * 按主键更新一个字段
	 *
	 * @param tbl
	 *            表名
	 * @param pk
	 *            主键值
	 * @param column
	 *            更新列
	 * @param value
	 *            更新列的值
	 * @return 成功条目数
	 */
	public int updateOneColumnByPk(String tbl, long pk, String column, Object value);
}
