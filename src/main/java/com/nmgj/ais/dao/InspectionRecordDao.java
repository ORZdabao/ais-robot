package com.nmgj.ais.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nmgj.ais.pojo.InspectionRecord;
import com.nmgj.ais.vo.InspectionVo;
import org.apache.ibatis.annotations.Param;


public interface InspectionRecordDao extends BaseMapper<InspectionRecord> {
    Page<InspectionVo> queryList(Page page, @Param("param") InspectionVo vo);

}
