package com.nmgj.ais.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 数据处理.
 * 
 */
public class DataUtil {

	/**
	 * 私有构造函数.
	 */
	private DataUtil() {

	}

	public static String converTime(String time) {
		try {
			time = time.substring(0, time.lastIndexOf("_"));
			String[] arr = time.split("_");
			String y = arr[0];
			String m = arr[1];
			m = m.length() == 1 ? ("0" + m) : m;
			String d = arr[2];
			d = d.length() == 1 ? ("0" + d) : d;
			String h = arr[3];
			h = h.length() == 1 ? ("0" + h) : h;
			String min = arr[4];
			min = min.length() == 1 ? ("0" + min) : min;
			String s = arr[5];
			s = s.length() == 1 ? ("0" + s) : s;
			time = y + "-" + m + "-" + d + " " + h + ":" + min + ":" + s;
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (!time.equals(dateFormat.format(dateFormat.parse(time)))) {
				return null;
			}
			return time;
		} catch (Exception e) {
			return null;
		}
	}

	public static String converTime1(String time) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date d = dateFormat.parse(time);
			if (!time.equals(dateFormat.format(d))) {
				return null;
			}
			time = time.substring(0, 4) + "-" + time.substring(4);
			time = time.substring(0, 7) + "-" + time.substring(7);
			time = time.substring(0, 10) + " " + time.substring(10);
			time = time.substring(0, 13) + ":" + time.substring(13);
			time = time.substring(0, 16) + ":" + time.substring(16);
			return time;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 字节数组拼接成十六字符串.
	 * 
	 * @param bytes 字节数组
	 * @return 拼接后的字符串
	 */
	public static String byteArr2StringHex(byte[] bytes) {
		StringBuffer buf = new StringBuffer();
		for (byte b : bytes) {
			int d = b & 0xFF;
			String v = Integer.toHexString(d).toUpperCase();
			if (v.length() == 1) {
				v = "0" + v;
			}
			buf.append(v + "");
		}
		return buf.toString();
	}
}
