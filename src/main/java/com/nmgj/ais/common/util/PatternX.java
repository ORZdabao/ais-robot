package com.nmgj.ais.common.util;

import java.util.regex.Pattern;

/**
 * 正则式验证类.
 * 
 */
public class PatternX {

	/**
	 * 验证整数.
	 */
	public static final Pattern INT = Pattern.compile("\\d*");
	
	/**
	 * 验证IP
	 */
	public static final Pattern IP = Pattern.compile(
			"(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d{1,2}|1\\d\\d|2[0-4]\\d|25[0-5])");

}
