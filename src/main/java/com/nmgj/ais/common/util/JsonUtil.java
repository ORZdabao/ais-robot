package com.nmgj.ais.common.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ognl.Ognl;
import ognl.OgnlContext;


import java.util.Map;

/**
 * @ClassName JsonUtil
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/10/22 20:02
 * @return
 **/
public class JsonUtil {
    /**
     * @Author jhyang
     * @Description 将json转换成一个Map
     * @Date 17:01 2022/5/26
     * @Param [json]
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    public static Map<String,Object> transferToMap(String json){
        Gson gson = new Gson();
        Map<String,Object> map = gson.fromJson(json, new TypeToken<Map<String,Object>>(){}.getType());
        return map;
    }
    /**
     * @Author jhyang
     * @Description 通过OGNL 从Map中获取值
     * @Date 17:51 2022/5/26
     * @Param [json, path, clazz]
     * @return T
     **/
    public static <T>T getValue(String json ,String path, Class<T> clazz){
        try{
            Map map = transferToMap(json);
            OgnlContext context = new OgnlContext();
            context.setRoot(map);
            T value = (T) Ognl.getValue(path,context,context.getRoot());
            return value;
        } catch(Exception e){
            throw  new RuntimeException(e);
        }
    }
}
