package com.nmgj.ais.common.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * properties文件解析.
 * 
 */
public final class PropertyUtil {

	/**
	 * 实例化日志类.
	 */
	private static Logger logger = LoggerFactory.getLogger(PropertyUtil.class);

	/**
	 * 构造函数
	 * 
	 */
	private PropertyUtil() {

	}

	/**
	 * 存储文件数据.
	 */
	private static Map<String, String> map = new HashMap<String, String>();

	/**
	 * 获取配置参数.
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static String getValue(String key) {
		return map.get(key);
	}

	public static Map<String, String> getMap() {
		return map;
	}

	/**
	 * 读取Property文件.
	 * 
	 * @param in 输入流
	 */
	public static void load(InputStream in) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (StringUtils.isNotBlank(line)) {
					int index = line.indexOf('=');
					if (index >= 0) {
						String key = line.substring(0, index);
						String value = line.substring(index + 1);
						map.put(key, value);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * 读取Property文件.
	 * 
	 * @param in 输入流
	 */
	public static void load(InputStream in, Map<Integer, String> data) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (StringUtils.isNotBlank(line)) {
					int index = line.indexOf('=');
					if (index >= 0) {
						String key = line.substring(0, index);
						String value = line.substring(index + 1);
						data.put(Integer.valueOf(key), value);
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * 读取Property文件
	 * 
	 * @param path
	 * @param data
	 */
	public static void load(String path, Map<Integer, String> data) {
		// 输入字符流
		InputStreamReader ir = null;
		try {
			ir = new InputStreamReader(new FileInputStream(path), "UTF-8");
			Properties prop = new Properties();
			prop.load(ir);
			Enumeration<?> en = prop.propertyNames();
			while (en.hasMoreElements()) {
				String strKey = (String) en.nextElement();
				String strValue = (String) prop.getProperty(strKey);
				data.put(Integer.valueOf(strKey), strValue);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (ir != null) {
				try {
					ir.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * 设置参数.
	 * 
	 * @param key   键
	 * @param value 值
	 */
	public static void setValue(String key, String value) {
		map.put(key, value);
	}
}