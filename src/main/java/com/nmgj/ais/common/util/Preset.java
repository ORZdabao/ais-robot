package com.nmgj.ais.common.util;

import java.io.InputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Preset {

	/**
	 * 实例化日志类.
	 */
	protected static Logger logger = LoggerFactory.getLogger(Preset.class);

	Socket socket = null;
	InputStream is = null;

	public void close() {
		if (is != null) {
			try {
				is.close();
				is = null;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		if (socket != null) {
			try {
				socket.close();
				socket = null;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}


}
