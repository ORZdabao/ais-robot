package com.nmgj.ais.common.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class CommonTools {
	/**
	 * 将十六进制的字符串转换成字节数组
	 *
	 * @param hexString
	 * @return
	 */
	public static byte[] hexStrToBinaryStr(String hexString) {

		if (hexString == null || hexString.length() < 0 || hexString.equals("")) {
			return null;
		}

		hexString = hexString.replaceAll(" ", "");

		int len = hexString.length();
		int index = 0;

		byte[] bytes = new byte[len / 2];

		while (index < len) {

			String sub = hexString.substring(index, index + 2);

			bytes[index/2] = (byte)Integer.parseInt(sub,16);

			index += 2;
		}


		return bytes;
	}

	/**
	 * 将字节数组转换成十六进制的字符串
	 *
	 * @return
	 */
	public static String BinaryToHexString(byte[] bytes) {
	    String hexStr = "0123456789ABCDEF";
	    String result = "";
	    String hex = "";
	    for (byte b : bytes) {
	        hex = String.valueOf(hexStr.charAt((b & 0xF0) >> 4));
	        hex += String.valueOf(hexStr.charAt(b & 0x0F));
	        result += hex + " ";
	    }
	    return result;
	}

	/**
	 * 客户端发送数据到服务器
	 * @param out
	 * @param socket
	 * @param data
	 */
	public static void sendDataToServer(DataOutputStream out,Socket socket,String data){
		try {
    		System.out.println(socket);
    		//将客户端的信息传递给服务器
    		if(out == null && socket != null){
    			out = new DataOutputStream(socket
    					.getOutputStream());
    		}
    		if(out != null){
    			byte[] buffer = CommonTools.hexStrToBinaryStr(data);
                System.out.println("客户端发送数据:" + data);//输出键盘输出内容提示 ，也就是客户端向服务器端发送的消息
                out.write(buffer);
    		}
		} catch (IOException e) {
			e.printStackTrace();
			boolean conn = socket.isConnected();
		}
	}


	/**
	 * 客户端接受到的反馈
	 * @param socket
	 */
	public static String serverResponse(Socket socket){
		InputStream in = null;
		String res = "";
		try {
			in = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
        byte[] b = new byte[7];
        int len = 0;
		try {
			in.read(b,0,b.length);
			res = BinaryToHexString(b);
			//System.out.println("接收到服务器数据:" + res);
			return res;
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("没有接收到服务反馈数据！！");
		}

        //System.out.println("接收到服务器数据:" + res);
        return res;
	}

	/**
	 * 连接服务器
	 * @param serverIP
	 * @param port
	 */
	public static void connServer(String serverIP,int port){
		try {
			Socket socket = new Socket(serverIP, port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 断开服务器
	 * @param
	 * @param
	 */
	public static void closeServer(Socket socket){
		try {
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 通过反馈值查询预置位置
	 * @param res
	 * @return
	 */
	public static String queryPresetPosByRes(String res){
		Map<String,String> map = new HashMap<String,String>();
		map.put("FF 1A 00 07 00 01 09", "预置位1");
		map.put("FF 1B 00 07 00 01 09", "预置位1");
		map.put("FF 1A 00 07 00 02 0A", "预置位2");
		map.put("FF 1A 00 07 00 02 0A", "预置位2");
		map.put("FF 1A 00 07 00 03 0B", "预置位3");
		map.put("FF 1B 00 07 00 03 0B", "预置位3");
		map.put("FF 1A 00 07 00 04 0C", "预置位4");
		map.put("FF 1B 00 07 00 04 0C", "预置位4");
		map.put("FF 1A 00 07 00 05 0D", "预置位5");
		map.put("FF 1B 00 07 00 05 0D", "预置位5");
		String val = map.get(res);
		return val;
	}

	public static double covert(String content){
        double number=0;
        String [] HighLetter = {"A","B","C","D","E","F"};
        Map<String,Integer> map = new HashMap<String,Integer>();
        for(int i = 0;i <= 9;i++){
            map.put(i+"",i);
        }
        for(int j= 10;j<HighLetter.length+10;j++){
            map.put(HighLetter[j-10],j);
        }
        String[]str = new String[content.length()];
        for(int i = 0; i < str.length; i++){
            str[i] = content.substring(i,i+1);
        }
        for(int i = 0; i < str.length; i++){
            number += map.get(str[i])*Math.pow(16,str.length-1-i);
        }
        return number;
    }

	/**
	 * 通过位置反馈的值计算位置值
	 * @param res
	 * @return
	 */
	public static String getPosValueByRes(String res){
		String[] str = res.split(" ");
		String num1 = str[4];
		String num2 = str[5];
		String val = num1 + num2;

		double num3 = covert(val);
		double result = num3 / 400 * 4.00 * 3.1415926;
		DecimalFormat df = new DecimalFormat("#.00");
		String result2 = df.format(result);
		return result2;
	}

}
