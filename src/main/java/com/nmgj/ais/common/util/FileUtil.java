package com.nmgj.ais.common.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {

	/**
	 * 实例化日志对象.
	 */
	private final static Logger logger = LoggerFactory.getLogger(FileUtil.class);
	
	private FileUtil() {
		
	}

	public static Object readObj(String path) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(path));
			return ois.readObject();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public static synchronized void writeObj(String path, Object obj) {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(path));
			oos.writeObject(obj);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				if (oos != null) {
					oos.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
}
