package com.nmgj.ais.common.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.web.socket.WebSocketSession;

import com.nmgj.ais.common.util.Preset;

/**
 * 全局数据类.
 *
 */
public class GlobalData {

	/**
	 * 私有的构造函数.
	 */
	private GlobalData() {
	}

	/**
	 * 存储活动告警图片
	 */
	public static final Set<String> alarms = Collections.synchronizedSet(new HashSet<String>());


	/**
	 * 当前正在发包的预置位机器人
	 */
	public static Map<String, Preset> presetMap = Collections.synchronizedMap(new HashMap<>());

	/**
	 * 机器人当前预置位
	 */
	public static Map<String, String> currentRobotPresetMap = Collections.synchronizedMap(new HashMap<>());

	public static Map<String, Integer> devLineMap = Collections.synchronizedMap(new HashMap<>());

	public static DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public synchronized static void adddelPreset(String pre) {
		String[] keys = presetMap.keySet().toArray(new String[] {});
		for (String key : keys) {
			if (key.startsWith(pre + "-")) {
				presetMap.get(key).close();
				presetMap.remove(key);
			}
		}
	}

	/**
	 * 存储告警级别.
	 */
	public static final Map<Integer, String> severityMap = Collections.synchronizedMap(new HashMap<Integer, String>());

	/**
	 * 存储WebSocket连接上的客户端会话.
	 */
	public static final Map<String, WebSocketSession> webSocketSessionMap = Collections
			.synchronizedMap(new HashMap<String, WebSocketSession>());

}
