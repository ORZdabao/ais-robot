package com.nmgj.ais.common.constants;

/**
 * 错误码.
 * 
 */
public class ErrorCode {

	/**
	 * 私有构造函数.
	 */
	private ErrorCode() {

	}

	/**
	 * 通用错误码.
	 * 
	 */
	public class Common {

		/**
		 * 私有构造函数.
		 */
		private Common() {

		}

		/**
		 * 操作失败,失败原因：删除数据失败.
		 */
		public static final String DELETEDB = "操作失败,失败原因：删除数据失败";

		/**
		 * 操作失败,失败原因：查询数据库失败.
		 */
		public static final String FINDDB = "操作失败,失败原因：查询数据库失败";

		/**
		 * 操作失败,失败原因：插入数据库失败.
		 */
		public static final String INSERTDB = "操作失败,失败原因：插入数据库失败";

		/**
		 * 操作失败,失败原因：旧密码不正确.
		 */
		public static final String OLDPASS = "操作失败,失败原因：旧密码不正确";

		/**
		 * 操作失败,失败原因：参数为空.
		 */
		public static final String PARAMNULL = "操作失败,失败原因：参数为空";

		/**
		 * 操作失败,失败原因：告警恢复失败.
		 */
		public static final String RESTOREALARM = "操作失败,失败原因：告警恢复失败";

		/**
		 * 操作成功.
		 */
		public static final String SUCCESS = "操作成功";

		/**
		 * 操作失败,失败原因：更新数据库失败.
		 */
		public static final String UPDATEDB = "操作失败,失败原因：更新数据库失败";
	}

	/**
	 * 用户相关.
	 * 
	 */
	public class User {

		/**
		 * 私有构造函数.
		 */
		private User() {

		}

		/**
		 * 操作失败,失败原因：该用户已登录.
		 */
		public static final String LOGINED = "操作失败,失败原因：该用户已登录";

		/**
		 * 操作失败,失败原因：旧密码不正确.
		 */
		public static final String OLDPASSERROR = "操作失败,失败原因：旧密码不正确";

		/**
		 * 操作失败,失败原因：用户名或密码错误.
		 */
		public static final String USERNAMEORPASSWORDERROR = "操作失败,失败原因：用户名或密码错误";

		/**
		 * 操作失败,失败原因：用户不存在.
		 */
		public static final String USERNOTEXISTS = "操作失败,失败原因：用户不存在";
	}
}