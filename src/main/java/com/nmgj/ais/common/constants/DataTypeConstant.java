package com.nmgj.ais.common.constants;

/**
 * @Author: lijunbt
 * @Date: 2023/3/6 14:46
 * @Version: 1.0
 */
public class DataTypeConstant {

    /**
     * 电流
     */
    public static final Integer ELECTRIC_CURRENT = 0;

    /**
     * 累计煤流量
     */
    public static final Integer ACCUMULATIVE_TOTAL_COAL = 11;

    /**
     * 碎煤机温度
     */
    public static final Integer COAL_CRASH_TEMP = 12;

    /**
     * 碎煤机振动
     */
    public static final Integer COAL_CRASH_VIBRATE = 13;

    /**
     * 电机温度
     */
    public static final Integer POWER_TEMP = 22;

    /**
     * 电机振动
     */
    public static final Integer POWER_VIBRATE = 24;

    /**
     * 减速机温度
     */
    public static final Integer REDUCER_TEMP = 23;

    /**
     * 减速机振动
     */
    public static final Integer REDUCER_VIBRATE = 25;


    /**
     * 煤仓料位
     */
    public static final Integer COAL_BUNKER = 50;


    /**
     * 热成像温度
     */
    public static final Integer THERMAL_IMAGING_TEMP = 51;

    /**
     * 2号电流
     */
    public static final Integer ELECTRIC_CURRENT_SECOND = 30;

    /**
     * 3号电流
     */
    public static final Integer ELECTRIC_CURRENT_THIRD = 31;
}
