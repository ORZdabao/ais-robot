package com.nmgj.ais.common.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 表名定义.
 * 
 */
public class Table {

	/**
	 * 创建对象.
	 */
	private static Table table;

	/**
	 * 存放表对应名称.
	 */
	private static final Map<String, String> tableDuplicateMap = new HashMap<String, String>();

	/**
	 * 存放表对应中文名称.
	 */
	private static final Map<String, String> tableZHMap = new HashMap<String, String>();

	/**
	 * 表英文名
	 * 
	 */
	public final class EN {

		/**
		 * 私有构造函数.
		 */
		private EN() {
		}

		/**
		 * 活动告警
		 */
		public static final String ACTIVEALARM = "tbl_activealarm";

		/**
		 * 告警音
		 */
		public static final String ALARMAUDIO = "tbl_alarmaudio";

		/**
		 * 自动上煤
		 */
		public static final String AUTOONCOAL = "tbl_autooncoal";

		/**
		 * 皮带
		 */
		public static final String BELT = "tbl_belt";

		/**
		 * 皮带摄像头
		 */
		public static final String BELTCAMERA = "tbl_beltcamera";

		/**
		 * 皮带告警输出
		 */
		public static final String BELTPI = "tbl_beltpi";

		/**
		 * 皮带犁煤器
		 */
		public static final String BELTPLOUGH = "tbl_beltplough";

		/**
		 * 皮带犁煤器落犁
		 */
		public static final String BELTPLOUGHDOWN = "tbl_beltploughdown";

		/**
		 * 皮带犁煤器上位检修
		 */
		public static final String BELTPLOUGHM = "tbl_beltploughm";

		/**
		 * 皮带犁煤器抬犁
		 */
		public static final String BELTPLOUGHUP = "tbl_beltploughup";

		/**
		 * 皮带启动
		 */
		public static final String BELTSTART = "tbl_beltstart";

		/**
		 * 皮带停止
		 */
		public static final String BELTSTOP = "tbl_beltstop";

		/**
		 * 煤仓
		 */
		public static final String BUNKER = "tbl_bunker";

		/**
		 * 煤仓料位
		 */
		public static final String BUNKERML = "tbl_bunkerml";

		/**
		 * 煤仓料位犁煤器
		 */
		public static final String BUNKERMLBELTPLOUGH = "tbl_bunkermlbeltplough";

		/**
		 * 摄像头
		 */
		public static final String CAMERA = "tbl_camera";

		/**
		 * 摄像头和机器人在线统计
		 */
		public static final String DEVLINE = "tbl_devline";

		/**
		 * 历史告警.
		 */
		public static final String HISTORYALARM = "tbl_historyalarm";

		/**
		 * 硬盘录像机.
		 */
		public static final String NVR = "tbl_nvr";

		/**
		 * 操作日志
		 */
		public static final String OPERATELOG = "tbl_operatelog";

		/**
		 * 其他设备
		 */
		public static final String OTHERDEV = "tbl_otherdev";

		/**
		 * PLC.
		 */
		public static final String PLC = "tbl_plc";

		/**
		 * 读取参数
		 */
		public static final String READPARAM = "tbl_readparam";

		/**
		 * 机器人.
		 */
		public static final String ROBOT = "tbl_robot";

		/**
		 * 机器人预置位.
		 */
		public static final String ROBOTPRESET = "tbl_robotpreset";

		/**
		 * 告警阈值
		 */
		public static final String THRESHOLD = "tbl_threshold";

		/**
		 * 用户表.
		 */
		public static final String USER = "tbl_user";

		/**
		 * 叶轮给煤机表
		 */
		public static final String COALFEEDER = "tbl_coalfeeder";

		/**
		 * 空气炮表
		 */
		public static final String AIRCANNON = "tbl_aircannon";

		/**
		 * 接口机表
		 */
		public static final String IMACHINE = "tbl_interfacemachine";
	}

	/**
	 * 私有构造函数.
	 */
	private Table() {
		tableZHMap.put(EN.ACTIVEALARM, "活动告警");
		tableZHMap.put(EN.ALARMAUDIO, "告警音");
		tableDuplicateMap.put(EN.ALARMAUDIO, "该设备告警类型对应的告警音已配置过");
		tableZHMap.put(EN.AUTOONCOAL, "自动上煤");
		tableZHMap.put(EN.BELT, "皮带");
		tableDuplicateMap.put(EN.BELT, "皮带名称已存在");
		tableZHMap.put(EN.BELTCAMERA, "皮带摄像头");
		tableZHMap.put(EN.BELTPI, "皮带告警输出");
		tableDuplicateMap.put(EN.BELTPI, "该干节点已被使用");
		tableZHMap.put(EN.BELTPLOUGH, "皮带犁煤器");
		tableDuplicateMap.put(EN.BELTPLOUGH, "该犁煤器名称已被使用");
		tableZHMap.put(EN.BELTPLOUGHDOWN, "皮带犁煤器落犁");
		tableDuplicateMap.put(EN.BELTPLOUGHDOWN, "该干节点已被使用");
		tableZHMap.put(EN.BELTPLOUGHM, "皮带犁煤器上位检修");
		tableDuplicateMap.put(EN.BELTPLOUGHM, "该读Modbus地址已被使用");
		tableZHMap.put(EN.BELTPLOUGHUP, "皮带犁煤器抬犁");
		tableDuplicateMap.put(EN.BELTPLOUGHUP, "该皮带犁煤器抬犁干节点已被使用");
		tableZHMap.put(EN.BELTSTART, "皮带启动");
		tableDuplicateMap.put(EN.BELTSTART, "该皮带启停干节点已被使用");
		tableZHMap.put(EN.BELTSTOP, "皮带停止");
		tableDuplicateMap.put(EN.BELTSTOP, "该皮带停止干节点已被使用");
		tableZHMap.put(EN.BUNKER, "煤仓");
		tableDuplicateMap.put(EN.BUNKER, "该名称已被使用");
		tableZHMap.put(EN.BUNKERML, "煤仓料位");
		tableDuplicateMap.put(EN.BUNKERML, "该名称已被使用");
		tableZHMap.put(EN.BUNKERML, "煤仓料位");
		tableZHMap.put(EN.BUNKERMLBELTPLOUGH, "煤仓料位犁煤器");
		tableZHMap.put(EN.CAMERA, "摄像头");
		tableZHMap.put(EN.DEVLINE, "摄像头和机器人在线统计");
		tableDuplicateMap.put(EN.CAMERA, "该NVR下的视频通道已被占用");
		tableZHMap.put(EN.HISTORYALARM, "历史告警");
		tableZHMap.put(EN.NVR, "硬盘录像机");
		tableDuplicateMap.put(EN.NVR, "硬盘录像机名称已存在");
		tableZHMap.put(EN.OPERATELOG, "操作日志");
		tableZHMap.put(EN.OTHERDEV, "其他设备");
		tableDuplicateMap.put(EN.OTHERDEV, "设备已存在");
		tableZHMap.put(EN.PLC, "PLC");
		tableDuplicateMap.put(EN.PLC, "该IP已存在");
		tableZHMap.put(EN.READPARAM, "读取参数");
		tableDuplicateMap.put(EN.READPARAM, "该读Modbus地址已被使用");
		tableZHMap.put(EN.ROBOT, "机器人");
		tableDuplicateMap.put(EN.ROBOT, "机器人编号或机器人IP已存在");
		tableZHMap.put(EN.ROBOTPRESET, "机器人预置位");
		tableDuplicateMap.put(EN.ROBOTPRESET, "该机器人对应的机器人预置位已存在");
		tableZHMap.put(EN.THRESHOLD, "告警阈值");
		tableDuplicateMap.put(EN.THRESHOLD, "已存在该设备的阈值类型的阈值");
		tableZHMap.put(EN.USER, "用户");
		tableDuplicateMap.put(EN.USER, "操作失败,失败原因：该用户名已存在");
		tableZHMap.put(EN.COALFEEDER, "叶轮给煤机");
		tableDuplicateMap.put(EN.COALFEEDER, "操作失败,失败原因：该叶轮给煤机配置已存在");
	}

	/**
	 * 获取表唯一约束.
	 * 
	 * @param key
	 * @return 唯一约束
	 */
	public static String getTableDuplicate(String key) {
		if (table == null) {
			table = new Table();
		}
		return tableDuplicateMap.get(key);
	}

	/**
	 * 获取表名.
	 * 
	 * @param key
	 * @return 表名
	 */
	public static String getTableZH(String key) {
		if (table == null) {
			table = new Table();
		}
		return tableZHMap.get(key);
	}
}
