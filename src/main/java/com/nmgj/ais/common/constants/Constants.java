package com.nmgj.ais.common.constants;

/**
 * 公共常量类.
 *
 */
public class Constants {

	/**
	 * 私有构造函数.
	 */
	private Constants() {

	}

	/**
	 * 告警恢复类型常量.
	 *
	 */
	public class AlarmRestoreType {
		/**
		 * 私有构造函数.
		 */
		private AlarmRestoreType() {

		}

		/**
		 * 自动恢复.
		 */
		public static final int TYPE_1 = 1;

		/**
		 * 手动恢复.
		 */
		public static final int TYPE_2 = 2;
	}

	public class AlarmType {

		/**
		 * 私有构造函数.
		 */
		private AlarmType() {

		}

		/**
		 * 电度
		 */
		public static final int TYPE_1 = 1;

		/**
		 * 电流
		 */
		public static final int TYPE_2 = 2;

		/**
		 * 甲乙粗碎温度1
		 */
		public static final int TYPE_3 = 3;

		/**
		 * 瞬时电流
		 */
		public static final int TYPE_4 = 4;

		/**
		 * 撕裂左
		 */
		public static final int TYPE_5 = 5;

		/**
		 * 撕裂右
		 */
		public static final int TYPE_6 = 6;

		/**
		 * 堵煤
		 */
		public static final int TYPE_7 = 7;

		/**
		 * 异物
		 */
		public static final int TYPE_8 = 8;

		/**
		 * 跑偏
		 */
		public static final int TYPE_9 = 9;

		/**
		 * 电机非驱动端温度
		 */
		public static final int TYPE_11 = 11;

		/**
		 * 碎煤机温度
		 */
		public static final int TYPE_12 = 12;

		/**
		 * 碎煤机振动
		 */
		public static final int TYPE_13 = 13;

		/**
		 * 非驱动端温度
		 */
		public static final int TYPE_14 = 14;

		/**
		 * 驱动端温度
		 */
		public static final int TYPE_15 = 15;
	}

	/**
	 * 备份目录.
	 *
	 */
	public class BackUp {
		/**
		 * 私有构造函数.
		 */
		private BackUp() {

		}

		/**
		 * 告警图片目录.
		 */
		public static final String ALARMIMG = "/backup/block/";
	}

	/**
	 * 设备类型
	 *
	 */
	public class DevType {
		/**
		 * 私有构造函数.
		 */
		private DevType() {

		}

		/**
		 * 皮带
		 */
		public static final int BELT = 0;

		/**
		 * 其他设备
		 */
		public static final int OTHER = 1;
	}

	/**
	 * 操作日志类型.
	 *
	 * @author dgh
	 * @date 2019/11/14
	 */
	public class OperateLogType {
		/**
		 * 私有构造函数.
		 */
		private OperateLogType() {

		}

		/**
		 * 登录.
		 */
		public static final int LOGIN = 1;

		/**
		 * 退出.
		 */
		public static final int LOGOUT = 2;

		/**
		 * 增加.
		 */
		public static final int ADD = 3;
		/**
		 * 删除.
		 */
		public static final int DELETE = 4;
		/**
		 * 修改.
		 */
		public static final int UPDATE = 5;
		/**
		 * 查询.
		 */
		public static final int QUERY = 6;
		/**
		 * 恢复
		 */
		public static final int RESTORE = 7;
	}

	/**
	 * 属性文件配置内容.
	 */
	public class Property {

		/**
		 * 开关量设备超时时间(单位:秒).
		 */
		public static final String IO_DEVIPPKGTIMEOUT = "IO_DEVIPPKGTIMEOUT";

		/**
		 * 开关量设备重连次数.
		 */
		public static final String IO_DEVIPPKGRETRYTIMES = "IO_DEVIPPKGRETRYTIMES";
	}

	/**
	 * 存储在session里的key值.
	 *
	 */
	public class SessionKey {

		/**
		 * 私有构造函数.
		 */
		private SessionKey() {

		}

		/**
		 * 角色.
		 */
		public static final String ROLE = "role";

		/**
		 * 用户名.
		 */
		public static final String USERNAME = "userName";
	}

	/**
	 * WebSocket消息常量
	 *
	 */
	public class WebSocket {

		/**
		 * 私有构造函数.
		 */
		private WebSocket() {

		}

		/**
		 * 定时发送服务器时间.
		 */
		public static final int SERVERTIME = 1;

		/**
		 * 告警
		 */
		public static final int ALARM = 2;

		/**
		 * 告警恢复
		 */
		public static final int ALARM_RESTORE = 5;

		/**
		 * 电度电流
		 */
		public static final int PARAM = 3;

		/**
		 * 料位都高于10米
		 */
		public static final int ML = 4;

		/**
		 * 自动巡检结果
		 */
		public static final int POLL = 6;

		/**
		 * 机器人预置位改变
		 */
		public static final int ROBOTPRESET = 7;

		/**
		 * 煤仓料位
		 */
		public static final int BUNKERML = 8;

		/**
		 * 梨煤器煤仓料位
		 */
		public static final int COALBUNKER = 9;

		/**
		 * 机器人
		 */
		public static final int ROBOTINSPECT = 10;
	}

	public class Robot{
	    private Robot(){

        }
        // 动力室地址
        public static final String ROBOT_POWER_HOST= "192.168.166.21";
		public static final String CAMERA_ROBOT_POWER_HOST= "192.168.166.31";
		// 设备key
		public static final String DEVICE_POWER_KEY = "10067394_1";
	    // 电容室地址
        public static final String ROBOT_CAPACITOR_HOST= "192.168.166.22";
		public static final String CAMERA_ROBOT_CAPACITOR_HOST= "192.168.166.32";

		public static final String DEVICE_CAPACITOR_KEY = "10068388_1";
        // 端口
        public static final String PORT = "9001";
		// 控制室
		public static final String MANAGER_HOST= "192.168.166.13";

		public static final String ROBOT_PORT = "4196";

		public static final String ROBOT_POWER_NAME ="综合动力站室";

		public static final String ROBOT_CAPACITOR_NAME ="补偿电容室";

		// 摄像头截图前缀
		public static final String CAMERA_PIC_URL="D:\\apache-tomcat-8.5.32\\webapps\\ROOT\\images";

		public static final String POWER_REDIS_KEY="power_position";

		public static final String CAPACITY_REDIS_KEY="capacity_position";

	}
}
