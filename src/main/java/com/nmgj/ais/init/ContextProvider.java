package com.nmgj.ais.init;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Spring容器上下文
 *
 */
@Component
public class ContextProvider implements ApplicationContextAware {

	/**
	 * 上下文.
	 */
	private static ApplicationContext applicationContext;

	/**
	 * 获取bean实例
	 *
	 * @param name
	 *            bean名称
	 * @return bean实例
	 * @throws BeansException
	 *             异常
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) throws BeansException {
		return (T) applicationContext.getBean(name);
	}

	public static <T> T getBean(Class<T> name) throws BeansException {
		return (T) applicationContext.getBean(name);
	}

	/**
	 * 容器启动完自动设置上下文
	 *
	 * @param applicationContext
	 *            上下文
	 * @throws BeansException
	 *             异常
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextProvider.applicationContext = applicationContext;
	}
}
