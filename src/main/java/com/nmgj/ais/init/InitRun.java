package com.nmgj.ais.init;

import java.util.*;


import com.google.gson.Gson;
import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.constants.GlobalData;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.service.IRobotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * 初始加载数据
 */
public class InitRun {

    /**
     * 实例化日志类.
     */
    protected static Logger logger = LoggerFactory.getLogger(InitRun.class);

    @Resource
    private IRobotService robotService;


    /**
     * 初始化.
     */
    public void init() {

        try {
            // redis 查看name 没有就发送请求
            // 动力室
/*            Object objectPowerName = redisTemplate.opsForValue().get("robot_userId");
            if(ObjectUtil.isEmpty(objectPowerName)){
                logger.error("接口请求是："+"http://"+ Constants.Robot.MANAGER_HOST+":"+Constants.Robot.PORT+"/app/Login");
                String powerResult  = HttpUtil.post("http://"+ Constants.Robot.POWER_HOST+":"+Constants.Robot.PORT+"/app/Login",paramMap);
                logger.error("powerResult的值是："+powerResult);
                if(StrUtil.isNotBlank(powerResult) && "1000".equals(JsonUtil.getValue(powerResult,"code",String.class))){
                   String userId = JsonUtil.getValue(powerResult,"data.userId",String.class);
                    redisTemplate.opsForValue().set("robot_userId",userId);
                    logger.error("id是："+userId);
                }
            }*/

            List<Robot> list = robotService.list();
            for(Robot robot: list){
                if(1==(robot.getId())){
                    GlobalData.currentRobotPresetMap.put(Constants.Robot.POWER_REDIS_KEY,String.valueOf(robot.getPosition()));
                } else {
                    GlobalData.currentRobotPresetMap.put(Constants.Robot.CAPACITY_REDIS_KEY,String.valueOf(robot.getPosition()));
                }

            }




       /*     if(StrUtil.isNotBlank(powerName)&& StrUtil.isNotBlank(capacityName)){
                Map<String,Object> queryMap = new HashMap<String,Object>();
                List<Object>  powerList = redisTemplate.opsForList().range("robot_power_alerm",0,-1);
                LocalDateTime time = LocalDateTime.now();
                // 获取当前年份
                int year = time.getYear();
                if(CollectionUtil.isEmpty(powerList)){
                    queryMap.put("userId",powerName);
                    queryMap.put("deviceKey",Constants.Robot.DEVICE_POWER_KEY);
                    queryMap.put("isAlermData",1);
                    queryMap.put("nodeId",-1);
                    queryMap.put("beginTime",year +"-"+"01"+"01");

                }
            }*/

            logger.info("启动成功");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }



}
