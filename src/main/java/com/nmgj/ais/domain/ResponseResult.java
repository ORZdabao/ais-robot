package com.nmgj.ais.domain;

/**
 * @Author: admin
 * @Date: 2021/8/30 16:02
 * @Version: 1.0
 */
public class ResponseResult<T> {
    private int code;

    private String msg;

    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
