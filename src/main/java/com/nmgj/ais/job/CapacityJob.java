package com.nmgj.ais.job;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nmgj.ais.dao.IRobotDao;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotInstruct;
import com.nmgj.ais.service.IRobotInstructService;
import com.nmgj.ais.service.IRobotService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

/**
 * @ClassName CapacityJob
 * @Deacription TODO
 * @Author jhyang
 * @Date 2023/11/25 16:34
 * @return
 **/
// 不允许并发
@DisallowConcurrentExecution
//执行后持久化
@PersistJobDataAfterExecution
@Slf4j
public class CapacityJob extends QuartzJobBean {
    @Autowired
    private IRobotDao robotDao;

    @Autowired
    private IRobotInstructService robotInstructService;

    @Autowired
    private IRobotService robotService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("开始电容室任务");
        // 先判断是否开启了自动巡检任务
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id",2);
        List<Robot> list = robotDao.selectList(queryWrapper);
        if(CollectionUtil.isNotEmpty(list)){
            Robot robot = list.get(0);
            int model = robot.getWorkModel();
            // 1为自动 0是手动
            if(1 == model){
                QueryWrapper robotWrapper = new QueryWrapper();
                robotWrapper.eq("region_id",2);
                List<RobotInstruct> instructList = robotInstructService.list(robotWrapper);
                for (RobotInstruct instruct:instructList){
                    robotService.startRobot(instruct);
                }
            }else {
                log.error("开启手动模式");
            }
        } else {
            log.error("没有配置电容室室信息");
        }

    }
}
