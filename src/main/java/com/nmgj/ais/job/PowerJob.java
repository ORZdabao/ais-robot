package com.nmgj.ais.job;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nmgj.ais.dao.IRobotDao;
import com.nmgj.ais.pojo.Robot;
import com.nmgj.ais.pojo.RobotInstruct;
import com.nmgj.ais.service.IRobotInstructService;
import com.nmgj.ais.service.IRobotService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName PowerJob
 * @Deacription 动力室定时任务
 * @Author jhyang
 * @Date 2023/10/27 23:09
 * @return
 **/
// 不允许并发
@DisallowConcurrentExecution
//执行后持久化
@PersistJobDataAfterExecution
@Slf4j
public class PowerJob extends QuartzJobBean {

    @Autowired
    private IRobotDao robotDao;

    @Autowired
    private IRobotInstructService robotInstructService;

    @Autowired
    private IRobotService robotService;

    @Resource
    private DataSourceTransactionManager transactionManager;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
      log.info("开始动力室任务");
      // 先判断是否开启了自动巡检任务
      QueryWrapper queryWrapper = new QueryWrapper();
      queryWrapper.eq("id",1);
      List<Robot> list = robotDao.selectList(queryWrapper);
      if(CollectionUtil.isNotEmpty(list)){
        Robot robot = list.get(0);
        int model = robot.getWorkModel();
        // 1为自动 0是手动
        if(1 == model){
            DefaultTransactionDefinition transDefinition = new DefaultTransactionDefinition();
            transDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW); // 事物隔离级别，开启新事务，这样会比较安全些。
            TransactionStatus status = transactionManager.getTransaction(transDefinition); // 得到事务状态

            try {
                QueryWrapper robotWrapper = new QueryWrapper();
                robotWrapper.eq("region_id",1);
                List<RobotInstruct> instructList = robotInstructService.list(robotWrapper);
                for (RobotInstruct instruct:instructList){

                    robotService.startRobot(instruct);
                    transactionManager.commit(status);
                }
            } catch (Exception e) {
                transactionManager.rollback(status);
            }

        }else {
            log.error("开启手动模式");
        }
      } else {
          log.error("没有配置动力室信息");
      }



      // 和机器人通信 发送指令


      // 调用海康威视摄像头


    }
}
