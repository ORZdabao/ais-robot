package com.nmgj.ais.config.socket;

import com.nmgj.ais.common.constants.Constants;
import com.nmgj.ais.common.constants.GlobalData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;


@Slf4j
public class CustomWebSocketHandler  extends TextWebSocketHandler {





    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("用户{}连接成功.... ",session);
        GlobalData.webSocketSessionMap.put("name", session);

    }


    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String msg = message.getPayload().toString();
        session.sendMessage(new TextMessage(GlobalData.currentRobotPresetMap.get(Constants.Robot.POWER_REDIS_KEY)+","+GlobalData.currentRobotPresetMap.get(Constants.Robot.CAPACITY_REDIS_KEY)));
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        GlobalData.webSocketSessionMap.remove("name");
        log.info("{} 连接已经关闭，现从list中删除 ,状态信息{}", session, status);
    }
}
