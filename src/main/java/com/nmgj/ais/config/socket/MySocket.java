package com.nmgj.ais.config.socket;

import com.nmgj.ais.common.util.CommonTools;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class MySocket{

	private Socket socket;
	private DataOutputStream out;
	private InputStream is;
	private String ip;
	private int port;
	private boolean conn;

	public void setDataOutputStream(DataOutputStream out){
		this.out = out;
	}

	public DataOutputStream getDataOutputStream(){
		return out;
	}

	public void setSocket(Socket socket){
		this.socket = socket;
	}

	public Socket getSocket(){
		return socket;
	}

	public boolean isConn(){
		return conn;
	}

	public MySocket(String ip,int port){
		this.ip = ip;
		this.port = port;
		try {
			socket = new Socket(ip,port);
			is = socket.getInputStream();
			out = new DataOutputStream(socket.getOutputStream());
			conn = socket.isConnected();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setOnServerResponse(OnServerResponse onServerResponse){
		while(true){
			String res = serverResponse(socket);
			onServerResponse.onResponse(res);
		}
	}

	public void close(){
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端发送数据到服务器
	 * @param out
	 * @param socket
	 * @param data
	 */
	public void sendDataToServer(DataOutputStream out,Socket socket,String data){
		try {
    		System.out.println(socket);
    		//将客户端的信息传递给服务器
    		if(out == null && socket != null){
    			out = new DataOutputStream(socket
    					.getOutputStream());
    		}
    		if(out != null){
    			byte[] buffer = CommonTools.hexStrToBinaryStr(data);
                System.out.println("客户端发送数据:" + data);//输出键盘输出内容提示 ，也就是客户端向服务器端发送的消息
                out.write(buffer);
    		}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 客户端接受到的反馈
	 * @param socket
	 */
	public String serverResponse(Socket socket){
		InputStream in = null;
		String res = "";
		try {
			in = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
        byte[] b = new byte[7];
        int len = 0;
		try {
			in.read(b,0,b.length);
			res = CommonTools.BinaryToHexString(b);
			//System.out.println("接收到服务器数据:" + res);
			return res;
		} catch (IOException e) {
			e.printStackTrace();
		}

        System.out.println("接收到服务器数据:" + res);
        return res;
	}

	public interface OnServerResponse{
		public void onResponse(String res);
	}

}
