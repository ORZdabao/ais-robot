package com.nmgj.ais.config.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 拦截器,验证是否登录.
 *
 */
public class CommonInterceptor implements HandlerInterceptor {

	/**
	 *
	 * @param request
	 * @param response
	 * @param handler
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// 请求路径
		String reqURI = request.getRequestURI();
	//	System.out.println("进拦截器...." + reqURI);

		if (1 == 1) {
			return true;
		}
		// 不需要拦截 websocket自动刷新时需要
/*		if (reqURI.endsWith("user/login.do") || reqURI.endsWith("/ws") || reqURI.endsWith("candidpic1.do")
				|| reqURI.endsWith("candidpic.do")) {
			return true;
		}
		// 获取Session
		HttpSession session = request.getSession();
		Object userName = session.getAttribute(Constants.SessionKey.USERNAME);
		if (userName != null) {
			// 获取请求的URL
			return true;
		}
		// 不符合条件的，跳转到登录界面
		response.setContentType("text/html;charset=UTF-8");
		response.setStatus(401);
		return false;	// 请求路径*/
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
